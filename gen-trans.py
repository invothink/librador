import os
import sys
import json
import urllib.request
import urllib.parse
from pathlib import Path

cwd = Path(__file__).parent.absolute()

logfd = open(str(cwd / 'malayalam.tr'), 'w')
def log(fmt, *args):
    out = (fmt % args) + '\n'
    logfd.write(out)
    sys.stdout.write(out)

def mltxt(txt):
    txt = urllib.parse.quote_plus(txt)
    url = "http://www.google.com/inputtools/request?text={}&ime=transliteration_en_ml&num=1&cp=0&cs=0&cb=_callbacks_._3jk2w04cn".format(txt)
    data = urllib.request.urlopen(url).read().decode('utf-8')
    data = data.replace('%ഡി', '%d')
    data = data.replace('%സ്', '%s')
    return data.split('"')[5]
    

cfg_str = {}
for cln in (cwd / 'cfgkeys.h').read_text().split('\n'):
    cln = cln.split('"')
    if len(cln) == 9:
        cfg_str[cln[1]] = cln[3]
log('LANGUAGE "ML-IN UTF-8";')
#log('//')
checked = []
files = list(cwd.glob('*.cpp'))
files.extend(list(cwd.glob('*.lay')))
for cppfn in files:
    log('//\n// ' + str(cppfn.name))
    for ln in open(cppfn, 'r'):
        tb = ln.find('t_(')
        if tb > -1:
            tb += 3
            te = ln.find(')', tb)
            ts = ln[tb:te]
            if ts.startswith('CfgStr'):
                log('//    '+ln[tb:-1])
                ts = cfg_str[ts.split('"')[1]]
            elif ts == 'APP_NAME':
                ts = 'LibCat'
            ts = ts.replace('"', '')                
            if ts in checked:
                continue
            checked.append(ts)
            log("\"%s\",\n\t\"%s\";\n", ts, mltxt(ts))

logfd.close()
#input()

