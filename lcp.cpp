#include "Librador.h"
#include <lcp/lcp.h>

/*
    Libcat Package files are zip files (DEFLATED) with multiple commands.
    Every command is a directory in root with associated files and args.json.
	Commands are hyphenated alpha-numeric strings suffixed with a version number.
    Command arguments may be passed through args.json inside the command directory.
    

    Example:
        thss-vkd.lcp
            /restore-v1
            /restore-v1/args.json : {"origin": {"os": "win", "app": "lc:20.09.04.0483"}}
            /import-data-v1
            /import-data-v1/args.json: {"origin": {"os": "win", "app": "lc:20.09.04.0483"}}
            /import-data-v1/data.json: {"members": {....}, "books": {....}}
*/

#define RESTORE_v1	"restore-v1"
#define IMPORT_v1	"import-v1"

typedef int (*LCPCommand)(ZipReader &pkg, Value args);

bool BackupDB(String pkgPath){
	
	ZipWriter pkg;
	if (!pkg.Create(pkgPath)) {
		PromptOK("Failed to create backup file");
		return false;
	}
	
	Progress pi("Backing up database...");
	auto pg = AsGate64(pi);
	
	
	Json args;
	args
		("creator", String()<<ToLower(APP_NAME)<<" "<<APP_VERSION)
		("time", FormatTime(GetSysTime(), "DD-MM-YYYY HH:mm:ss <AM/PM>"))
		("os", OSName())
	;
	
	StringStream argBuf(~args);
	pkg.AddFile(argBuf, RESTORE_v1"/args.json", pg);
	
	pkg.AddFile(ConfigPath("config.json"), RESTORE_v1"/config.json", pg);
	
	{
		Sql sql(Sqlite3());
		if (!sql.Execute("PRAGMA wal_checkpoint;")){
			PromptOK(DeQtf(String("Failed to checkpoint databse: ") << sql.GetErrorCodeString()));
			return false;
		}
		
		pkg.AddFile(GetEnvVar("DB"), RESTORE_v1"/libcat.db",pg);
	}
	
	pkg.Close();
	
	return !pi.Canceled();
}


bool RestoreDB(ZipReader &pkg){
	auto db = GetEnvVar("DB"), ini = ConfigPath("config.ini"), json = ConfigPath("config.json");;
	auto now = FormatTime(GetSysTime(), "DDMMYYhhmmss");
	auto db_bak = Format("%s.%s.bak", db, now);
	auto ini_bak = Format("%s.%s.bak", ini, now);
	auto json_bak = Format("%s.%s.bak", json, now);
	
	Progress pi("Restoring database...");
	auto restore = [&](String pkgfile, String outfile, String bakfile)->bool{
		bool cancelled = false;
		FileMove(outfile, bakfile);
		FileOut out(outfile);
		int iFile = pkg.Find(pkgfile);
		if (iFile < 0)
			return false;
		pkg.ExtractFile(iFile, out, AsGate64(pi));
		return !pi.Canceled();
	};

	
	Sqlite3().Close();
	
	if((restore(RESTORE_v1"/config.ini", ini, ini_bak) || restore(RESTORE_v1"/config.json", json, json_bak))
		&& restore(RESTORE_v1"/libcat.db", db, db_bak)){
		return true;
	}
	
	if (FileExists(json_bak)){
		FileDelete(json);
		FileMove(json_bak, json);
	}
	
	if (FileExists(ini_bak)){
		FileDelete(ini);
		FileMove(ini_bak, ini);
	}
	
	if (FileExists(db_bak)){
		FileDelete(db);
		FileMove(db_bak, db);
	}
	
	return false;
}

int Restore_v1(ZipReader &pkg, Value args){
	if(PromptYesNo(Format("Are you sure you want restore database with the backup taken at % ? ", args["time"]))){
		if(RestoreDB(pkg)){
			PromptOK("Database successfully restored");
			return UPDATE_RESTART;
		}
	}
	PromptOK("Database not restored");
	return UPDATE_CONTINUE;
}

int Import_v1(ZipReader &pkg, Value args){
	if(PromptYesNo(Format("Are you sure you want to import data from package created at % ?", args["time"]))){
		ValueMap data = ParseJSON(pkg.ReadFile(IMPORT_v1"/data.json", Null));
		ImportData(data);
		return UPDATE_RESTART;
	}
	PromptOK("Package not imported");
	return UPDATE_CONTINUE;
}

int ImportPackage(String lcp) // returns true when restart needed.
{
	//return UPDATE_CONTINUE;
	int response = UPDATE_EXIT;
	
	ArrayMap<String, LCPCommand> supportedCmds;
	supportedCmds
		(RESTORE_v1, Restore_v1)
		(IMPORT_v1, Import_v1)
	;
	
	ZipReader pkg;
	
	if(!pkg.Open(lcp)){
		Exclamation(Format("Invalid packge file: %s", lcp));
		return response;
	}
	
	//	Find all commands from pkg
	ArrayMap<String, Value> packageArgs;
	for(ZipEntry &f : pkg.files){
		String cmd = f.path.Mid(0, f.path.Find("/"));
		if(f.path.EndsWith("/args.json") && f.path.GetLength() - cmd.GetLength() == 10)
			packageArgs.Add(cmd,ParseJSON(pkg.ReadFile(f.path.Begin(), Null)));
	}
	
	//	Run matching commands
	int processed = 0;
	for(int i=0; i < supportedCmds.GetCount(); i++){
		String cmd = supportedCmds.GetKey(i);
		if (packageArgs.Find(cmd) >= 0){
			int ret = supportedCmds.Get(cmd)(pkg, packageArgs.Get(cmd));
			response = max(ret , response);
			if(ret != UPDATE_CONTINUE)
				processed ++;
		}
	}

	if( packageArgs.GetCount() > 1)
		PromptOK(Format("Completed %d updates out of %d", processed, packageArgs.GetCount()));

	return response;
}

