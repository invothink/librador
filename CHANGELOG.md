# Libcat changelog
------------------

## Changes in next version (13 Nov 2023)
 - FIX: using html escape instead of xml escape
 - fix: added Date of renewal and depoist fields

## Changes in v2.9.8 (22 Jun 2023)
 - adding xml export to reports
 - feat: added upload makefile target and stared integration of xml export
 - feat: stared integration of xml export
 - fix: lanugage id in xmlexport
 - fix: settings corruption on save
 - fix: xmlexport added dcterms name space
 - fix: xmlexport added path updates and compile info in advanced info
 - fix: xmlexport utf-8 encoding for dc in header

## Changes in v2.9.7 (17 Mar 2023)
 - fix: GetInt raises exception when value is a string

## Changes in v2.9.6 (17 Mar 2023)
 - fix: phone numbers and purge before update on linux
 - release v2.9.5

## Changes in v2.9.5 (08 Mar 2023)
 - fix: reports.json cleanup of surplus entries

## Changes in v2.9.4 (08 Mar 2023)
 - feat: added configurable barcode regex filter for books and members
 - feat: added sqlite checkpoint before backup
 - feat: changelog generation and updated testing release upload
 - feat: integrated lcp with lzma for backup and tuned the compression params
 - fix: barcode regex pattern label
 - fix: focus of filter edit box in select member dialog
 - fix: selection of added member when created from member select dialog
 - refact: moved db init to serperate folder and restore_v1 code
 - release v2.9.4

## Changes in v2.9.3 (01 Mar 2023)
 - feat: added sysinfo button in advanced tab
 - fix: barcode behaviour
 - fix: barcode behaviour, registeration dlg type bug and macros for beta upload

## Changes in v2.9.2 (28 Feb 2023)
 - feat: added return date in book return
 - feat: integrated code signing
 - fix: crash on Action dialalogs for editing items. Heed to the warnings of the overloads of the system.
 - fix: report generation on linux due to missing wkhtmltopdf
 - fix: report generation on windows due to failing SetEnvironment

## Changes in v2.9.1 (28 Jan 2023)
 - feat: new registeration window, moved to json config with editor and updator for linux
 - feat: submenus in reports and configurable root menu
 - fix: api key in config
 - fix: frameless reg window on windows
 - fix: merged with windwos layout correction for reg screen
 - fix: windows update versionion
 - fixed: windows logo and frame for reg win
 - refact: updated config to use json for storage and new config editor
 - updated registeration and added LLVM 14 cross building

## Changes in v2.9.0 (23 Dec 2022)
 - fix: erratic issual reload after return
 - fix: members.cpp removal for build
 - fix: windres version
 - updated registeration screen

## Changes in v2.3.0 (02 Jun 2022)
 - Refactored members and added variant config key
 - Removed mandatory flag for reg dlg password & updated version
 - added types column to members and relabels it for school variant.
 - merged linux config and data dir to home/.libcat

## Changes in v2.2.0 (24 Feb 2022)
 - feat: git_commit_and_push build: integration for linux
 - refact: changed name to Libcat

## Changes in v2.1.0 (21 Feb 2022)
 - feat: added downlaod code function and update check.
 - feat: ensures single instance on both windows and linux
 - feat: windows update code
 - fix: removed db path prompting
 - fix: windows path support

## Changes in v2.0.0 (24 Jan 2022)
 - fix: long books.cpp into a dir, ignoring of mandatory fields in new dialogs, invalid damaged book count and searchablity of deleted members.

## Changes in v1.99.99 (15 Dec 2021)
 - Add CHANGELOG
 - Added Job
 - Added Malayalam translations
 - Added auto complete of publisher
 - Added autocomplete
 - Added deleted members report
 - Added shortcut buttons and SQLSelect report dialog
 - FIX: ML-IN translations
 - Feat: Added report of issueals by a member group
 - Feat: Issue report by member group
 - Feat: More buttons. Renamed Syc as advanced and moved in settings
 - Fixed report bug and added barcode
 - Fixed sync
 - Integrating barcode
 - Merged auto completion of publisher and new reports by mukesh
 - Merged with origin
 - Merging windows with linux sourcetree
 - Updated changelog
 - Updated lcp format and added todays status in home screen
 - added export of import errors as csv
 - added import-v1 to lcp commands
 - added licenses file
 - changed barcode search to trigger on enter and minor bug fixes
 - checking
 - feat: added barcode and auto complete of ddc-code
 - feat: added barcode for books and DDC code field
 - feat: added new icons
 - feat: adding auto listing of books
 - feat: csv viewer and sql commander
 - fix: book status on deleting issues
 - fix: bug in books by category report when the call_no does not have a digit
 - fix: jobs and sync and added backup
 - fix: ordering books with stock number in bokos report
 - fix: reading reports.json from config folder
 - fix: sorting books by catalog number in books by category report
 - fix: windows compilation
 - gradation overview reports and classwise issue reports
 - merge categories
 - merging changes
 - modified changelog
 - refact: changed import to support transaction
 - reference books report
 - removed bakup inclusion
 - updating book reports
