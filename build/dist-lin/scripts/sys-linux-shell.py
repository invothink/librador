#!/usr/bin/python3
import subprocess
import json


def add_unity_favs():
    try:
        app = 'application://libcat.desktop'
        res = subprocess.check_output([
            u'gsettings', u'get', u'com.canonical.Unity.Launcher', u'favorites'
        ])
        data = json.loads(res[:-1].replace("'", '"'))
        data.append(app)
        x = json.dumps(data)
        subprocess.check_output([
            u'gsettings', u'set', u'com.canonical.Unity.Launcher', u'favorites', x
        ])
    except:
        pass
        
add_unity_favs()
