#!/bin/bash
#
# This file contains utility functions to be invoked during installation
#
pwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

ARCH=$(dpkg  --print-architecture)  # i386/amd64
source /etc/os-release;
DISTRO=$ID
DISTRO_VER="$ID-$VERSION_ID$BUILD_ID"
OS="$DISTRO_VER-$ARCH"
CODE_ARCH="${VERSION_CODENAME:=$ID}_${ARCH}"
archdir="$pwd/arch/$ARCH"


function cmd_setup_shell(){
  echo "Setting shell integrations for $DISTRO_VER..."
  
  python3 /opt/libcat/scripts/sys-linux-shell.py || true
}


function cmd_debian_postinst(){
  cmd_setup_shell
}


function cmd_run(){
  $pwd/libcat || true
}


# run commands given in arg or default to cmd_run
cmds=$*
test -z "$cmds" && cmds="run"
for a in $cmds; do
  (declare -F cmd_$a > /dev/null) && cmd_$a || echo "Ignoring $a..."
done
