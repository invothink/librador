[Setup]
#define ApplicationName 'LibCat'
#define ApplicationVersion GetStringFileInfo('dist-win\libcat.exe', PRODUCT_VERSION)
AppName={#ApplicationName}
AppVersion=1.5
AppVerName={#ApplicationName} {#ApplicationVersion}
AppCopyright=Invothink Systems LLP
AppPublisher=Invothink Systems LLP
AppPublisherURL=https://invothink.com/libcat
AppSupportPhone=+919744410015
VersionInfoVersion={#ApplicationVersion}
;LicenseFile=dist/licences.txt

OutputBaseFilename=libcat-setup-{#ApplicationVersion}+windows
DefaultDirName={sd}\LibCat
DefaultGroupName=LibCat
UninstallDisplayIcon={app}\libcat.exe
Compression=lzma2
SolidCompression=true
OutputDir=.
DisableWelcomePage=yes
DisableStartupPrompt=true
DisableProgramGroupPage=true
DisableDirPage=true
SetupLogging=true
RestartIfNeededByRun=false
TimeStampsInUTC=true
ShowLanguageDialog=no

[Files]
Source: dist\*; DestDir: {app}; Flags: recursesubdirs
Source: dist-win\*; DestDir: {app}; Flags: recursesubdirs
Source: dist\fonts\NotoSans-Regular.ttf; DestDir: {commonfonts}; Flags: uninsneveruninstall; FontInstall: Noto Sans
Source: dist\fonts\NotoSansMalayalamUI-Regular.ttf; DestDir: {commonfonts}; Flags: uninsneveruninstall; FontInstall: Noto Sans Malayalam UI

[Run]
Filename: {app}\libcat.exe; Description: Launch LibCat; Flags: postinstall nowait skipifsilent

[Icons]
Name: {group}\LibCat; Filename: {app}\libcat.exe
Name: {commondesktop}\LibCat; Filename: {app}\libcat.exe
Name: {group}\Uninstall; Filename: {uninstallexe}


[Registry]
Root: HKLM; Subkey: "Software\Classes\.lcp"; ValueType: string; ValueName: ""; ValueData: "LibCatPkg"; Flags: uninsdeletevalue
Root: HKLM; Subkey: "Software\Classes\.db"; ValueType: string; ValueName: ""; ValueData: "LibCatPkg"; Flags: uninsdeletevalue
Root: HKLM; Subkey: "Software\Classes\.csv"; ValueType: string; ValueName: ""; ValueData: "LibCatPkg"; Flags: uninsdeletevalue
Root: HKLM; Subkey: "Software\Classes\LibCatPkg"; ValueType: string; ValueName: ""; ValueData: "LibCat Package"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\LibCatPkg\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\libcat.exe,1"
Root: HKLM; Subkey: "Software\Classes\LibCatPkg\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\libcat.exe"" ""%1"""

