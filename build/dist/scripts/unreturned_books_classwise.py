#!/bin/env python3
import lib

report = lib.Report(
    debug=True,
    name='unreturned_classwise',
    title="Classwise List of Unreturned Books - {klass}".format(klass=lib.argv[1]),
    pre="From: {from_}                              To: {to}".format(
            from_ = lib.ymd2dmy(lib.argv[2]),
            to = lib.ymd2dmy(lib.argv[3])),
    cols=['Accession Num', 'Title', 'Student Adm. Num', 'Name', 'Issued Date']
)

ret = report.runsql("select accn_no, title, member_no, name, issued from \
           issues where due >= ? and due <= ? and address = ? and returned is NULL", \
                    lib.argv[2], lib.argv[3], lib.argv[1]).fetchall()

report.begin()
for row in ret:
    report.add(row)
report.end()