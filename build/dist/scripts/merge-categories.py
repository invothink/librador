#!/bin/env python3
import lib

db = lib.get_db()
curser = db.cursor()

new_cat, code = lib.argv[2].split(',')

curser.execute("insert into CATEGORY(name,code) values (?,?)",(new_cat, code))
new_cat_id = curser.lastrowid
params = []
for cat in lib.argv[1]:
    params.append((new_cat_id, cat))

curser.executemany("update BOOK set cat_id=? where cat_id=?", params)
curser.executemany("delete from CATEGORY where id=?", lib.argv[1])

db.commit()
