#!/bin/env python3
import lib


db = lib.get_db()
mgroup_from_db = db.execute( "select name from category where id = ?", (lib.argv[1], ) ).fetchone()[0]
encoded_mgroup = str(mgroup_from_db).encode('cp1252', errors='xmlcharrefreplace')
mgroup = lib.utf_decode(encoded_mgroup)

report = lib.Report(
    debug=True,
    name='issues By Member Group',
    title="Issue Report for {mgname}".format(mgname=mgroup),
    pre="From: {from_}                              To: {to}".format(
            from_ = lib.ymd2dmy(lib.argv[2]),
            to = lib.ymd2dmy(lib.argv[3])),
    cols=['Stock No', 'Title', 'Member No', 'Name', 'Returned']
)

ret = report.runsql("select accn_no, title, issues.member_no, issues.name, returned, issued from issues \
                    where member_no in (select member_no from member where mgroup_id = ?) and \
                    issued >= ? and issued <= ?",  lib.argv[1], lib.argv[2], lib.argv[3])

ldate = ''
report.begin()

for row in sorted(ret.fetchall(), key=lambda x: x[5]):
    if row[5] != ldate:
        ldate = row[5]
        report.subhead([ lib.longdate(ldate) ])
    report.add(row[:-1])

report.end()