#!/bin/env python3
import lib
from datetime import datetime

db = lib.get_db()
cat_name_from_db = db.execute( "select name from category where id = ?", (lib.argv[1], ) ).fetchall()[0][0]
encoded_cat_name = str(cat_name_from_db).encode('cp1252', errors='xmlcharrefreplace')
category_name = lib.utf_decode(encoded_cat_name)
language = db.execute( "select name from language where id = ?", (lib.argv[2], ) ).fetchall()[0][0]

report = lib.Report(
    name="category_books_report_"+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    title=category_name.title() + "_" + language + " Report - "+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    cols=['Serial', 'Title', 'Author', 'Price', 'Added on'],
    debug=True
)

rows = report.db.execute("select accn_no, title, author, price, book.ctime \
            from book where book.cat_id = ? and book.lang_id = ? \
            order by accn_no, title", (int(lib.argv[1]), int(lib.argv[2]), ) ).fetchall()

report.begin()

for row in rows:
    report.add(row)
report.end()