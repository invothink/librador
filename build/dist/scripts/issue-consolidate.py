#!/bin/env python3
import lib
import calendar
import collections


report = lib.Report(
    debug=True,
    name='consolidated_issues',
    title="Consolidated Issue Report",
    cols=['Month', 'Number of Issues']
)


start_year = lib.argv[1].split('-')[0]
start_date = "{sy}-03-31".format(sy=start_year)
end_date = "{ey}-03-31".format(ey=int(start_year)+1)

months = calendar.month_abbr[4:13] + calendar.month_abbr[1:5] #Financial Year
month_indexes = {i +1: m for i, m in enumerate(calendar.month_abbr[1:])}

ret = report.runsql("select strftime(\"%m\", issued), count(accn_no) from \
           issues where issued >= ? and issued <= ? group by strftime(\"%m\", issued)",  start_date, end_date)

ldate = ''
report.begin()
data = ret.fetchall()#sorted(ret.fetchall(), key=lambda x: month_indexes[int(x[0])])
data = {d[0]: d[1] for d in data}
cleaned_data = {}
for k, v in data.items():
    month_index = month_indexes[int(k)]
    cleaned_data[month_index] = v

for m in months:
    if m not in cleaned_data:
        cleaned_data[m] = 0

cleaned_data = collections.OrderedDict(sorted(cleaned_data.items(), key=lambda t: months.index(t[0])))

for m, i in cleaned_data.items():
    report.add([m, i])

report.add(["Total", sum(cleaned_data.values())])
report.end()