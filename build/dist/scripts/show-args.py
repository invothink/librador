#!/bin/env python3
import sys,os
from os.path import join
from tkinter import Tk, messagebox

root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
args = "Program launched with args: \n"

for i in range(len(sys.argv)):
	args += "sys.argv[%d] = %s\n" % (i, sys.argv[i])

root = Tk()
root.withdraw()
messagebox.showinfo("Args", args)
sys.exit(0)