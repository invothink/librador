#!/bin/env python3
import lib
from datetime import datetime

report = lib.Report(
    name="deleted_books_report_"+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    title = "Deleted Books Report - "+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    cols=['Serial', 'Title', 'Author', 'Category', 'Price', 'Deleted on', ],
    debug=True
)

rows = report.db.execute("select accn_no, title, author, name, price, book.dtime \
            from book left join category on book.cat_id = category.id \
            where book.dtime is not NULL \
            order by accn_no, title").fetchall()

report.begin()

for row in rows:
    report.add(row)
report.end()