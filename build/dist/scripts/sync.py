import json
import sys, os
import sqlite3
import time, collections
import urllib.request

version = '1'
sync_url = 'https://libcat.in/apiv1/sync'
#sync_url = 'http://localhost:9999/apiv1/sync'

def die(code, msg):
	print(json.dumps(msg))
	exit(code)


def upload(table, cols, data, updated):
	post = {'ver': version} #table: {'cols': cols, 'data': data}}
	post.update({k: os.getenv(k, k+str(time.time())) for k in ['aid', 'pid']})
	post = json.dumps(post, ensure_ascii=False).encode('utf-8')
	req = urllib.request.Request(sync_url, post)	
	req.add_header('content-type', 'application/json')
	req.add_header('Content-Length', len(post))	
	try:
		rep = urllib.request.urlopen(req)
		ret = json.loads(rep.read().decode('utf-8'))
		if ret['err']:
			die(0, {'err': ret['err']})
	except Exception as e:
		err = str(e)
		if 'failure in name resolution' in err or 'Connection refused' in err:
			err = "No internet connection, nothing synced."
		die(0, {'err': err})	
	
	if table not in updated:
		updated[table] = {}
	iid = cols.index('id')
	times = updated[table].get(ret['time'], []) 
	times.extend([d[iid] for d in data])
	updated[table][ret['time']] = times
	


def sync_db(fn):
	"""
	with open("sync.txt","r") as fp:
		print(fp.read())
		exit(0)
	"""
	err = 'Nothing to upload.'
	updated = {}  # updated['table']['time']=[ids...]
	db = sqlite3.connect(fn)
	tables = db.execute('select NAME from SQLITE_MASTER where type="table"').fetchall()
	stats = collections.Counter()
	#die(0, {"err": os.environ.get('aid').strip() + '//' + os.environ.get('pid')})
	for tab in tables:
		tab = tab[0].lower()
		if tab not in ['category', 'publisher', 'language', 'book']: continue		 #['publisher']: continue #
		data = []
		cur = db.execute('select * from ' + tab +' where STIME is null')
		cols = [desc[0].lower() for desc in cur.description]
		for row in cur.fetchall():
			stats[tab] += 1
			data.append([str(r) if r else '' for r in row])
			if len(data) > 420:
				upload(tab, cols, data, updated)
				data = []
		if data: upload(tab, cols, data, updated)
	if sum(stats.values()) > 0:
		msg = 'Uploaded: ' + (', '.join([k+'('+str(v)+')' for k,v in stats.items()]))
		die(0, {'err': msg, 'ids': updated})
	die(0, {'err': 'Nothing to upload'})


def sync_json(fn):
	return {'err': 'Not implemented.'}




if __name__ == "__main__":
	show_usage = lambda _: {'err': 'usage: sync.py <data.db|data.json>'}
	if len(sys.argv) < 2:
		die(1, show_usage(None))
	if sys.argv[1] == '-v':
		die(0, {'ver': '19.10.18'})
	
	ext = os.path.splitext(sys.argv[1])[1]
	handlers = {'.db': sync_db, '.json': sync_json}
	status = handlers.get(ext, show_usage)(sys.argv[1])
	
	print(json.dumps(status))