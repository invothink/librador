#!/bin/env python3
import lib
from datetime import datetime

db = lib.get_db()
cat_name_from_db = db.execute( "select name from category where id = ?", (lib.argv[1], ) ).fetchall()[0][0]
encoded_cat_name = str(cat_name_from_db).encode('cp1252', errors='xmlcharrefreplace')
category_name = lib.utf_decode(encoded_cat_name)

report = lib.Report(
    name="category_books_report_"+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    title=category_name.title() + " Report - "+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    cols=['Catalog num', 'Title', 'Author', 'Price', 'Shelf'],
    debug=True
)

rows = report.db.execute("select call_no, title, author, price, shelf \
            from book where book.cat_id = ? and book.dtime IS NULL order by call_no", (lib.argv[1], ) ).fetchall()
try:
    rows = sorted(rows, key = lambda a: int(''.join([e for e in a[0] if str(e).isdigit()])))
except Exception as e:
    pass
    # rows = report.db.execute("select call_no, title, author, price, shelf \
    #             from book where book.cat_id = ? and book.dtime IS NULL order by call_no", (lib.argv[1],)).fetchall()

report.begin()

for row in rows:
    report.add(row)
report.end()