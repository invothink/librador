#!/bin/env python3
import lib

report = lib.Report(
    debug=True,
    name='issues',
    title="Issue Report",
    pre="From: {from_}                              To: {to}".format(
            from_ = lib.ymd2dmy(lib.argv[1]),
            to = lib.ymd2dmy(lib.argv[2])),
    cols=['Book ID', 'Title', 'Member ID', 'Name']
)

ret = report.runsql("select accn_no, title, member_no, name, issued from \
           issues where issued >= ? and issued <= ?",  lib.argv[1], lib.argv[2])

ldate = ''
report.begin()

for row in sorted(ret.fetchall(), key=lambda x: x[4]):
    if row[4] != ldate:
        ldate = row[4]
        report.subhead([ lib.longdate(ldate) ])
    report.add(row[:-1])

report.end()