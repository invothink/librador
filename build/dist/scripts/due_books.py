#!/bin/env python3
import lib
from datetime import datetime

report = lib.Report(
    name="due_books_report_"+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    title = "Deleted Books Report - "+str(datetime.strftime(datetime.now(), "%d-%m-%Y")),
    cols=['Serial', 'Title', 'Issued on', 'Due on', 'Issued to', 'phone', 'Address'],
    debug=True
)

rows = report.db.execute("""select issues.book_id, issues.title, issues.issued, issues.due, issues.name, issues.phone, issues.address from issues where issues.due < date() and issues.returned IS NULL order by issues.due""").fetchall()

report.begin()

for row in rows:
    report.add(row)
report.end()