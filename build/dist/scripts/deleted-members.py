#!/bin/env python3
import lib

lib.Report(
    name='deleted_members',
    title="Deleted Member Report",
    cols=['Name', 'Phone', 'Address', 'Ward', 'Deleted on'],
    
    sql="select name, phone, address, ward, dtime from member where DTIME not NULL order by dtime DESC"
)