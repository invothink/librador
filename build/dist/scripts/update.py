#!/bin/python
import sys, os, time
import datetime, tarfile
import subprocess, threading
from tkinter import *
from tkinter.ttk import *
from pathlib import Path

def do_update():
    root = Path(__file__).resolve().parent
    bakdir = root.parent / '_bak_' /  datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S")
    tar = tarfile.open(sys.argv[1], "r:xz")

    def run_hook(hook):
        try:
            tfile = tar.getmember(hook) 
            tar.extract(tfile)
            exec(open(hook).read())
            Path(hook).remove()
        except:
            pass

    HOOKS = ['./_preupdate_.py', './_postupdate_.py']
    run_hook(HOOKS[0])
    time.sleep(1)
    os.system('taskkill /IM libcat.exe')
    time.sleep(1)    
    for tfile in tar:
        if tfile.name not in HOOKS and tfile.isreg():
            file = Path(tfile.name)
            if file.exists():
                print(' U ', tfile.name)
                bakup = bakdir / file
                if not bakup.parent.exists():
                    bakup.parent.mkdir(parents=True)
                file.rename(bakup)
            else:
                print(' N ', tfile.name)
            tar.extract(tfile)

    run_hook(HOOKS[1])
    tar.close()
    subprocess.Popen(["libcat.exe", "--updated", str(os.getpid())])
    time.sleep(20)


worker = threading.Thread(target=do_update)

def check_thread():
    if not worker.is_alive():
        win.destroy()
    win.after(100, check_thread)

def start_thread():
    worker.start()
    win.after(100, check_thread)

win = Tk()
win.resizable(0,0)
win.title('Libcat')
win.geometry('300x100')
#win.overrideredirect(1)
win.attributes('-topmost', 1)
win.eval('tk::PlaceWindow %s center' % win.winfo_pathname(win.winfo_id()))

lb = Label(win, text="Updating Libcat")
lb.pack(padx=20, pady=10)
pi = Progressbar(win, orient ="horizontal", length = 250, mode ="indeterminate")
pi.pack(padx=20, pady=10)
pi.start(10)
pi.after(1000, start_thread)

win.mainloop()    
