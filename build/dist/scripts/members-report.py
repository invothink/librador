#!/bin/env python3
import lib

lib.Report(
    name='members',
    title="Member Report",
    cols=['Name', 'Phone', 'Address', 'Ward', 'Blood<br/>Group'],
    
    sql="select name, phone, address, ward, blood from member order by name"
)