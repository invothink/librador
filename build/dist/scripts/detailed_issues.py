#!/bin/env python3
import lib

db = lib.get_db()
selects = " where issues.issued >= ? and issues.issued <= ?"
joins = ""
args_list = [lib.argv[1], lib.argv[2]]
title_text = "Detailed Issue Report"

def handle_encoding(name):
    encoded = str(name).encode('cp1252', errors='xmlcharrefreplace')
    return lib.utf_decode(encoded)

if lib.argv[4] != 0:
    joins = joins + " left join member on MEMBER.MEMBER_NO = ISSUES.MEMBER_ID"
    selects = selects + " and member.mgroup_id = ?"
    args_list.append(int(lib.argv[4]))
    title_text = title_text + " - " + handle_encoding(db.execute("select name from mgroup where id = ?", (lib.argv[4], )).fetchall()[0][0])

if lib.argv[3] != 0:
    joins = joins + " left join BOOK on book.accn_no = ISSUES.ACCN_NO"
    selects = selects + " and book.CAT_ID = ?"
    args_list.append(int(lib.argv[3]))
    title_text = title_text + " - " + handle_encoding(db.execute("select name from category where id = ?", (lib.argv[3], )).fetchall()[0][0])

report = lib.Report(
    debug=True,
    name='issues',
    title= title_text,
    pre="From: {from_}                              To: {to}".format(
            from_ = lib.ymd2dmy(lib.argv[1]),
            to = lib.ymd2dmy(lib.argv[2])),
    cols=['Book ID', 'Title', 'Member ID', 'Name']
)

ret = report.db.execute("select issues.accn_no, issues.title, member.member_no, issues.name, issues.issued from issues" + joins + selects,  args_list)

ldate = ''
report.begin()

for row in sorted(ret.fetchall(), key=lambda x: x[4]):
    if row[4] != ldate:
        ldate = row[4]
        report.subhead([ lib.longdate(ldate) ])
    report.add(row[:-1])

report.end()