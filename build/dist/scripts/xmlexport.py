import lib
import string
import re
from html import escape

def fieldset(tpl):
    fields = []
    for l in tpl.splitlines(False):
        l = l.strip()
        var = re.findall('{(.*)}', l)
        fields.append((var[0] if var else '', l))
    return fields


def get_books():
    db = lib.get_db()
    cursor = db.cursor()

    # Execute the query
    cursor.execute("""
        SELECT
            ACCN_NO, CALL_NO, CATEGORY, PUBLISHER, LANG_ID as LANGUAGE,
            TITLE, AUTHOR, ISBN, EDITION, SHELF, BARCODE, DDCCODE, PRICE
        FROM
            bookcat
        ;
    """)

    columns = [column[0] for column in cursor.description]
    for row in cursor.fetchall():
        row = [escape(c) if type(c) is str else c for c in row]
        d = dict(zip(columns, row))
        d["LANGUAGE"] = "eng"
        if row[5] and sum([int(c > 'ÿ') for c in row[5]]) > len(row[5])/3:
            d["LANGUAGE"] = "mal"
        yield d
    cursor.close()


def export(xml_fn, header="", footer="", fields=[]):
    with open(xml_fn, 'w', encoding='utf8') as f:
        f.write(header)

        for book in get_books():
            for var, line in fields:
                val = book.get(var)
                if not var:
                    f.write(line)
                elif val not in ['None', None, '', 'null']:
                    f.write(line.format(**book))
            f.write('\n')
        f.write(footer)


marc_fields = '''
<record xmlns="http://www.loc.gov/MARC21/slim">
    <leader>01472cam a2200361 a 4500</leader>
    <controlfield tag="001">{ACCN_NO}</controlfield>
    <controlfield tag="008">040903s2023    nyu           000 1 {LANGUAGE}  </controlfield>
    <datafield tag="020" ind1=" " ind2=" "><subfield code="a">{ISBN}</subfield></datafield>
    <datafield tag="082" ind1=" " ind2=" "><subfield code="a">{CALL_NO}</subfield></datafield>
    <datafield tag="100" ind1="1" ind2=" "><subfield code="a">{AUTHOR}</subfield></datafield>
    <datafield tag="245" ind1="1" ind2="0"><subfield code="a">{TITLE}</subfield></datafield>
    <datafield tag="650" ind1=" " ind2="0"><subfield code="a">{CATEGORY}</subfield></datafield>
    <datafield tag="852" ind1=" " ind2=" "><subfield code="c">{SHELF}</subfield></datafield>
    <datafield tag="876" ind1=" " ind2=" "><subfield code="p">{BARCODE}</subfield></datafield>
    <datafield tag="876" ind1=" " ind2=" "><subfield code="c">{PRICE}</subfield></datafield>
    <datafield tag="082" ind1="0" ind2="4"><subfield code="a">{DDCCODE}</subfield></datafield>
    <datafield tag="041" ind1="0" ind2="4"><subfield code="a">{LANGUAGE}</subfield></datafield>
</record>
'''

dc_fields = '''
<rdf:Description>
    <dc:type>text</dc:type>
    <dc:title>{TITLE}</dc:title>
    <dc:creator>{AUTHOR}</dc:creator>
    <dc:language>{LANGUAGE}</dc:language>
    <dc:subject>{CATEGORY}</dc:subject>
    <dc:identifier>Accession Number: {ACCN_NO}</dc:identifier>
    <dc:identifier>Call Number: {CALL_NO}</dc:identifier>
    <dc:identifier>ISBN: {ISBN}</dc:identifier>
    <dc:identifier>Barcode: {BARCODE}</dc:identifier>
    <dcterms:location>{SHELF}</dcterms:location>
    <dcterms:ddc>{DDCCODE}</dcterms:ddc>
</rdf:Description>
'''

formats = {
    'marc': {
        'header': '<?xml version="1.0" encoding="utf-8"?>\n'
                  '<collection xmlns="http://www.loc.gov/MARC21/slim">\n',
        'footer': '</collection>',
        'fields': fieldset(marc_fields),
    },
    'dc': {
        'header': '<?xml version="1.0" encoding="utf-8"?>\n'
                  '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/">\n',
        'footer': '</rdf:RDF>',
        'fields': fieldset(dc_fields),
    }
}


format = formats[lib.argv[1]]
xml_fn = lib.argv[2] if lib.argv[2].endswith('.xml') else lib.argv[2] + '.xml'
export(xml_fn, **format)

# python build/dist/scripts/marc21.py dc /home/me/dev/lcmarc21/exportdc.xml
