#!/bin/env python3
import lib

lib.Report(
    name='books',
    title="Books Report",
    cols=['Serial', 'Title', 'Author', 'Category', 'ISBN', 'Price'],

    sql="select accn_no, title, author, name, isbn, price \
            from book left join category on book.cat_id = category.id\
            where book.dtime IS NULL order by cast(accn_no as integer)"
)