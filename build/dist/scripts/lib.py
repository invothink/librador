import os
import sys
import json
import sqlite3
from pathlib import Path
from datetime import datetime
from subprocess import Popen
try:
    from ctypes import windll
    windows=True
except:
	windows=False
    


# -------------------------------------Report-----------------------------------

root = os.getenv('DATADIR') or Path(__file__).resolve().parent.parent
root = Path(root)
reports = root / 'reports/'
bin = root / 'bin'
argv = sys.argv

if not reports.exists():
    reports.mkdir(parents=True)

def ymd2dmy(date):
    return datetime.strptime(date, "%Y-%m-%d").strftime("%d-%m-%Y")

def longdate(date):
    return datetime.strptime(date, "%Y-%m-%d").strftime('%B %d %Y')

def error(msg):
    sys.exit(msg)

default_style="""
<style type="text/css">
    table {
        font-family: 'Noto Sans Malayalam UI', verdana,arial,sans-serif;
        font-size:9pt;
        color:#333333;
        border-width: 1px;
        border-color: #3A3A3A;
        border-collapse: collapse;
        width: 96%;
        margin:auto auto;
        postition: relative;
    }
    table caption { padding-bottom: 2em;}
    table th {
        border-width: 1px;
        padding: 12px;
        border-style: solid;
        border-color: #3A3A3A;
        background-color: #e2e2e2;
        margin-top:10px;
    }
    table td {
        border-width: 1px;
        padding: 2px;
        border-style: solid;
        border-color: #555;
        background-color: #ffffff;
    }
</style>
"""

default_header="""
<table>
  <caption>
    <h2>{org}</h2>
    <h3>{regno}</h3>
    <h3>{addressline}</h3>
    <h3>{title}</h2>
    <pre>{pre}</pre>
  </caption>
  <thead>
    <tr>{theads}</tr>
  </thead>
  <tbody>
"""

default_header_multi="""
<table>
  <caption>
    <h2>{org}</h2>
    <h3>{regno}</h3>
    <h3>{addressline}</h3>
    <h3>{title}</h2>
    <pre>{pre}</pre>
  </caption>
  
"""

class Report:
    def __init__(self, name='', title='', cols=[], pre='', sql=None, debug=False):
        self.debug = debug
        self.name = name
        self.report = reports / (name + '.html')
        self.pdf = reports / datetime.now().strftime("%d-%m-%y-"+name+"-%H%M%S.pdf")
        self.db = get_db()

        self.cfg = {k: os.getenv(k, '') for k in ['addressline', 'regno', 'org']}
        self.cfg['addressline'] = self.cfg['addressline'].replace('\\n', ', ')
        self.cfg.update(dict(title=title, cols=cols, pre=pre))
        if sql:
            self.query(sql)

    def runsql(self, query, *args):
        if(self.debug):
            print(query.replace('?', '{}').format(*args))
        return self.db.execute(query, args)

    def begin(self, style=default_style, header=default_header):
        self.cfg['theads']  = '<th scope="col">#</th><th scope="col">'
        self.cfg['theads'] += '</th><th scope="col">'.join(self.cfg['cols'])
        self.cfg['theads'] += '</th>'

        self.fd = self.report.open(mode='w')
        self.fd.write(style)
        encoded_header = header.format(**self.cfg).encode('cp1252', errors='xmlcharrefreplace').decode('utf-8', errors='ignore')
        self.fd.write(encoded_header)
        self.irow = 0
        return self.fd

    def begin_multi_table(self, style=default_style, header=default_header_multi):
        self.fd = self.report.open(mode='w')
        self.fd.write(style)
        encoded_header = header.format(**self.cfg).encode('cp1252', errors='xmlcharrefreplace').decode('utf-8', errors='ignore')
        self.fd.write(encoded_header)
        self.irow = 0
        return self.fd

    def add_table(self, caption, headers, data):
        table = "<table><caption><h3>{c}</h3></caption>".format(c=caption)
        table += "<thead>"
        for col in headers:
            table += "<th>" + col + "</th>"
        table += "</thead><tbody>"
        table += data
        table += "</tbody></table>"
        table += "</br></br></br></br>"
        self.fd.write(table)
        return self.fd

    def end(self, multi=False):
        if not multi:
            self.fd.write('</tbody></table>')
        self.fd.close()
        proc = Popen([os.getenv('WKHTML'), str(self.report), str(self.pdf)])
        proc.wait()
        shellexe(str(self.pdf))
        return 0 if self.pdf.exists() else "Report generation failed"

    def add(self, row):
        self.irow += 1
        srow = [str(self.irow)]
        for r in row:
            srow.append(str(r))
        tr = "<tr><td>"
        tr += "</td><td>".join(srow)
        tr += "</tr></td>"
        encoded_tr = tr.encode('cp1252', errors='xmlcharrefreplace')
        self.fd.write(utf_decode(encoded_tr))

    def subhead(self, row):
        self.fd.write("<tr><th colspan='{0}'>{1}</th><tr>".format(
            len(self.cfg['cols'])+1, row[0]
        ))

    def query(self, sql):
        self.begin()
        for row in self.db.execute(sql).fetchall():
            self.add(row)
        self.end()


# -------------------------------------Misc-------------------------------------

def get_db():
    return sqlite3.connect(os.getenv('DB'))

def shellexe(fn, action='open'):
	if windows:
		windll.shell32.ShellExecuteW(0, action, fn, None, None, 0)
	else:
		os.system('xdg-open "' + fn + '"&')

def utf_decode(data):
    return data.decode(encoding="utf-8", errors='ignore')

def read_config_from_file():
    file_contents = open(str(root / 'config.ini'), encoding='utf-8', errors='xmlcharrefreplace').readlines()
    config = {}
    for line in file_contents:
        config[line.split('=')[0]] = line.split('=')[1]
    config["addressline"] = config["address"]
    return config
