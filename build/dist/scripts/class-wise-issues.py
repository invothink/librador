#!/bin/env python3
import lib
import calendar
import collections


report = lib.Report(
    debug=True,
    name='classwise_issuals',
    title="Classwise Issuals - {klass}".format(klass=lib.argv[1]),
        pre="From: {from_}                              To: {to}".format(
            from_ = lib.ymd2dmy(lib.argv[2]),
            to = lib.ymd2dmy(lib.argv[3])),
    cols=['Adm. Num', 'Student Name', 'Number of Issues']
)

classroom = lib.argv[1]
start_date = lib.argv[2]
end_date = lib.argv[3]

months = calendar.month_abbr[4:13] + calendar.month_abbr[1:5] #Financial Year
month_indexes = {i +1: m for i, m in enumerate(calendar.month_abbr[1:])}

ret = report.runsql("select member_no as AdmNum, Name, count(*) as Total from issues \
                    where issued >= ? and issued <= ? and address = ? \
                    group by member_no order by cast(member_no as int)", start_date, end_date, classroom).fetchall()


report.begin()
for row in ret:
    report.add(row)
report.end()