#!/bin/env python3
import lib
import collections
import calendar
import sys,os
from os.path import join
from tkinter import Tk, messagebox

db = lib.get_db()
start_year = lib.argv[1].split('-')[0]

start_date = "{sy}-04-01".format(sy=start_year)
end_date = "{ey}-03-31".format(ey=int(start_year)+1)
report_name = 'overview'


default_header_multi="""
<table>
  <caption>
    <h2>{org}</h2>
    <h3>{regno}</h3>
    <h3>{addressline}</h3>
    <h3>{title}</h2>
    <pre>{pre}</pre>
  </caption>
"""

report = lib.Report(
    debug=True,
    name=report_name,
    title="Overview Report",
)

report.begin_multi_table()

def mgroup_issuals(caption, book_type):
    issues = db.execute(
        "select mgroup.NAME, strftime(\"%m\", issued) as month, count(mgroup.NAME) as issuals  from ledger \
        left join member on member_id = member.id \
        left join book on book_id = book.id \
        left join mgroup on mgroup.id=member.mgroup_id \
        where issued >= (?) and issued <= (?) and book.type_id = (?) \
        group by month, mgroup.Name", (start_date, end_date, book_type, )).fetchall()

    mgroups = db.execute("select mgroup.NAME from mgroup").fetchall()
    counter = collections.defaultdict(collections.Counter)
    months = calendar.month_abbr[4:13] + calendar.month_abbr[1:4] #Financial Year
    monthly_totals = collections.Counter()
    mgroup_totals = collections.Counter()

    for mg in mgroups:
        for m in months:
            counter[mg[0]][m] = 0

    for k, v in counter.items():
        counter[k] = collections.OrderedDict(sorted(v.items(), key=lambda t: months.index(t[0])))

    # because database month int needs to be converted to an index map of the financial calendar
    month_indexes = {i + 1: m for i, m in enumerate(calendar.month_abbr[1:])}

    for r in issues:
        #['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar']
        month = month_indexes[int(r[1])]
        mgroup = r[0]
        counter[mgroup][month] = int(r[2])
        mgroup_totals[mgroup] += int(r[2])
        monthly_totals[month] += int(r[2])

    for m in months:
        if m not in monthly_totals:
            monthly_totals[m] = 0

    monthly_totals = collections.OrderedDict(sorted(monthly_totals.items(), key=lambda t: months.index(t[0])))

    col_headers = ["Membership Types"] + months + ["Total"]
    table = []
    for k, v in counter.items():
        row = [k]
        for m, icount in v.items():
            row.append(icount)
        row.append(mgroup_totals[k])
        table.append(row)

    total_issuals = 0
    row = ['Total']
    for m, mtotal  in monthly_totals.items():
        total_issuals += mtotal
        row.append(mtotal)
    row.append(total_issuals)
    table.append(row)

    thtml = ""
    for row in table:
        thtml += "<tr>"
        for cell in row:
            thtml += "<td>" + str(cell) + "</td>"
        thtml += "</tr>"
    report.add_table(caption, col_headers, thtml)

def total_members(caption):
    members = db.execute("select mgroup.name, count(member.id) from member \
    join mgroup on member.mgroup_id=mgroup.id \
    where member.dtime is null group by mgroup.name").fetchall()


    headers = ["Membership Type", "Number"]
    table = ""
    total = 0
    for mgroup, count in members:
        table += "<tr><td>" + mgroup + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count
    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"
    report.add_table(caption, headers, table)

def new_members(caption):
    members = db.execute("select mgroup.name, count(member.id) from member \
    join mgroup on member.mgroup_id=mgroup.id \
    where member.dtime is null and member.doj >= (?) and member.doj <= (?) \
    group by mgroup.name", (start_date, end_date, )).fetchall()

    headers = ["Membership Type", "Number"]
    table = ""
    total = 0
    for mgroup, count in members:
        table += "<tr><td>" + mgroup + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count
    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"

    report.add_table(caption, headers, table)

def deleted_members(caption):
    members = db.execute("select mgroup.name, count(member.id) from member \
    join mgroup on member.mgroup_id=mgroup.id \
    where member.dtime is not null and member.dtime >= (?) and member.dtime <= (?) \
    group by mgroup.name", (start_date, end_date, )).fetchall()

    headers = ["Membership Type", "Number"]
    table = ""
    total = 0
    for mgroup, count in members:
        table += "<tr><td>" + mgroup + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count

    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"
    report.add_table(caption, headers, table)


def total_books(caption):
    members = db.execute("select category.name, count(book.id) from book \
    join category on book.cat_id=category.id \
    where book.dtime is null group by category.name ").fetchall()


    headers = ["Category", "Books"]
    table = ""
    total = 0
    for category, count in members:
        table += "<tr><td>" + category + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count

    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"
    report.add_table(caption, headers, table)

def new_books(caption):
    members = db.execute("select category.name, count(book.id) from book \
    join category on book.cat_id=category.id \
    where book.dtime is null and book.ctime >= (?) and book.ctime <= (?) group by category.name", \
                         (start_date, end_date, )).fetchall()

    headers = ["Book Category", "Number"]
    table = ""
    total = 0
    for mgroup, count in members:
        table += "<tr><td>" + mgroup + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count

    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"
    report.add_table(caption, headers, table)

def deleted_books(caption):
    members = db.execute("select category.name, count(book.id) from book \
    join category on book.cat_id=category.id \
    where book.dtime is not null and book.dtime >= (?) and book.dtime <= (?) group by category.name", \
                         (start_date, end_date, )).fetchall()

    headers = ["Book Category", "Number"]
    table = ""
    total = 0
    for mgroup, count in members:
        table += "<tr><td>" + mgroup + "</td>" + "<td>" + str(count) + "</td></tr>"
        total += count

    table += "<tr><td>Total" + "</td><td>" + str(total) + "</td></tr>"
    report.add_table(caption, headers, table)

mgroup_issuals('Normal Issuals', 0)
mgroup_issuals('Reference Issuals', 2)
total_members("Total Members")
new_members("New Members")
deleted_members("Removed Members")

total_books("Total Books")
new_books("New Books")
deleted_books("Removed Books")

report.end(multi=True)