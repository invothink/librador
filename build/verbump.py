#!/bin/env python3
import json
import os
import re
import argparse
import sys
from datetime import datetime
from pathlib import Path
from typing import List, Dict


PARSE_REGEX = r'^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.' \
              r'(?P<patch>0|[1-9]\d*)(?:-(?P<pre_release>' \
              r'(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.' \
              r'(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?' \
              r'(?:\+(?P<build>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$'

SOURCE_TPLS = {
    'h':  '// DO NOT EDIT. This file will be over-written by verbump.\n'
          '#define APP_NAME "{dir}"\n'
          '#define APP_VERSION "{version}"\n'
          '#define APP_MAJOR {major}\n'
          '#define APP_MINOR {minor}\n'
          '#define APP_PATCH {patch}\n'
          '#define APP_PRERELEASE "{pre_release}"\n'
          '#define APP_BUILDYEAR {build_year}\n'
          '#define APP_BUILDTIME "{build_time}"\n',

    'go': '// DO NOT EDIT. This file will be over-written by verbump.\n'
          'package {dir}\n'
          'const Version = "{version}"\n'
          'const Major = {major}\n'
          'const Minor = {minor}\n'
          'const Patch = {patch}\n'
          'const PreRelease = "{pre_release}"\n'
          'const BuildYear = {build_year}\n'
          'const BuildTime = "{build_time}"\n',
          

    'js': '// DO NOT EDIT. This file will be over-written by verbump.\n'
          'const Version = "{version}"\n'
          'const Major = {major}\n'
          'const Minor = {minor}\n'
          'const Patch = {patch}\n'
          'const PreRelease = "{pre_release}"\n'
          'const BuildYear = {build_year}\n'
          'const BuildTime = "{build_time}"\n',

    'py': '# DO NOT EDIT. This file will be over-written by verbump.\n'
          'version = "{version}"\n'
          'major = {major}\n'
          'minor = {minor}\n'
          'patch = {patch}\n'
          'pre_release = "{pre_release}"\n'
          'build_year = {build_year}\n'
          'build_time = "{build_time}"\n',

    'json': '{{\n'
            ' "version": "{version}",'
            ' "major": {major},'
            ' "minor": {minor},'
            ' "patch": {patch},'
            ' "pre_release": "{pre_release}"'
            ' "build_year = {build_year}\n'
            ' "build_time = "{build_time}"\n'
            '}}\n',            
}


class SemVer:
    def __init__(self, verfile, semver='', source_files=set()):
        self.major = 0
        self.minor = 0
        self.patch = 0
        self.build = None
        self.pre_release = None
        self.parse_from(semver)
        self.verfile = Path(verfile)
        self.source_files = set(source_files)
        self.load_verfile()

    def load_verfile(self):
        import shlex
        if not self.verfile.exists():
            return
        for ln in self.verfile.read_text().splitlines():
            k, v = shlex.split(ln)[0].split('=')
            if k.upper() == 'VERSION':
                self.parse_from(v)
            elif k.upper() == 'VER_SOURCES':
                self.source_files.update(set(v.split(' ')))

    def save_verfile(self):
        if not self.source_files:
            input("Press [ENTER] to create Verfile in "+os.getcwd())
        self.verfile.write_text(f'VERSION={self.semver()}\n'
                                f'VER_SOURCES="{" ".join(self.source_files)}"\n')

    def save_sources(self):
        time = datetime.now()
        data = {**self.__dict__,
                'version': self.semver(),
                'pre_release': self.pre_release or '',
                'build_year': time.year,
                'build_time': time.isoformat()
                }

        for f in map(Path, self.source_files):
            tpl = SOURCE_TPLS.get(f.suffix.lower()[1:])
            if tpl:
                f.write_text(tpl.format(**data, dir=f.absolute().parent.name))
                print("Updated", f)
            else:
                print("Unsupported", f)

    def parse_from(self, semver):
        if '.' in semver:
            ver = re.search(PARSE_REGEX, semver).groupdict()
            ver = {k:int(v) if v and v.isdigit() else v for k,v in ver.items()}
            self.__dict__.update(ver)

    def semver(self):
        return f"{self.major}.{self.minor}.{self.patch}" \
               f"{'-'+self.pre_release if self.pre_release else ''}" \
               f"{'+' + str(self.build) if self.build else ''}"

    def bump(self, args:Dict[str, str]):
        print('Current version:', self.semver())
        for k, v in args.items():
            if not v:
                continue
            if v[0].isdigit():
                self.__dict__[k] = int(v)
            elif v[0] == '+':
                self.__dict__[k] += int(v[1:])
            elif v[0] == '-':
                self.__dict__[k] -= int(v[1:])
            elif k in self.__dict__:
                self.__dict__[k] = v
            else:
                raise ValueError('Invalid option :', (k, v))

            if 'major' in k: self.patch = self.minor = 0
            if 'minor' in k: self.patch = 0
        print('Bumpped version:', self.semver())


#sys.argv = [ '--verfile=Verfile', '--minor=+1', 'test/version.py', 'test/version.json', 'test/version.js', 'test/version.go']

def parse_args(argv):
    parser = argparse.ArgumentParser(description='Tool to change semantic version')
    parser.add_argument('source_files', metavar='source_file', type=str, nargs='*',
                        help='file to write the version info. supported: *.go,*.py,*.js,*.json')
    parser.add_argument('--major', dest='major', const='+1', nargs='?',
                        help='+/i major version')
    parser.add_argument('--minor', dest='minor', const='+1', nargs='?',
                        help='+/i minor version')
    parser.add_argument('--patch', dest='patch', const='+1', nargs='?',
                        help='+/i patch version')
    parser.add_argument('--pre-release', dest='pre_release', nargs='?',
                        help='mark as alpha,beta-1...')
    parser.add_argument('--build', dest='build', nargs='?',
                        help='build metadata, will be sanitized')
    parser.add_argument('--verfile', dest='verfile', type=Path,
                        default=Path('./Verfile'),
                        help='version file, default: ./verfile')

    args = parser.parse_args(argv)
    if not (args.major or args.minor or args.patch or args.pre_release):
        args.minor = '+1'
    return args


args = parse_args(sys.argv[1:])

sv = SemVer(args.verfile, source_files=args.source_files)

sanitize = lambda s: ''.join((c if c.isalnum() or c in '.-' else '-' for c in (s or '')))

try:
    sv.bump(dict(
        major=args.major,
        minor=args.minor,
        patch=args.patch,
        pre_release=sanitize(args.pre_release),
        build=sanitize(args.build)
    ))
except Exception as e:
    raise e
    exit(1)

sv.save_sources()
sv.save_verfile()




