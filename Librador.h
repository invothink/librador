
#ifndef _Librador_Librador_h
#define _Librador_Librador_h

#include <Libcat/version.h>

#include <Core/Core.h>
#include <Core/Rpc/Rpc.h>
#include <CtrlLib/CtrlLib.h>
#include <SqlCtrl/SqlCtrl.h>
#include <GridCtrl/GridCtrl.h>
#include <DropGrid/DropGrid.h>
#include <plugin/sqlite3/Sqlite3.h>
#include <plugin/zip/zip.h>
#include <plugin/pcre/Pcre.h>

//#include <Functions4U/Functions4U.h>


#include <Libcat/components/components.h>

using namespace Upp;

#define SCHEMADIALECT <plugin/sqlite3/Sqlite3Schema.h>
#define MODEL <Libcat/librador.sch>
#include "Sql/sch_header.h"


#define LAYOUTFILE <Libcat/Librador.lay>
#include <CtrlCore/lay.h>

#define IMAGECLASS LibradorImg
#define IMAGEFILE <Libcat/Librador.iml>
#include <Draw/iml_header.h>

#include "config/config.h"

enum LIBTYPES {
	TypePublicLib,
	TypeHomeLib,
	TypeSchoolLib,
};

Sqlite3Session& Sqlite3();

extern String AppPath(String filename);
extern String DataPath(String filename);
extern String ConfigPath(String filename);

int IntValue(Value v);
void EnsureSingleInstance(Function<void ()> cb);
bool DoRegisteration();
bool AskQuestion(String title, String field, Vector<String> &args);
void MakeMultiSelect(SuggestCtrl &ctrl);
String GetEnvVar(const char *id);
bool SetEnvVar(const char *id, const char *val);
void SetEnvVars();
String ExpandPath(String path);
String OSName();
ValueMap OSInfo();
bool ValidBarcode(String str);
int64 GetMicroSeconds();
extern void StartUpdateThread();

bool SaveCSV(const char *path, Vector<Vector<Value>> &rows, String header, String sep=",");


enum {
	CB_BOOKS_FILTER
};

extern void DownloadAndRun(String code, bool showError);
extern void UpdateCheck();

extern bool DevMachine;
extern Index<WString> AutoTitles, AutoAuthors, AutoPublishers;
extern Value ReportConfig;
extern String NotAvaliable;

extern int BookCount;
extern int UpdateBookCount();


extern bool BackupDB(String pkgPath);
extern bool RestoreDB(FileUnZip &pkg);


inline String Checksum(String str){
	String enc=Base64Encode(ToLower(str));
	return FormatIntHex(enc.GetCount()%254,2)+enc;
}

int MyPid();
void SysDetached(String cmd);


extern int RunUpdate(Value result, String file);
extern int ImportPackage(String lcp);
extern void ImportData(ValueMap &data);
inline Value ReadJson(String filename){ return ParseJSON(LoadFile(ConfigPath(filename)));}
inline Value ReadConfig(String filename){ return ParseJSON(LoadFile(GetExeDirFile(filename)));}

extern Value LoadJSON(String filename);
extern bool SaveJSON(String filename, Value data);


const int UPDATE_CONTINUE = 0;
const int UPDATE_EXIT = 1;
const int UPDATE_RESTART = 2;

const int DELETED = -1;
const int GENDER_FEMALE = 1;
const int GENDER_MALE = 2;
const int GENDER_OTHER = 3;

enum BookStatus {
	BOOK_DELETED = -1,
	BOOK_AVALIABLE = 0,
	BOOK_BORROWED,
	BOOK_EXTENTED,
	BOOK_DAMAGED,
	BOOK_LOST,
	BOOK_WITHELD
};

namespace Codes {
    static const int OK                 = 0;
    static const int Canceled           = 1;
    static const int Unknown            = 2;
    static const int InvalidArgument    = 3;
    static const int DeadlineExceeded   = 4;
    static const int NotFound           = 5;
    static const int AlreadyExists      = 6;
    static const int PermissionDenied   = 7;
    static const int ResourceExhausted  = 8;
    static const int FailedPrecondition = 9;
    static const int Aborted            = 10;
    static const int OutOfRange         = 11;
    static const int Unimplemented      = 12;
    static const int Internal           = 13;
    static const int Unavailable        = 14;
    static const int DataLoss           = 15;
    static const int Unauthenticated    = 16;
}

const int CALLNO_CCACC = 0; // {CODE}{ACCN_NO}
const int CALLNO_CCIDX = 1; // {CODE}{INDEX}

const int BARCODE_NONE = 0;
const int BARCODE_BOOK = 1;
const int BARCODE_MEMBER = 2;
const int BARCODE_BOOK_MEMBER = 3;


const int VARIANT_NORMAL = 0;
const int VARIANT_SCHOOL = 1;
extern bool SchoolVariant;

SQLID(ISSUES);
SQLID(BOOKCAT);
SQLID(SEL);



struct CachedId: Moveable<CachedId> {
	bool batch;
	InVector<String> values;
	Event<> WhenChanged;
	Event<> WhenPopulate;
	CachedId() :batch(false)		{};
	String None;
	
	void Populate()					{ Clear(); Begin(); WhenPopulate(); End();};
	void Clear()					{ values.Clear(); };
	void Begin()					{ batch = true; };
	void End()						{ batch = false; WhenChanged();};
	CachedId& Set(int k, String v)		{ values.At(k) = v; if(!batch) WhenChanged(); return *this;};
	int GetCount()					{ return values.GetCount(); };
	int GetId(String v)				{ return values.Find(v);};
	String &operator()(int k)		{ return k>0 && k < values.GetCount()?values[k]: None; };
};
extern CachedId MGroupName, CatName, PubName, LangName, BloodGroups, Genders;


struct BookDlg : public WithBookDlg<TopWindow> {
	typedef BookDlg CLASSNAME;
	
	SqlCtrls ctrls;
	BookDlg();
	//bool Accept();
	
	Value bookId;
	bool ExecuteAdd();
	bool ExecuteEdit(Value id);
	void DeleteBook();
	
	void NewCategory();
	void ChangeCategory();
	void UpdateCallNo(String code);
	
	void NewPublisher();
	
	void CheckForAuthor();
	void ChildGotFocus();
	
	void CloseDlg();
};

struct DelBookDlg : WithDelBookDlg<TopWindow> {
	typedef DelBookDlg CLASSNAME;
	DelBookDlg(String title);
	
	//bool ExecuteDelete(Value id);
};


struct SelectMemberDlg : WithSelectMemberDlg<TopWindow> {
	typedef SelectMemberDlg CLASSNAME;
	SelectMemberDlg();
	
	void FilterList();
	void ClearFilter();
	void Highlight();
	void CloseDlg();
};

struct IssueDlg : public WithIssueDlg<TopWindow> {
	typedef IssueDlg CLASSNAME;
	
	SqlCtrls ctrls;
	SelectMemberDlg membDlg;
	
	public:
	IssueDlg();
	
	Value issueId;
	bool ExecuteAdd(Index<int> &bookIds);
	bool ExecuteEdit(Value id, bool showReturn = false);
	void DeleteIssue();
	void SelectMember();
	String FormatBook(Sql &book);

};


struct LoanArchiveDlg : WithLoanArchiveDlg<TopWindow> {
	typedef LoanArchiveDlg CLASSNAME;
	LoanArchiveDlg();
	
	void EditIssue();
	void DeleteIssue(int id, IssueDlg *dlg);
	void FilterList();
	void ClearFilter();
};


struct BookArchiveDlg : WithBookArchiveDlg<TopWindow> {
	typedef BookArchiveDlg CLASSNAME;
	BookArchiveDlg();
	
	void RestoreBook();
	void ShowNote();
	void FilterList();
	void ClearFilter();
};


void RefreshBooks();
void RefreshIssuals();
void RefreshMembers();



class BooksTab : public WithBooksTab<StaticRect> {
	typedef BooksTab CLASSNAME;
	RegExp reBookBarcode;

public:
	WithBooksPanel<StaticRect> books;
	WithLoanPanel<StaticRect> loans;

	void NewBook();
	void EditBook();
	void ClickedBook();
	void SelectBook();
	void IssueBook();
	void FilterBooksList();
	void CheckBarcode(EditString *filter);
	void ClearBooksFilter();
	void QueryBooks();
	void MoreBook();

	void SelectReturn();
	
	void EditIssue();
	void ExtendIssue();
	void ReturnIssue();
	void DeleteIssue(int id, IssueDlg *dlg);
	void FilterLoanList();
	void ClearLoanFilter();
	void QueryIssues();
	void MoreIssue();
	
	BooksTab();
};

struct HomeTab : WithHomeTab<StaticRect> {
	typedef HomeTab CLASSNAME;
	HomeTab();
	void UpdateStats();
};




struct MemberDlg : public WithMemberDlg<TopWindow> {
	typedef MemberDlg CLASSNAME;
	
	SqlCtrls ctrls;
	SelectMemberDlg membDlg;
	MemberDlg();
	
	Value memberId;
	void Clear();
	bool ExecuteAdd();
	bool ExecuteEdit(Value id);
	void SelectRefrer();
	void DeleteMember();
	void SetRefrer(Value id);
	void CloseDlg();
};


class MembersTab : public WithMembersTab<StaticRect> {
	typedef MembersTab CLASSNAME;
	
	RegExp reMemberBarcode;
public:

	void NewMember();
	void EditMember();
	void DeleteMember(int id, MemberDlg *dlg);
	void FilterList();
	void CheckBarcode();
	void ClearFilter();
	void MoreMember();
	void UpdateFilterMGroup();
	int  Query(SqlBool where);

	MembersTab();
};


class AdvancedTab : public WithAdvancedTab<StaticRect> {
	typedef AdvancedTab CLASSNAME;
	FileStream logFile;
public:
	void LogLine(String msg);
	void Download();
	void Upload();
	void Backup();
	void Restore();
	void UpdateSynced(const Value &ret);

	AdvancedTab();
};


struct MemberHistoryDlg : WithArrayListDlg<TopWindow> {
	typedef MemberHistoryDlg CLASSNAME;
	MemberHistoryDlg(String name, Value id);
};

struct BookListDlg : WithArrayListDlg<TopWindow> {
	typedef BookListDlg CLASSNAME;
	BookListDlg(String name, SqlId &key, Value id);
};

struct MemberListDlg : WithArrayListDlg<TopWindow> {
	typedef MemberListDlg CLASSNAME;
	MemberListDlg(String name, Value id);
};


class Librador : public WithLibradorLayout<TopWindow> {
public:
	typedef Librador CLASSNAME;
	Librador();
	
	String lastKeys;
	int lastKeyTime;
	
	HomeTab	homeTab;
	BooksTab booksTab;
	MembersTab membersTab;
	//ReturnsDlg returnsdlg;
	AdvancedTab advancedTab;
	WithAboutTab<StaticRect> aboutTab;
	
	
	bool reportCancelled;
	LocalProcess reportProcess;
	void RunReport(ValueMap);
	void ReportThread(String *cmd, Vector<String> *args);
	Bar::Item* populateMenu(Value items, Bar &bar);

};


struct ItemListDlg : WithItemListDlg<TopWindow> {
	typedef ItemListDlg CLASSNAME;
	
	bool queried;
	Array<EditString> editor;
	
	
	ItemListDlg(const char* title, SqlId &table);
	ItemListDlg& AddColumn(SqlId id, const char *text, int w);
	
	
	ItemListDlg& OnAdd(Event<> clicked);
	ItemListDlg& OnEdit(Event<> clicked);
	ItemListDlg& OnDelete(Event<> clicked);
	ItemListDlg& OnView(Event<int> clicked);
	ItemListDlg& OnChange(Event<> changed);
	
	void toggleButtons();
};


extern Librador *librador;


struct Downloader {
	Progress    pi;
	HttpRequest http;
	int64       loaded;
	String      url;
	FileOut     out;
	String      path;
	
	typedef Downloader CLASSNAME;
	
	void Content(const void *ptr, int size);
	bool Download();
	void ShowProgress();
	void Start();
	
	Downloader(String url, String path);
};


struct CsvViewer : WithCsvViewerDlg<TopWindow> {
	typedef CsvViewer CLASSNAME;
	Vector<int> cw;
	Font font;
	String source;
	
	CsvViewer();
	CsvViewer& OpenCsv(String path, bool searching=false);
	void GetCsvLine(Stream& s, int separator,Event<String&> onCol);
	void ColSize();
};




#endif
