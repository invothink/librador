#include <Libcat/Librador.h>
#include <Libcat/res.brc>


enum STEPS {
	Welcome,
	TypeSel,
	TypeInfo,
	Details,
};


class LibcatReg : public WithLibcatRegLayout<TopWindow> {
	int step;
	
public:
	LibcatReg();
	void ShowPane(int d);
	String Validate(int d);
	void SetValues(ValueMap m);
	ValueMap GetValues();
	
	WithRegStart<StaticRect> startPane;
	WithRegType<StaticRect> typePane;
	WithRegPublicLib<StaticRect> publicLibPane;
	WithRegSchoolLib<StaticRect> schoolLibPane;
	WithRegHomeLib<StaticRect> homeLibPane;
	WithRegDetails<StaticRect> detailsPane;

};


LibcatReg::LibcatReg()
{
	CtrlLayout(*this, APP_NAME " v" APP_VERSION);
	CloseBoxRejects().TopMost().SetFrame(BlackFrame());
		
	// Wizard interface
	BackPane.Background(White());
	ImgLogo.SetImage(LibradorImg::lc_logo());
	BtnBack << [=] { ShowPane(-1); };
	BtnNext << [=] { if(BtnNext.GetLabel()=="Finish") Break(IDOK); else ShowPane(+1); };
	
	
	// Welcome
	CtrlLayout(startPane);
	startPane.Background(White());
	startPane.Greeting =
						"[4 &Hi &Welcome to Libcat ] &"
						"[1 &]"
						"[2 Please answer a few questions to let us setup the software for your needs.]"
						"[1 &&Information entered here will be displayed in reports and can be updated later.]"
						//"[1 &&For support, call: +91 87146 12395.]"
						;
	startPane.SideImg.SetImage(LibradorImg::books());
	Body.AddChild(&startPane.SizePos());
	

	// Library type
	CtrlLayout(typePane);
	typePane.Background(White());
	typePane.Type.MinCaseHeight(77);
	typePane.Type.SetVertical();
	typePane.Type.Add(TypePublicLib, "Public Library", 40);
	typePane.Type.Add(TypeHomeLib, "Home Library", 30);
	typePane.Type.Add(TypeSchoolLib, "School Library", 30);
	typePane.PublicImg.SetImage(LibradorImg::human());
	typePane.HomeImg.SetImage(LibradorImg::couple());
	typePane.SchoolImg.SetImage(LibradorImg::schoolstud());
	Body.AddChild(&typePane.SizePos());


	// Public library
	CtrlLayout(publicLibPane);
	publicLibPane.Background(White());
	publicLibPane.SideImg.SetImage(LibradorImg::human());
	Body.AddChild(&publicLibPane.SizePos());

	// Home library
	CtrlLayout(homeLibPane);
	homeLibPane.Background(White());
	homeLibPane.SideImg.SetImage(LibradorImg::couple());
	Body.AddChild(&homeLibPane.SizePos());
	

	// School library
	CtrlLayout(schoolLibPane);
	schoolLibPane.Background(White());
	schoolLibPane.SideImg.SetImage(LibradorImg::schoolstud());
	Body.AddChild(&schoolLibPane.SizePos());
	

	// Library details
	CtrlLayout(detailsPane);
	detailsPane.Background(White());
	detailsPane.SideImg.SetImage(LibradorImg::bookss());
	detailsPane.Scheme
		.Add("${cat}${acc}", "Category Code + Stock/Accession No")
		.Add("${cat}${idx}", "Category Code + Book Index")
		.Adjust();
/*	detailsPane.Scheme.Add("cat+acc",	"Category Code + Stock/Accession No");
	detailsPane.Scheme.Add("acc",		"Stock/Accession No");
	detailsPane.Scheme.Add("lcc",		"Libcat Category Code v1");
	detailsPane.Scheme.Add("ddc",		"Dewey Decimal Classification (DDC)");*/
	detailsPane.LoanPeriod <<= 5;
	Body.AddChild(&detailsPane.SizePos());
	
	step = 0;
	ShowPane(0);
	//Body.AddChild(&typePane.SizePos());
}


void LibcatReg::ShowPane(int d){
	String err = Validate(d);
	if(!err.IsEmpty()) {
		Exclamation("Please " + err);
		return;
	}

	step = minmax(step + d, 0, 3);
	Step = String("STEP ") << step << "/3";
	Step.Show(step > 0);
	Contact.Show(step == 0);
	BtnBack.Show(step > 0);
	BtnNext <<= step == 3 ? "Finish" : "Next";
	
	startPane.Hide();
	typePane.Hide();
	publicLibPane.Hide();
	homeLibPane.Hide();
	schoolLibPane.Hide();
	detailsPane.Hide();
	BtnNext.SetLabel("Next");
	
	switch(step){
	case Welcome:
		startPane.Show();
		break;
	case TypeSel:
		typePane.Show();
		break;
	case TypeInfo:
		switch(typePane.Type){
		case TypePublicLib:
			publicLibPane.Show();
			publicLibPane.Name.SetFocus();
			break;
		case TypeHomeLib:
			homeLibPane.Show();
			homeLibPane.Name.SetFocus();
			break;
		case TypeSchoolLib:
			schoolLibPane.Show();
			schoolLibPane.Name.SetFocus();
			break;
		}
		break;
	case Details:
		String regLbl("Pincode");
		
		if(typePane.Type == TypePublicLib && publicLibPane.KSLC == 1)
			regLbl = "KSLC Registeration No.";
		else if(typePane.Type == TypeSchoolLib) {
			if(schoolLibPane.CBSE == 1)
				regLbl = "CBSE Affiliation No. (optional)";
		}
		
		detailsPane.LblDetails = regLbl;
		detailsPane.Show();
		BtnNext.SetLabel("Finish");
		break;
	}
	
}


String LibcatReg::Validate(int d){
	if(d < 1 || step < TypeSel)
		return "";
	
	switch(step){
	case TypeSel:
		if(int(~typePane.Type) < 0)
			return "Please select a library type";
		break;
			
	case TypeInfo:
		#define CheckVal(X, Y) if(~X.Name == "") { X.Name.SetFocus(); return "provide the " Y " name."; } \
							if(~X.Phone == "") { X.Phone.SetFocus(); return "provide the contact number."; }
		switch(typePane.Type){
		case TypePublicLib:
			CheckVal(publicLibPane, "Public Library");
			break;
		case TypeHomeLib:
			CheckVal(homeLibPane, "House/Owner");
			break;
		case TypeSchoolLib:
			CheckVal(schoolLibPane,"School");
			break;
		}
		#undef CheckVal
	}
	return "";
}

void LibcatReg::SetValues(ValueMap m)
{
	typePane.Type = m["type"];
	#define SetVals(X) X.Name = AsString(m["name"]); X.Phone = AsString(m["phone"]); X.Email = AsString(m["email"]);
	switch(int(typePane.Type)){
		case TypePublicLib:
			SetVals(publicLibPane);
			publicLibPane.KSLC.Set(m["affiliations"]["kslc"]);
			publicLibPane.RRFL.Set(m["affiliations"]["rrfl"]);
			publicLibPane.Other.Set(m["affiliations"]["other"]);
			break;
		case TypeHomeLib:
			SetVals(homeLibPane);
			break;
		case TypeSchoolLib:
			SetVals(schoolLibPane);
			schoolLibPane.State.Set(m["affiliations"]["state"]);
			schoolLibPane.CBSE.Set(m["affiliations"]["cbse"]);
			schoolLibPane.ICSE.Set(m["affiliations"]["icse"]);
			schoolLibPane.Other.Set(m["affiliations"]["other"]);
			break;
	}
	#undef SetVals
	detailsPane.RegNo.SetData(m["regno"]);
	detailsPane.Scheme.SetData(m["classification"]);
	detailsPane.LoanPeriod.SetData(m["loanPeriod"]);
	
}

ValueMap LibcatReg::GetValues()
{
	ValueMap m;
	m
		("type", int(typePane.Type))
		("loanPeriod", ~detailsPane.LoanPeriod)
		("classification", ~detailsPane.Scheme)
		("regno", ~detailsPane.RegNo)
		("regTime", GetUTCSeconds(GetSysTime()))
	;
	#define GetVals(X)  m.Set("name", ~X.Name); m.Set("phone", ~X.Phone); m.Set("email", ~X.Email);
	switch(int(typePane.Type)){
		case TypePublicLib:
			GetVals(publicLibPane);
			m.Set("affiliations", ValueMap()
				("kslc", ~publicLibPane.KSLC)
				("rrfl", ~publicLibPane.RRFL)
				("other", ~publicLibPane.Other)
			);
			break;
		case TypeHomeLib:
			GetVals(homeLibPane);
			break;
		case TypeSchoolLib:
			GetVals(schoolLibPane);
			m.Set("affiliations", ValueMap()
				("state", ~schoolLibPane.State)
				("cbse", ~schoolLibPane.CBSE)
				("icse", ~schoolLibPane.ICSE)
				("other", ~schoolLibPane.Other)
			);
			break;
	}
	#undef GetVals
	return m;
}


bool DoRegisteration(){
	LibcatReg dlg;
	
	dlg.SetValues(config.Values);
	
	if(dlg.Execute() != IDOK)
		return false;
	
	ValueMap values = dlg.GetValues();
	for(int i=0; i<values.GetCount(); i++){
		config.Set(values.GetKey(i), values.GetValue(i));
	}
	CfgSave();
	return true;
}

