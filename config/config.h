#ifndef _Libcat_config_h_
#define _Libcat_config_h_

class Config{
	bool modified;
	Mutex mutex;
public:
	String ConfigFile;
	ValueMap Options;
	ValueMap Values;
	
	Config();
	
	
	void Load();
	void Save();
	void MigrateOld();
	
	bool HasKey(String key);
	
	Value Get(String key);
	void Set(String key, Value val);
	bool Remove(String key);
	Value operator[](String key)			{ return Get(key); };
	
	int GetInt(String key);
	String GetStr(String key)				{ return Get(key); };
	void SetInt(String key, int64 val)		{ Set(key, val); };
	void SetStr(String key, String val)		{ Set(key, val); };
	
	Event<String, Value> WhenSet;
};


class ConfigDlg : public WithConfigDlg<TopWindow> {
public:
	Config &config;
	
	ConfigDlg(Config &c);
	ValueMap changed;
	
	void Load();
	void Save();
	void EditorFactory(int line, One<Ctrl>& x);
	void WhenSel();
	
	typedef ConfigDlg CLASSNAME;
};

extern Config config;
inline int CfgInt(String key)				{ return config.GetInt(key); };
inline String CfgStr(String key)			{ return config.GetStr(key); };
inline void CfgSetStr(String key, String val){ return config.SetStr(key, val); };
inline void CfgSetInt(String key, int64 val)	{ return config.SetInt(key, val); };
inline bool CfgHas(String key)				{ return config.HasKey(key); };
inline void CfgLoad()						{ config.Load(); };
inline void CfgSave()						{ config.Save(); };
#define AuthID()   String(CfgStr("phone"))<<"\t"<<CfgStr("regno")<<"\t"<<CfgStr("email")
#define AuthParams "authID", AuthID())("authToken",  CfgStr("authToken"))("appver", APP_NAME " v" APP_VERSION

#endif
