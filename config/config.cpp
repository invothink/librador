
#include "../Librador.h"
#include "options.brc"
#include <plugin/pcre/Pcre.h>

Config config;

Config::Config()
{
	// Load();
}

void Config::Load()
{
	ConfigFile = ConfigPath("config.json");
	Options = ParseJSON(String(options_json, options_json_length));
	Values = ParseJSON(LoadFile(ConfigFile));
	
	MigrateOld();
	
	if(DevMachine)
		CfgSetStr("libcatAPIv2", "localhost:9999/api/v2");
}

void Config::Save()
{
	mutex.Enter();
	SaveFile(ConfigFile, AsJSON(Values, true));
	mutex.Leave();
}

void Config::MigrateOld()
{
	String iniFile = ConfigPath("config.ini");
	if(!FileExists(iniFile))
		return;
	
	VectorMap<String, String> vals = LoadIniFile(iniFile);
	
	VectorMap<String, Function<void(String)>> keyMap;
	#define ON_V [&](String v)
	keyMap
		("org",           ON_V{SetStr("name", v);})
		("phone",         ON_V{SetStr("phone", v);})
		("regno",         ON_V{SetStr("regno", v);})
		("email",         ON_V{SetStr("email", v);})
		("barcode",       ON_V{SetInt("barcode", ScanInt(v));})
		("loan_period",   ON_V{SetInt("loanPeriod", ScanInt(v));})
		("format_callno", ON_V{SetStr("classification", v=="0"?"${cat}${acc}":"${cat}${idx}");})
		("address",       ON_V{
			Vector<String> lines = Split(v, "\\n");
			SetStr("address1", lines.Get(0, ""));
			SetStr("address2", lines.Get(1, ""));
			RegExp r(".*(\\d{6}).*");
			if(r.Match(v))
				SetStr("pincode", r.GetString(0));
		})
	;
	#undef ON_V
	
	for (const auto &k: vals.GetKeys()){
		if(keyMap.Find(k) > -1)
			keyMap.Get(k)(vals.Get(k));
	}
	
	Save();
	
	String bakFile = String(iniFile)<<"."<<GetUTCSeconds(GetUtcTime())<<".bak";
	FileMove(iniFile, bakFile);
}


bool Config::HasKey(String key)
{
	return !Get(key).IsNull();
}


Value Config::Get(String key)
{
	if(!Values[key].IsVoid())
		return Values[key];
	
	for(int i=0; i < Options.GetCount(); i++)
		if(Options[i]["key"] == key)
			return Options[i]["defval"];
		
	return ErrorValue();
}

int Config::GetInt(String key)
{
	try {
		return Get(key);
	}
	catch(ValueTypeError) {
		return 0;
	}
}

void Config::Set(String key, Value val)
{
	mutex.Enter();
	RLOG(String("set: ")<<key<<":"<<val<<"-"<<val.GetTypeName());
	Values.Set(key, val);
	WhenSet(key, val);
	mutex.Leave();
}

bool Config::Remove(String key)
{
	mutex.Enter();
	bool ok = Values.RemoveKey(key) > -1;
	mutex.Leave();
	return ok;
}

ConfigDlg::ConfigDlg(Config &c):config(c)
{
	CtrlLayout(*this, "Settings");
	
	list.SetLineCy(list.GetLineCy() + 14);
	list.AddKey();
	list.AddColumn(t_("Setting"));
	list.AddColumn(t_("Value")).Ctrls(THISBACK(EditorFactory));
	list.WhenSel = THISBACK(WhenSel);
	
	desc.AutoHideSb();
	
	expert << [=]{ Load(); };
	btnOK << [=] { Save(); AcceptBreak(IDOK); };
	btnCancel << [=] { Break(IDCANCEL); };
	
	Load();
}


String ExpandCamelCase(String s){
	String r;
	for(int i=0; i<s.GetCount(); i++){
		int c = s[i];
		if(IsUpper(c) && i > 0)
			r.Cat(' ');
		if(i==0)
			c = ToUpper(c);
		r.Cat(c);
	}
	return r;
}


void ConfigDlg::Load()
{
	list.Clear();
	for(int i=0; i<config.Options.GetCount(); i++){
		Value option = config.Options.At(i);
		if(option["expert"] && !~expert){
			continue;
		}
		
		String label = option["label"];
		if(label.IsEmpty())
			label = ExpandCamelCase(option["key"]);
		Value value = config.Get(option["key"]);
		RLOG(String("Load: ")<<option["key"]<<":"<<value<<"-"<<value.GetTypeName());
		list.Add(i, label + (option["expert"]? "*":""), value);
	}
}


void ConfigDlg::Save()
{
	for(int i=0; i < list.GetCount(); i++){
		int oi = list.Get(i, 0);
		Value option = config.Options[oi];
		Value value = list.Get(i, 2);
		RLOG(String("Save: ")<<option["key"]<<":"<<value<<"-"<<value.GetTypeName());
		if(option["defval"] == value)
			config.Remove(option["key"]);
		else
			config.Set(option["key"], value);
	}
	config.Save();
}

void ConfigDlg::EditorFactory(int line, One<Ctrl>& x)
{
	
	Value option = config.Options[list.Get(line, 0)];
	Value choices = option["choices"];
	
	if(!choices.IsNull()){
		DropList &d = x.Create<DropList>();
		d.NoWheel().SetFrame(NullFrame());
		for(const auto &choice: choices)
			d.Add(choice[0], choice[1]);
		return;
	}
	
	//RLOG(String()<<config.Options[line]["key"]<<config.Options[line]["defval"].GetTypeName());
	if (option["defval"].Is<double>())
		x.Create<EditInt>().SetFrame(NullFrame());
	else
		x.Create<EditString>().SetFrame(NullFrame());
}

void ConfigDlg::WhenSel()
{
	if(list.IsCursor()){
		Value key = config.Options.At(list.Get(0));
		desc = (String("[4 ")<<key["desc"]);
	}
}

