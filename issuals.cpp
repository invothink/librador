#include "Librador.h"

IssueDlg::IssueDlg()
{
	CtrlLayoutOK(*this, t_("Issuals"));
	Sizeable().CloseBoxRejects();
	
	ctrls
		(BOOK_ID, book)
		(MEMBER_ID, member)
		(ISSUED, issued)
		(DUE, due)
		(RETURNED, returned)
	;
	book = 0; book.Hide();
	member = 0; member.Hide();
	btnChange <<= THISBACK(SelectMember);
	btnDelete <<= THISBACK(DeleteIssue);
}

String IssueDlg::FormatBook(Sql &book)
{
	String view;
	view
		<<"@@iml:425*480`LibradorImg::bbook`||"
		<<" [/* "<<DeQtf(~book[TITLE])<<"]"
		<<"&[ By] [/ "<<DeQtf(~book[AUTHOR])<<"]"
		<<"&[ Category] [/ "<<DeQtf(CatName(book[CAT_ID]))<<"]";
	if(book[PUB_ID] != NotAvaliable)
		view << "&[ Published by] [/ "<<DeQtf(PubName(book[PUB_ID]))<<"]";
		
	view<<"&[ Cat No#] "<<DeQtf(~book[CALL_NO])<<",  [ Accession#] "<<DeQtf(~book[ACCN_NO]);
	view<<"||";
	return view;
}

bool IssueDlg::ExecuteAdd(Index<int> &bookIds)
{
	Title("Issue Book");
	ok.SetLabel("Issue");
	btnDelete.Hide();
	returned.Hide();
	lblReturned.Hide();
	
	SelectMember();
	if(!member)
		return false;
	
	Sql sql;
	sql * Select(SqlAll()).From(BOOK).Where(ID==SqlSetFrom(bookIds));
	String view;
	view<<"[{_}!"<<CfgStr("fontFace")<<"!2 {{1:4 ";
	while(sql.Fetch())
		view << FormatBook(sql);
	view.TrimLast(2);
	view<<"}}]";
	viewer.SetQTF(view);


	Date today = GetSysDate(), dt = today + CfgInt("loanPeriod");
	due.SetDate(dt.year, dt.month, dt.day);
	issued.SetDate(today.year, today.month, today.day);
	
	if(ExecuteOK()){
		sql.Begin();
		for(int i=0; i<bookIds.GetCount(); i++){
			book = bookIds[i];
			sql * ctrls.Insert(LEDGER);
		}
		sql * ::Update(BOOK)(STATUS, BOOK_BORROWED).Where(ID ==  SqlSetFrom(bookIds));
		sql.Commit();
		return true;
	}
	return false;
}

bool IssueDlg::ExecuteEdit(Value id, bool showReturn)
{
	Title("Edit Issue");
	ok.SetLabel("Edit");
	btnDelete.Show();
	returned.Show(showReturn);
	lblReturned.Show(showReturn);
	
	issueId = id;
	
	Sql sql;
	sql * Select(MEMBER_ID, MEMBER_NO, NAME,
				BOOK_ID, TITLE, AUTHOR, CALL_NO, ACCN_NO, PUB_ID, CAT_ID, ISSUED, DUE, RETURNED)
			.From(ISSUES).Where(ID==issueId);

	if(!sql.Fetch())
		return false;
	
	String view;
	view<<"[{_}!"<<CfgStr("fontFace")<<"!2 {{1:4 " << FormatBook(sql) ;
	view.TrimLast(2);
	view<<"}}]";
	viewer.SetQTF(view);

	book = sql[BOOK_ID];
	book.Tip(~sql[TITLE]);
	member = sql[MEMBER_ID];
	name <<= Format("%s (%d)",~sql[NAME], ~sql[MEMBER_NO]);
	
	
	Date idt = sql[ISSUED], ddt = sql[DUE], rdt = sql[RETURNED];
	due.SetDate(ddt.year, ddt.month, ddt.day);
	issued.SetDate(idt.year, idt.month, idt.day);
	if(rdt.IsValid())
		returned.SetDate(rdt.year, rdt.month, rdt.day);
	
	if(ExecuteOK()){
		sql * ctrls.Update(LEDGER).Where(ID == id);
		sql.Commit();
		return true;
	}
	return false;

}

void IssueDlg::DeleteIssue()
{
	if(PromptYesNo(Format(t_("Do you want to delete this issual of '%s' to '%s'?"), DeQtf(book.GetTip()), DeQtf(~name.GetData())))) {
		SQL * ::Update(LEDGER)(DTIME, SqlCurrentTime()).Where(ID == issueId);
		SQL * ::Update(BOOK)(STATUS, BOOK_AVALIABLE).Where(ID == issueId);
		Break(IDOK);
	}
}

void IssueDlg::SelectMember()
{
	if(int(member))
		membDlg.list.FindSetCursor(int(member));
		
	if(membDlg.ExecuteOK()){
		member = membDlg.list.GetKey();
		name = Format("%s (%s)",~membDlg.list.Get(NAME),~membDlg.list.Get(MEMBER_NO));
	}
}


