#include <Libcat/Librador.h>


#include <Sql/sch_schema.h>
#include <Sql/sch_source.h>

#include <Libcat/res.brc>



void InitDB(bool freshDB, Sqlite3Session &sqlSession){
	SQL = sqlSession;
	
	SqlSchema sch(SQLITE3);
	SQL.Begin();
	All_Tables(sch);
	SqlPerformScript(sch.Upgrade());
	SqlPerformScript(sch.Attributes());
	SQL.Commit();
	SQL.ClearError();

	{
		SQL.Begin();
		String strSQL = String((char *)database_sql);
		if(!SqlPerformScript(strSQL))
			PromptOK(String("[ ")<<SQL.GetErrorCodeString()<<"] &[ "<<
				DeQtf(SQL.GetErrorStatement()) << " ]");
		SQL.Commit();
	}
	
	if(freshDB){
		{
			SQL.Begin();
			String masterSqlFile = GetExeDirFile("master.sql");
			String strSQL = FileExists(masterSqlFile)?LoadFile(masterSqlFile):String((char *)master_sql);
			if(!SqlPerformScript(strSQL))
				PromptOK(String("[ ")<<SQL.GetErrorCodeString()<<"] &[ "<<
					DeQtf(SQL.GetErrorStatement()) << " ]");
			SQL.Commit();
		}

		SQL.Begin();
		SQL * Insert(LANGUAGE)(NAME, "Malayalam");
		SQL * Insert(LANGUAGE)(NAME, "English");
		SQL.Commit();
	}
	
}


Sqlite3Session& Sqlite3()
{
	static Sqlite3Session ss;
	bool freshDB = !FileExists(GetEnvVar("DB"));
	if(!ss.IsOpen()) {
		if(!ss.Open(GetEnvVar("DB"))) {
			Exclamation("Can't create or open database file");
			Exit(1);
		}
		#ifdef _DEBUG
			ss.SetTrace();
			ss.LogErrors(true);
		#endif
		if(DevMachine)
			ss.LogErrors(true);
		InitDB(freshDB, ss);
	}
	return ss;
}
