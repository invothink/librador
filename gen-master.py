import json
import difflib
from pathlib import Path


# TODO: We are taking the first entry as the true one, but thats wrong

books = json.loads(Path('gen-master-books.json').read_text())


def clean_up(s):
    return s.replace('\u200d', '').replace('\u200c', '').strip()

def map_duplicates(dd, k, similarity=0.85):
    elst = set()
    dupes = {}
    for d in dd:
        elst.add(clean_up(d[k]))
    for e in elst:
        mlst =  difflib.get_close_matches(e, elst, 30, similarity)
        for m in mlst:
            dupes[m] = e
    return dupes

dupes_file = Path('dupes.json')
if dupes_file.exists():
    dupes = json.load(dupes_file.open('r'))
else:
    dupes = {
        'title': map_duplicates(books, 'title'),
        'author': map_duplicates(books, 'author'),
    }
    json.dump(dupes, dupes_file.open('w'))

values = set() # TODO: Cheap deduplication of similar books, this must be a list
deduped_title = dupes['title']
deduped_author = dupes['author']
for book in books:
    if not book['title']: continue
    b = {k:v.replace("'", "''") for k,v in book.items()}
    lang = 1 if b['lang'] == 'M' else 2
    title = deduped_title[clean_up(b['title'])]
    author = deduped_author[clean_up(b['author'])]
    values.add(f"('{title}', '{author}', '{b['publisher']}', {lang})")

values = ',\n '.join(sorted(values))
Path('master.sql').write_text("INSERT INTO AUTOBOOK (TITLE, AUTHOR, PUBLISHER, LANG_ID) "\
                              f"VALUES {values};")
