#ifndef _Librador_components_h_
#define _Librador_components_h_

#include <CtrlLib/CtrlLib.h>
using namespace Upp;

#define APIv2(method) JsonRpcRequestNamed(CfgStr("libcatAPIv2"))(method)



struct ActionBtns: ParentCtrl {
	Array<Button> btns;
	Array<RichTextCtrl> helps;
	int itemHeight;
	int itemSpacing;
	
	ActionBtns(int itemHeight=60, int itemSpacing=20):
		itemHeight(itemHeight),
		itemSpacing(itemSpacing)
	{
	}
	
	virtual ActionBtns& Add(const char *title, const Image &img, const char *helpText, Event<> clicked);
	int GetHeight();
};

struct ActionDlg : TopWindow
{
    ScrollBar sb;
    ActionBtns buttons;
	int itemHeight;
	int itemSpacing;
	
	ActionDlg(const char* title, int itemHeight=50, int itemSpacing=20);
    
    ActionDlg& Add(const char *title, const Image &img, const char *help, Event<> clicked);
	void MouseWheel(Point, int zdelta, dword);
	virtual void Layout();
	
	int GetCount()		{return buttons.btns.GetCount(); };
};


#endif
