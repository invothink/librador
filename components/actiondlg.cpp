#include <Libcat/Librador.h>

ActionBtns& ActionBtns::Add(const char *title, const Image &img, const char *helpText, Event<> clicked){
	auto &btn = btns.Add();
	auto &help = helps.Add();
	
	//AddFrame(InsetFrame());
	
	btn.SetImage(img).SetLabel(title);
	btn.WhenAction = clicked;
	
	help.WhenAction = clicked;
	help.SetQTF(String("[g ") << DeQtf(helpText));
	
	int top = (btns.GetCount()-1) * (itemHeight + itemSpacing) + itemSpacing;
	ParentCtrl::Add(btn.LeftPosZ(itemSpacing, 112 + itemSpacing).TopPosZ(top , itemHeight));
	ParentCtrl::Add(help.VCenter(true).HSizePosZ(128 + itemSpacing*2, itemSpacing).TopPosZ(top , itemHeight));
	return *this;
}


int ActionBtns::GetHeight(){
	return Zy((btns.GetCount()) * (itemHeight + itemSpacing) + itemSpacing);
}



ActionDlg::ActionDlg(const char* title, int itemHeight, int itemSpacing):
		itemHeight(itemHeight),
		itemSpacing(itemSpacing),
		buttons(itemHeight, itemSpacing)
{
        CenterScreen().Sizeable().CloseBoxRejects();
        SetRect(GetWorkArea().CenterRect(Size(690, 520)));
        Title(title);
        AddFrame(sb);
        TopWindow::Add(buttons.SizePos());
        sb.SetLine(itemHeight/7);
        sb.WhenScroll = [=] {
            buttons.TopPos(-sb, buttons.GetSize().cy);
        };
        sb.AutoHide();
}

ActionDlg& ActionDlg::Add(const char *title, const Image &img, const char *help, Event<> clicked){
	buttons.Add(title, img, help, [=]{
		clicked();
		Close();
	});
	int height = buttons.GetHeight();
	sb.SetTotal(height);
	SetRect(GetWorkArea().CenterRect(Size(520, min(640, height))));
	return *this;
}

void ActionDlg::Layout()
{
    Size sz = buttons.GetSize();
    buttons.SetRect(0, 0, sz.cx, buttons.GetHeight());
    sb.SetPage(GetSize().cy);
}

void ActionDlg::MouseWheel(Point, int zdelta, dword)
{
    sb.Wheel(zdelta);
}