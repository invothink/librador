#include <Libcat/Librador.h>

// -:- ItemListDlg -:-

ItemListDlg::ItemListDlg(const char* title, SqlId &table)
{
	CtrlLayout(*this, title);
	
	queried = false;

	
	list.SetLineCy(GetStdFontCy() + DPI(2));
	list.SetTable(table);
	list.AddKey(ID);
	list.Appending().Removing().AskRemove();
	
	btnAdd.Hide();
	btnEdit.Hide();
	btnDel.Hide();
	btnView.Hide();
}

ItemListDlg& ItemListDlg::AddColumn(SqlId id, const char *text, int w)
{
	list.AddColumn(id, text, w).Edit(editor.Add());
	if (editor.GetCount() == 1)
		list.SetOrderBy(id);
	return *this;
}


ItemListDlg& ItemListDlg::OnAdd(Event<> clicked)
{
	btnAdd.SetImage(LibradorImg::icoAdd()) << [=]{
		clicked();
		list.StartInsert();
	};
	btnAdd.Show();
	return *this;
}

ItemListDlg& ItemListDlg::OnEdit(Event<> clicked)
{
	btnEdit.SetImage(LibradorImg::icoEdt()) << [=]{
		clicked();
		if(list.GetCount() > 0)
			list.StartEdit();
	};
	btnEdit.Show();
	return *this;
}

ItemListDlg& ItemListDlg::OnDelete(Event<> clicked)
{
	btnDel.SetImage(LibradorImg::icoDel()) << [=]{
		clicked();
		if(list.GetCount() > 0){
			if(list.IsEdit())
				list.CancelCursor();
			else{
				list.DoRemove();
				toggleButtons();
			}
		}
	};
	btnDel.Show();
	return *this;
}

ItemListDlg& ItemListDlg::OnView(Event<int> clicked)
{
	btnView.SetImage(LibradorImg::icoEye()) << [=]{
		int id = list.GetKey();
		if(id > 0)
			clicked(id);
	};
	btnView.Show();
	return *this;
}

ItemListDlg& ItemListDlg::OnChange(Event<> changed)
{
	list.Query();
	list.WhenAcceptEdit << changed;
	list.WhenArrayAction << changed;
	return *this;
}


void ItemListDlg::toggleButtons()
{
	bool enable = list.GetCount() > 0;
	btnEdit.Enable(enable);
	btnDel.Enable(enable);
}
