#include <Libcat/Librador.h>


Downloader::Downloader(String url, String path):url(url), path(path)
{
	http.MaxContentSize(INT_MAX);
	http.WhenContent = THISBACK(Content);
	http.WhenWait = http.WhenDo = THISBACK(ShowProgress);
	http.WhenStart = THISBACK(Start);
}

void Downloader::Start()
{
	if(out.IsOpen()) {
		out.Close();
		DeleteFile(path);
	}
	loaded = 0;
}

bool Downloader::Download()
{
	pi.Reset();
	http.New();
	http.Url(url).Execute();
	if(out.IsOpen())
		out.Close();
	if(!http.IsSuccess()) {
		DeleteFile(path);
		Exclamation("Download has failed.&\1" +
		            (http.IsError() ? http.GetErrorDesc()
		                            : AsString(http.GetStatusCode()) + ' ' + http.GetReasonPhrase()));
	}
	return http.IsSuccess();
}

void Downloader::Content(const void *ptr, int size)
{
	loaded += size;
	if(!out.IsOpen()) {
		RealizePath(path);
		out.Open(path);
	}
	out.Put(ptr, size);
}

void Downloader::ShowProgress()
{
	if(http.GetContentLength() >= 0) {
		pi.SetText("Downloading " + GetFileName(url));
		pi.Set((int)loaded, (int)http.GetContentLength());
	}
	else {
		pi.Set(0, 0);
		pi.SetText(http.GetPhaseName());
	}
	if(pi.Canceled())
		http.Abort();
}
