#include "../Librador.h"


CsvViewer::CsvViewer()
{
	CtrlLayout(*this, "LibCSV");
	MinimizeBox().MaximizeBox().Sizeable(),Maximize();
	

	filter.NullText("Type & enter to search..."). WhenEnter << [=] {OpenCsv(source, true);};
	btnClear.SetImage(LibradorImg::clear()) << [=] {filter.Clear(); OpenCsv(source, true);};
	btnSave << [=] { };
	btnSave.Hide();
	
	font = GetStdFont();
	String fonts[] = {"Noto Sans Malayalam", "Noto Sans Malayalam UI", "Karthika", "Manjari",
		"Lohit Malayalam", "Arial", "Liberation Sans", ""};
	for(int i=0; fonts[i] != ""; i++){
		int fi = Font::FindFaceNameIndex(fonts[i]);
		if(fi){
			font = FontZ(fi, 10);
			SetStdFont(font);
			break;
		}
	}
	
	list.AutoHideHorzSb();
	list.WhenLeftDouble << [&] {
		WriteClipboardUnicodeText(list.GetColumn(list.GetClickRow(), list.GetClickColumn()));
		PromptOK("Copied to clipboard");
	};
}

CsvViewer& CsvViewer::OpenCsv(String path, bool searching)
{
	FileIn csv(path);
	if (csv.IsError()){
		Exclamation(DeQtf(Format("Can't open file : %s", path)));
		return *this;
	}
	Title(Format("LibCSV - %s", path));
	source = path;
	list.Clear();
	
	Progress pi(Format("%s %s", searching?"Searching": "Reading", GetFileName(path)), -1);

	if (searching)
		csv.GetLine(); // skip header when searching
	else
		GetCsvLine(csv, ',', [&](String c){
			int w = 2;
			cw.Add(w);
			list.AddColumn(c.Begin(), w);
		});
	
	Vector<Value> row;
	row.SetCount(cw.GetCount());

	String query = ToLower(filter.GetText().ToString());
	
	while (!csv.IsEof()){
		bool add = query.IsEmpty();
		int i=0;
		GetCsvLine(csv, ',', [&](String c){
			cw[i] = max(cw[i], GetTextSize(c, font).cx + 2);
			row[i] = c;
			i++;
			if (!add) add = ToLower(c).Find(query) >= 0;
		});
		if (add) list.Add(row);
		if(pi.StepCanceled())
			break;
	}
	ColSize();
	lblStatus.SetText(Format("%s %d rows with %d columns.", searching?"Found":"Loaded",
		list.GetCount(), cw.GetCount()));
	return *this;
}

void CsvViewer::GetCsvLine(Stream& s, int separator, Event<String&> onCol)
{
	bool instring = false;
	byte dcs = GetDefaultCharset();
	byte charset = CHARSET_UTF8;
	String val;
	int i=0;
	for(;;) {
		int c = s.Get();
		if(c == '\n' && instring)
			val.Cat(c);
		else
		if(c == '\n' || c < 0) {
			if(val.GetCount()){
				val = ToCharset(dcs, val, charset);
				onCol(val);
			}
			return;
		}
		else
		if(c == separator && !instring) {
			val = ToCharset(dcs, val, charset);
			onCol(val);
			val.Clear();
		}
		else
		if(c == '\"') {
			if(instring && s.Term() == '\"') {
				s.Get();
				val.Cat('\"');
			}
			else
				instring = !instring;
		}
		else
		if(c != '\r')
			val.Cat(c);
	}
}

void CsvViewer::ColSize() {
	int maxw = list.GetSize().cx;
	int wx = 0;
	for(int i = 0; i < list.GetColumnCount(); i++){
		int w = min(maxw, cw[i]);
		wx += w;
		list.HeaderObject().SetTabRatio(i, w);
		list.HeaderObject().ShowTab(i);
	}
}






