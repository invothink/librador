#include "Librador.h"
#include "version.h"
#include <build_info.h>

#include "res.brc"

VectorMap<int, String> Cats, Langs, PUBS;
Index<WString> AutoTitles, AutoAuthors, AutoPublishers;

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

#ifdef __clang__
    #define COMPILER_VERSION ("Compiled with Clang version " \
    STRINGIZE_VALUE_OF(__clang_major__) "." \
    STRINGIZE_VALUE_OF(__clang_minor__) "." \
    STRINGIZE_VALUE_OF(__clang_patchlevel__))
#elif defined(__GNUC__) || defined(__GNUG__)
    #define COMPILER_VERSION ("Compiled with GCC version " \
    STRINGIZE_VALUE_OF(__GNUC__) "." \
    STRINGIZE_VALUE_OF(__GNUC_MINOR__) "." \
    STRINGIZE_VALUE_OF(__GNUC_PATCHLEVEL__))
#else
    #define COMPILER_VERSION "Unknown compiler"
#endif




Librador::Librador()
{
	CtrlLayout(*this, CfgStr("name").ToString() + " - " APP_NAME " ver" APP_VERSION);
	CtrlLayout(aboutTab);

	Icon(LibradorImg::icon());

	tabs.Add(homeTab.Background(White()).SizePos(), LibradorImg::home(), t_("Home"));
	tabs.Add(booksTab.Background(White()).SizePos(), LibradorImg::book(), t_("Books"));
	tabs.Add(membersTab.Background(White()).SizePos(), LibradorImg::user(), t_("Members"));
	tabs.Add(advancedTab.Background(White()).SizePos(), LibradorImg::hammerscrew(), t_("Advanced"));
	tabs.Add(aboutTab.Background(White()).SizePos(), LibradorImg::info(), t_("About"));
	tabs << [&]{
		switch(tabs.Get()){
			case 0:
				homeTab.UpdateStats();
				break;
		}
	};
	//tabs.Set(4);
	homeTab.UpdateStats();

	String aboutTxt = String()<<"[= "
				"[ @@iml:400*400`LibradorImg::icon`] &"
				"[* &" APP_NAME "]"
				"[ &v" APP_VERSION "]"
				"[ &(C) Invothink ]"

				"[* &&Customer Care ]"
				"[ &(+91) 8714612394 ]"
				"[ &(+91) 8714612395 ]"
				"[ &libcat@invothink.com ]"
			" ]";
	aboutTab.about.NoSb() <<= aboutTxt;

	if(DevMachine)
		aboutTab.devImg.SetImage(StreamRaster::LoadStringAny(String(calvin, calvin_length)));
	else
		aboutTab.devImg.Hide();

	populateMenu(ReportConfig, menu);

	NoToolWindow().Sizeable().Zoomable();
	Maximize();
}

Bar::Item* Librador::populateMenu(Value items, Bar &bar){
	Bar::Item *mi = 0;
	for(int i=0; i< items.GetCount(); i++){
		ValueMap m = items[i];
		if(m["name"].ToString().StartsWith("--"))
			bar.Separator();
		else if (m["submenu"].GetCount() > 0)
			mi = &bar.Sub(m["name"].ToString(), [=](Bar& sub) {
				populateMenu(m["submenu"], sub);
			});
		else
			mi = &bar.Add(m["name"].ToString(), THISBACK1(RunReport, m));
	}
	return mi;
}

void Librador::ReportThread(String *cmd, Vector<String> *args)
{
	String cmdOutput, out;
	reportProcess.Start(*cmd, *args);
	if (!reportCancelled && reportProcess.Finish(cmdOutput) && !cmdOutput.IsEmpty())
		ErrorOK(DeQtf(cmdOutput));
}

void Librador::RunReport(ValueMap m)
{
	//ValueMap m = ReportConfig[idx];
	String cmd = ExpandPath(m["cmd"]);
	Vector<String> args;

	for(int i=0; i<m["args"].GetCount(); i++){
		args.Add(ExpandPath(m["args"][i]));
		//args[i].Replace("$ROOT", rootdir);
	}

	if(!m["ask"].IsVoid()){
		ValueArray fields;
		if(m["ask"].GetCount()==0)
			fields.Add(m["ask"]);
		else
			fields = m["ask"];
		for(int i=0; i<fields.GetCount(); i++)
			if(!AskQuestion(m["name"], fields[i], args))
				return;
	}

	Progress progress(Sprintf("Generating %s...", m["name"].ToString().Begin()));
	reportCancelled = false;
	librador->advancedTab.LogLine(String("report: ")<<cmd<<" "<<Join(args, " "));
	Thread().Run(THISBACK2(ReportThread, &cmd, &args));
	Sleep(100);
	while(reportProcess.IsRunning() && !reportCancelled){
		if(progress.StepCanceled(10)){
			reportCancelled = true;
			reportProcess.Kill();
		}
		Sleep(100);
	}
}