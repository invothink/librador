#include "Librador.h"



/*
 TODO:	This way of import is rudimentary, a documentation of existing stucture and design
		decisions will be very much appreciated
*/




void ImportData(ValueMap &data){
	
/*	ValueMap data = ParseJSON(
						FileExists(GetExeDirFile("data.json"))
						?	LoadFile(GetExeDirFile("data.json"))
						:	LoadFile(GetDesktopFolder()+"\\data.json"));*/
	Progress pi("Importing data");
	pi.Show();
	VectorMap<int, int> catids, bookids, memids, pubids,lngids, mgroupids;
	Index<int> accnos, memnos;
	String report;
	report << APP_NAME " v" APP_VERSION <<"\n";

	report << "Importing data on " << GetSysDate() << " " << GetSysTime() << "\n\n";
	Value cats = data["categories"];
	Vector<Vector<Value>> flaggedBooks, flaggedMembers;
	
	Sql sql(Sqlite3());
	sql.Begin();
	
	pi.SetText("Importing categories...");
	for(int i=0; i<cats.GetCount(); i++){
		Value cat = cats[i];
		sql * Insert(CATEGORY)
			(NAME, cat["name"])
			(CODE, cat["code"])
		;
		catids.Add(cat["id"],  SQL.GetInsertedId());
		pi.Step();
	}
	report << "Import Categories...\n";
	report << "Found " << catids.GetCount() <<" records\n\n";

	
	pi.SetText("Importing publishers...");
	for(int i=0; i<data["publishers"].GetCount(); i++){
		Value pub = data["publishers"][i];
		sql * Insert(PUBLISHER)
			(NAME, pub["name"])
		;
		pubids.Add(pub["id"],  SQL.GetInsertedId());
		pi.Step();
	}
	report << "Import Publishers...\n";
	report << "Found " << pubids.GetCount() <<" records\n\n";
	
	
	pi.SetText("Importing languages...");
	for(int i=0; i<data["languages"].GetCount(); i++){
		Value lng = data["languages"][i];
		sql * Insert(LANGUAGE)
			(NAME, lng["name"])
		;
		lngids.Add(lng["id"],  SQL.GetInsertedId());
		pi.Step();
	}
	report << "Import Languages...\n";
	report << "Found " << lngids.GetCount() <<" records\n\n";	

	
	report << "Import Books...\n";
	pi.SetText("Importing books...");
	
	for(int i=0; i<data["books"].GetCount(); i++){
		Value book = data["books"][i];
		
		int accno = ScanInt(TrimBoth(~book["acc"]));
		
		if(accno<1 || accno > 200000000){
			report << "\tSkipped: Invalid Accession No '" << ~book["acc"] <<"'\n";
			flaggedBooks.Add() << "Invalid Acc #"<<book["acc"]<<book["title"]<<book["author"];
			continue;
		}
		if(accnos.Find(accno) > -1){
			report << "\tSkipped: Duplicate Accession No '" << ~book["acc"] <<"'\n";
			flaggedBooks.Add() << "Duplicate Acc #"<<book["acc"]<<book["title"]<<book["author"];
			continue;
		}
		accnos.Add(accno);
		sql * Insert(BOOK)
			(TITLE, ~book["title"])
			(CAT_ID, catids.GetAdd(book["catid"]))
			(LANG_ID, lngids.GetAdd(book["langid"]))
			(AUTHOR, book["author"])
			(PRICE, (int)ScanDouble(~book["price"]))
			(ACCN_NO, accno)
			(CALL_NO, book["call"])
			(PUB_ID, pubids.GetAdd(book["pubid"]))
			(SHELF, book["shelf"])
			(DDCCODE, book["ddc"])
			(BARCODE, book["barcode"])
		;
		Value bid = book["id"];
		bookids.Add(IntValue(book["id"]),  sql.GetInsertedId());
		pi.Step();
	}
	
	
	
	
	report << "Found " << bookids.GetCount() <<" records\n\n";

	
	report << "Import Member Groups...\n";
	pi.SetText("Importing members...");
	for(int i=0; i<data["mgroups"].GetCount(); i++){
		Value mgroup = data["mgroups"][i];
		sql * Insert(MGROUP)
			(NAME, mgroup["name"])
			(CODE, mgroup["code"])
		;
		mgroupids.Add(mgroup["id"],  sql.GetInsertedId());
		pi.Step();
	}
	report << "Found " << mgroupids.GetCount() <<" records\n\n";
	
	
	report << "Import Members...\n";
	for(int i=0; i<data["members"].GetCount(); i++){
		Value memb = data["members"][i];
		
		int memno = IntValue(memb["id"]);
		if(memno<1 || memno > 100000000){
			report << "\tSkipped: Invalid Member No '" << ~memb["id"] <<"'\n";
			flaggedMembers.Add()<< "Invalid Member #"<<memb["id"]<<memb["name"]<<memb["dob"];
			continue;
		}
		if(memnos.Find(memno) > -1){
			report << "\tSkipped: Duplicate Member No '" << ~memb["id"] <<"'\n";
			flaggedMembers.Add()<< "Duplicate Member #"<<memb["id"]<<memb["name"]<<memb["dob"];
			continue;
		}
		memnos.Add(memno);
		
		
		sql * Insert(MEMBER)
			(MEMBER_NO, memno)
			(NAME, memb["name"])
			(DOB, ScanDate("dmy", ~memb["dob"]))
			(GENDER, memb["gender"])
			(ADDRESS, memb["addr"])
			(PHONE, memb["phone"])
			(EMAIL, memb["email"])
			(BLOOD, memb["blood"])
			(MGROUP_ID, memb["groupid"].IsNull() ? 0 : mgroupids.GetAdd(memb["groupid"]))
			(JOB, memb["job"])
			(BARCODE, memb["barcode"])
		;
		
		memids.Add(memno,  sql.GetInsertedId());
		pi.Step();
	}
	report << "Found " << memids.GetCount() <<" records\n\n";
	
	
	report << "Import Issues...\n";
	pi.SetText("Importing issuals...");
	int issueCount = 0;
	for(int i=0; i<data["issues"].GetCount(); i++){
		Value issue = data["issues"][i];
		if (memids.Find(issue["memid"]) < 0 || bookids.Find(issue["bookid"])<0){
			RLOG(String()<<"Invalid record on import: "<<issue["memid"]<<", "<<issue["bookid"]);
			continue;
		}
		sql * Insert(LEDGER)
			(MEMBER_ID, memids.GetAdd(issue["memid"]))
			(BOOK_ID, bookids.GetAdd(issue["bookid"]))
			(ISSUED, issue["issued"])
			(DUE, issue["due"])
			(RETURNED, issue["returned"])
		;
		issueCount++;
	}
	
	sql.Commit();
	
	report << "Found " << issueCount <<" records\n\n";
	
	String dest = GetDesktopFolder()+"/" APP_NAME;
	RealizeDirectory(dest);
	SaveFile (dest + "/import-summary.log", report);
	report << "This report is saved to desktop in " APP_NAME "/import-summary.log";
	if(!flaggedBooks.IsEmpty())
		SaveCSV(dest + "/import-books.csv", flaggedBooks, "reason,acc-no,title,author");
	if(!flaggedMembers.IsEmpty())
		SaveCSV(dest + "/import-members.csv", flaggedMembers, "reason,mem-no,name,dob");

	PromptOK(DeQtf(report));
}

