#include "Librador.h"


#define IMAGECLASS LibradorImg
#define IMAGEFILE <Libcat/Librador.iml>
#include <Draw/iml_source.h>

//#define TFILE <Libcat/Librador.t>
//#include <Core/t.h>

//#include <Web/Web.h>

#include "res.h"
#include "res.brc"


bool DevMachine;

int BookCount;
Librador *librador;
Value ReportConfig;
String build;
String NotAvaliable = "NA";
String loadLCP;
CachedId CatName, PubName, LangName, BloodGroups, Genders, MGroupName;

bool SchoolVariant = false;



//enum { ENGLISH, MALAYALAM };
enum {
    ENGLISH =	LNG_( 'E', 'N', 'U', 'S'),
    MALAYALAM = LNG_( 'M', 'L', 'I', 'N')
};

struct SplashDlg : WithSplashDlg<TopWindow> {
	
	typedef SplashDlg CLASSNAME;
	SplashDlg(){
		CtrlLayout(*this, APP_NAME " - "  APP_VERSION);
		Image simg = StreamRaster::LoadStringAny(String(splash_en, splash_en_length));
		img.SetImage(simg);
		SetRect(GetWorkArea().CenterRect(simg.GetSize()));
		
		
		#ifndef PLATFORM_POSIX
		PopUp(NULL, true, false, false, false);
		#endif
	}
	void operator()(const char *msg)	{txt = msg; Ctrl::ProcessEvents();}
	void Done()					{SetTimeCallback(2000,THISBACK(Close));}
	
};




void SetLang(String lang){
	SetStdFont(Font(Font::FindFaceNameIndex(CfgStr("fontFace")), CfgInt("fontSize")));
	return;
	if(lang.IsEmpty()){
		if(DevMachine){
			WithLangSelDlg<TopWindow> dlg;
			CtrlLayoutOKCancel(dlg, t_(APP_NAME "-" APP_VERSION));
			lang = dlg.ExecuteOK()?LNGAsText(MALAYALAM):LNGAsText(ENGLISH);
		}else
			lang = LNGAsText(ENGLISH);
	}
	int lng = LNGFromText(lang);
	SetLanguage(SetLNGCharset(lng, CHARSET_UTF8));
	CfgSetStr("lang", lang);
}

void ParseCmdline(){
	const Vector<String> &args = CommandLine();
	if(args.size() < 1)
		return;
	
	// Show SQL UI for .db files
	if(args[0].EndsWith(".db")){
		Sqlite3Session sqlite3;
		if (sqlite3.Open(args[0])){
			SQLCommander(sqlite3);
			
		} else
			Exclamation(DeQtf(Format("Unknown format: %s", args[0])));
		Exit(0);
	}
	
	// Show CSV UI for .csv files
	if(args[0].EndsWith(".csv")){
		CsvViewer().OpenCsv(args[0]).Run(true);
		Exit(0);
	}
	
	// Store lcp for later import
	if(args[0].EndsWith(".lcp"))
		loadLCP = args[0];
}

void SetVariants(){
	SchoolVariant = CfgInt("type") == TypeSchoolLib;
}

void UpdateCache(){

	Sql sql;
	
	
	MGroupName.WhenPopulate<<[&]{
		Sql sql(Sqlite3());
		MGroupName.Clear();
		sql * Select(ID, NAME).From(MGROUP);
		
		while(sql.Fetch())
			MGroupName.Set(sql[ID], sql[NAME]);
	};
	MGroupName.Populate();
		
	
	CatName.WhenPopulate<<[&]{
		Sql sql(Sqlite3());
		sql * Select(ID, NAME).From(CATEGORY);
		while(sql.Fetch())
			CatName.Set(sql[ID], sql[NAME]);
	};
	CatName.Populate();
	
	PubName.WhenPopulate<<[&]{
		Sql sql(Sqlite3());
		sql * Select(ID, NAME).From(PUBLISHER);
		while(sql.Fetch())
			PubName.Set(sql[ID], sql[NAME]);
	};
	PubName.Populate();
	
	LangName.WhenPopulate<<[&]{
		Sql sql(Sqlite3());
		sql * Select(ID, NAME).From(LANGUAGE);
		while(sql.Fetch())
			LangName.Set(sql[ID], sql[NAME]);
	};
	LangName.Populate();
	
	BloodGroups.Set(0, "NA").Set(1, "A+").Set(2, "A-").Set(3, "A1+").Set(4, "A1-")
			  .Set(5, "A1B+").Set(6, "A1B-").Set(7, "A2+").Set(8, "A2-").Set(9, "A2B+")
			  .Set(10, "A2B-").Set(11, "AB+").Set(12, "AB-").Set(13, "B+").Set(14, "B-")
			  .Set(15, "Bombay").Set(16, "INRA").Set(17, "O+").Set(18, "O-");
	
	Genders.Set(0, "NA").Set(GENDER_FEMALE, t_("Female")).Set(GENDER_MALE, t_("Male"))
		  .Set(GENDER_OTHER, t_("Other"));
	
	sql * (Select(Distinct(TITLE), AUTHOR).From(BOOK) | Select(Distinct(TITLE), AUTHOR).From(AUTOBOOK));
	
	while(sql.Fetch()){
		AutoTitles.Add(sql[TITLE]);
		AutoAuthors.FindAdd(sql[AUTHOR]);
	}
}


void EnumWins(Ctrl *parent, String prefix){
	for(int i=0; i < parent->GetChildCount(); i ++){
		Ctrl *c = parent->GetIndexChild(i);
		RLOG(String(prefix)<<i<<". "<<c->GetDesc()<<": "<<c->Name( )<<":");
		RLOG(String(prefix)<<"    "<<c->GetLayoutId());
		EnumWins(c, prefix + "  ");
	}
}



void StartApp(){
	CfgLoad();
	SetVariants();
	SetEnvVars();
	SetLang(CfgStr("lang"));

	SplashDlg splash;
	splash("Reading config...");
		
	
	if(!CfgHas("name") && !DoRegisteration())
		return;
	
	
	String addressline = CfgStr("address");
	SetEnvVar("name", CfgStr("name"));
	SetEnvVar("regno", CfgStr("regno"));
	SetEnvVar("addressline", Format("%s, Phone: %s",CfgStr("address"), CfgStr("phone")));
	SetEnvVar("aid", CfgStr("phone"));
	SetEnvVar("pid", CfgStr("pid"));

	SetDateFormat("%3:02d-%2:02d-%1:4d");
	SetDateScan("dmy");

	
	LoadJSON(AppPath("reports.json"));
	
	ReportConfig = ReadConfig("reports.json");

	splash("Loading data...");
	Sleep(500);
	
	
	
	Sqlite3Session &sqlSession = Sqlite3();
	
	if(!loadLCP.IsEmpty()){
		switch(ImportPackage(loadLCP)){
			case UPDATE_RESTART:
				SysDetached(GetExeFilePath());
			case UPDATE_EXIT:
				return;
		}
	}

		
	UpdateCache();
	
	//RLOG(String()<<"AutoAuthors: "<<AutoAuthors.GetCount()<<" AutoAuthors: "<<AutoTitles.GetCount()<<"\n");

	
	splash("Checking update...");
	Thread::Start([&](){StartUpdateThread();}, true);

	splash(t_("© " ASSTRING(APP_YEAR) " Invothink Systems LLP"));
	splash.Close();
	{
		librador = new Librador();
		//EnumWins(librador, "  ");
		librador->Run(true);
		CfgSave();
		delete librador;
	}
	Thread::BeginShutdownThreads();
}

GUI_APP_MAIN
{
	DevMachine = FileExists(GetExeDirFile("shit.happens"));
	
	ParseCmdline();
	EnsureSingleInstance(StartApp);

}
