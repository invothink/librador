create view if not exists ISSUES as 
	select ledger.ID,ledger.DTIME, MEMBER_ID, MEMBER_NO, NAME, PHONE, ADDRESS, 
		BOOK_ID, ACCN_NO, CALL_NO, TITLE, AUTHOR, ACCN_NO, CAT_ID, PUB_ID, ISSUED, DUE, 
		RETURNED, member.BARCODE, book.STATUS from ledger
	left join member on member_id = member.id 
	left join book on book_id = book.id;
		
 create view if not exists BOOKCAT as 
	select BOOK.*, CATEGORY.NAME as CATEGORY, PUBLISHER.NAME as PUBLISHER from BOOK 
	left join CATEGORY on BOOK.CAT_ID = CATEGORY.ID
	left join PUBLISHER on PUB_ID = PUBLISHER.ID;
