#include <Libcat/Librador.h>

void SysInfo();

AdvancedTab::AdvancedTab()
{
	CtrlLayout(*this);
	
	btnSysInfo.SetImage(LibradorImg::info()) << [=] {SysInfo();};
	

	btnBackup.SetImage(LibradorImg::usb()) << [=] { Backup(); };
	btnRestore.SetImage(LibradorImg::ddrive()) << [=] { Restore(); };
	
	btnDownload.SetImage(LibradorImg::download()) << [=] { Download(); };
	btnUpload.SetImage(LibradorImg::upload()) << [=] { Upload(); };
	
	btnSettings.SetImage(LibradorImg::cog()) << [=] {
		ConfigDlg(config).Execute();
	};
	
	list.AddColumn("Time", 150);
	list.AddColumn("Log", 900);
	list.ColumnWidths("107 609");
	logFile.Open(ConfigPath("sync.log"), BlockStream::READWRITE);
	while(!logFile.IsEof()){
		String ln = logFile.GetLine();
		list.Add(ln.Mid(0, 19), ln.Mid(20));
	}
	//btnDownload.Hide();
	btnRestore.Hide();
	list.ScrollEnd();
	
	//if(!DevMachine) btnUpload.Disable();
}

String CompilerInfo(){
	return String("")
		#ifdef __clang__
			<< Format("clang %d.%d.%d", __clang_major__, __clang_minor__, __clang_patchlevel__)
		#elif defined(__GNUC__) || defined(__GNUG__)
			<< Format("gcc %d.%d.%d", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)
		#else
			<< "NA"
		#endif
	;
}

void SysInfo() {
	Vector<String> lines;
	
	lines
		<<("{{1:3-2*@N [* " APP_NAME " Info]:: ::")
		;
	
	lines
		<< (String(" Version :: ") << APP_VERSION << "::")
		<< (String(" Compiler :: ") << DeQtf(CompilerInfo()) << "::")
		<< (String(" App Path :: ") << DeQtf(AppPath("")) << "::")
		<< (String(" Data Path :: ") << DeQtf(DataPath("")) << "::")
		<< (String(" DB Path :: ") << DeQtf(GetEnv("DB")) << "::")
		<< (String(" Config Path :: ") << DeQtf(ConfigPath("config.json")) << "::")
	;
	
	auto si = OSInfo();
	lines << "-2 [* &OS Info]:: ::";
	for(auto && k: si.GetKeys()){
			lines << (String(" ")<<DeQtf(InitCaps(~k))<<" :: "<<DeQtf(InitCaps(si[k].ToString()))<< "::");
	}

	lines << " }}";
	PromptOK((Join(lines, "")));
	
}

void AdvancedTab::LogLine(String msg)
{
	String time = FormatTime(GetSysTime(), "DD-MM-YYYY hh:mm:ss");
	logFile.Put(time);
	logFile.Put(" ");
	logFile.Put(msg);
	logFile.Put('\n');
	list.Add(time, msg);
}


void AdvancedTab::Backup()
{
	FileSelector fsel;
	fsel.Set(Format("%s/libcat-%s.lcp",GetFileDirectory(CfgStr("lastBakup")), 
		FormatTime(GetSysTime(), "DD-MM-YYYY-HH-mm")));
	if (fsel.ExecuteSaveAs("Select backup directory")){
		if(BackupDB(~fsel)){
			LogLine(Format("Created backup to %s", ~fsel));
			CfgSetStr("lastBakup", ~fsel);
			CfgSave();
		}else{
			LogLine(Format("Cancelled backup to %s", ~fsel));
			DeleteFile(~fsel);
		}
	}
}



void AdvancedTab::Restore()
{
	LogLine("Restore started...");
}

