#include <Libcat/Librador.h>

void AdvancedTab::UpdateSynced(const Value &ret){
	Sql sql;
	ValueMap ids = ret["ids"];
	StringBuffer buf;
	for(int i=0; i<ids.GetCount(); i++){
		Value table = ids.GetKey(i);
		ValueMap time_ids = ids.GetValue(i);
		int processed = 0;
		for(int j=0; j<time_ids.GetCount(); j++){
			buf.Clear();
			buf << Format("update %s set STIME = '%s' where ID in (", table, time_ids.GetKey(j));
			ValueArray syncedIds = time_ids.GetValue(j);
			for (int k=0; k < syncedIds.GetCount(); k++){
				if(k != 0) buf <<',';
				buf << syncedIds[k];
			}
			buf << ");\n";
			sql.Execute(buf.begin());
			processed += sql.GetRowsProcessed();
		}
		LogLine(Format("Updated %d %s", processed, table.ToString()));
	}
}

void AdvancedTab::Upload()
{
	if (!btnUpload.IsEnabled()) { return; }
	ValueMap ret = ParseJSON(Sys(ExpandPath("$PYTHON $ROOT/scripts/sync.py -v")));
	if (ret.Find("ver") >= 0 ){
		LogLine(String()<<"Upload started (client ver: "<<ret["ver"]<<")");
		LocalProcess proc;
		Progress pi("Uploading data...");
		String out, buf;
		proc.Start(ExpandPath("$PYTHON $ROOT/scripts/sync.py $DB"));
		while(!pi.StepCanceled() && proc.Read(buf)){
			out.Cat(buf);
			Sleep(30);
		}
		if (pi.Canceled())
		{
			return;
		}
		//RLOG(out);
		ret = ParseJSON(out);
		if (ret.Find("err") >= 0){
			String err = ret["err"];
			if (err[0] != '<'){
				LogLine(err);
				if(ret.Find("ids") >=0){
					pi.SetText("Saving synced data...");
					UpdateSynced(ret);
				}
			}else
				LogLine(String("Sync failed: ") << err);
		}else
			LogLine("Unexpected sync");
	}else{
		PromptOK("Failed to start sync, sync disabled");
		btnUpload.Disable();
	}
	list.ScrollEnd();
}


void RegisterOPAC(){
	// attempt passwordless auth or pre-existing token auth in case server allows it for user-name
	Value result = APIv2("RegisterOPAC")
		("config", config.Values)
		(AuthParams)
		.Execute();
	
	if (!result.IsError()){
		CfgSetStr("authToken", result["authToken"]);
	} else {
		String error = GetErrorText(result);
		if(!error.StartsWith("Http request failed") && !error.IsEmpty())
			Exclamation(DeQtf(error));
	}
}


void CheckUpdate(){
	DownloadAndRun("update", false);
}

void StartUpdateThread(){
	int i = 0;
	for(;;){
		if(Thread::IsShutdownThreads())
			break;
		
		Sleep(100);
		if (i++ % 4200 != 0) // 100*4200ms = 7 mins
			continue;
		
		if(!CfgHas("authToken"))
			RegisterOPAC();
		CheckUpdate();
		i=1;
	}
}

