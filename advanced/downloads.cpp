#include <Libcat/Librador.h>
#include <Core/Rpc/Rpc.h>


void AdvancedTab::Download()
{
	String code = "";
	
	if (!EditText(code, "Download LCP", "Enter the download code", 11))
		return;
	
	DownloadAndRun(code, true);
}


int RunUpdate(Value result, String file){
	String cmd;
	String name = result["name"].IsNull()?GetFileName(~result["url"]):~result["name"];
	
	if(PromptYesNo(String()<<"A new version of libcat (" << DeQtf(~result["version"]) << ") is available.&&Do you want to install this update ?") != 0) {
		if(file.EndsWith("tar.xz")){
			cmd = GetExeDirFile("bin/python.exe") << " " <<
				GetExeDirFile("scripts/update.py") << " " <<
				file;
		}
		else if(file.EndsWith(".exe")) {
			cmd = String(file) << " /SILENT";
		}
		else if(file.EndsWith(".deb")){
			Vector<String> cmds;
			cmds <<
				"echo \"Please enter your password to start installation (password will not be visible)\"" <<
				"sudo apt update" <<
				
				"sudo apt purge -y libcat" <<
				"sudo apt install -y " + file <<
				"echo -e \"\\\\nUPDATE SUCCESSFULL\\\\n\\\\nPress enter to start libcat...\"" <<
				"read" <<
				GetExeFilePath()
			;
			cmd = "x-terminal-emulator -e '" + Join(cmds, "&&") + "'";
		}
		
		if(!cmd.IsEmpty()){
			SysDetached(cmd);
			return UPDATE_EXIT;
		}
	}
	
	
	if(!cmd.IsEmpty()){
		SysDetached(cmd);
		return UPDATE_EXIT;
	}
	return UPDATE_CONTINUE;
}

void DownloadAndRun(String code, bool showError=true){
	Value result = APIv2("DownloadInfo")
			("osinfo", OSInfo())
			("code",  code)
			(AuthParams)
		.Execute();
	//RLOG(result);
	PostCallback([=]{
		//PromptOK(String(CfgStr("libcatAPIv2"))<<":"<<~result);
		if (!result.IsError() &&
			PromptYesNo(String()<<"A new version of libcat (" << DeQtf(~result["version"]) << ") is available.&&Do you want to install this update ?") == IDOK){
				String dst = TempFile(String("lc-") << GetSysTime().Get() << "-" << GetFileName(~result["url"]));
				if(Downloader(~result["url"], dst).Download()) {
					int ret;
					
					if (result["type"] == "lcp")
						ret = ImportPackage(dst);
					else if (result["type"] == "update")
						ret = RunUpdate(result, dst);
					else {
						Exclamation(String() << "Unknown file type: " << DeQtf(~result["type"]));
						return;
					}
					
					switch(ret){
						case UPDATE_RESTART:
							SysDetached(GetExeFilePath());
							break;
						case UPDATE_CONTINUE:
							return;
							break;
						case UPDATE_EXIT:
							//Exit(0);
							librador->Close();
							return;
					}
				}
	
		} else if(showError){
			String error = GetErrorText(result);
			if(error.StartsWith("Http request failed")) {
				Exclamation("Internet connection failed. Please ensure you have good internet connection!");
			} else
				Exclamation(DeQtf(error));
		}
	});
}

