#include "Librador.h"

int IntValue(Value v){
	if(v.GetType() == DOUBLE_V || v.GetType() == INT_V){
		return int(v);
	}
	return ScanInt(TrimBoth(~v));
}

String AppPath(String filename)	{
	return GetExeDirFile(filename);
}

String DataPath(String filename) {
	#ifdef PLATFORM_POSIX
		String dir = GetHomeDirFile(".libcat");
		if(!FileExists(dir))
			DirectoryCreate(dir);
		if(filename.IsEmpty())
			return dir;
		return AppendFileName(dir, filename);
	#else
		return AppPath(filename);
	#endif
}

String ConfigPath(String filename) {
	#ifdef PLATFORM_POSIX
		return DataPath(filename);
	#else
		return ConfigFile(filename);
	#endif
}

Value LoadJSON(String filename){
	return ParseJSON(LoadFile(filename));
}


bool AskQuestion(String title, String field, Vector<String> &args){
	Vector<String> f = Split(field, "|");
	if(f[0] == "category"){
		WithMultiselDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];
		for(int i=0; i < CatName.GetCount(); i++){
			String item = CatName(i);
			if(!item.IsEmpty())
				dlg.options.Add(i, item);
		}
		if(dlg.options.GetCount() == 0)
			dlg.options.Add(0,t_("None Avaliable"));
		dlg.options.SetIndex(0);
		if(dlg.Execute() != IDOK)
			return false;
		args.Add(FormatInt(dlg.options.GetData()));
	} else
	if(f[0] == "mgroup"){
		WithMultiselDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];

		Sql sql;
		sql * Select(ID, NAME).From(MGROUP);
		while(sql.Fetch())
			dlg.options.Add(sql[ID], sql[NAME]);

		if(dlg.options.GetCount() == 0)
			dlg.options.Add(0,t_("None Avaliable"));
		dlg.options.SetIndex(0);
		if(dlg.Execute() != IDOK)
			return false;
		args.Add(FormatInt(dlg.options.GetData()));
	} else
	if(f[0] == "select"){
		WithMultiselDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];
		for(int i=2; i < f.GetCount(); i++)
			dlg.options.Add(i, f[i]);

		if(dlg.options.GetCount() == 0)
			dlg.options.Add(0,t_("None Avaliable"));
		dlg.options.SetIndex(0);
		if(dlg.Execute() != IDOK)
			return false;
		args.Add(String("\"")<<dlg.options.Get()<<"\"");
	} else
	if(f[0] == "sqlselect"){
		// sqlselect|Pick a deleted book|id,title,author|book|edition = ''
		WithSqlSelectDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];
		Vector<String>  cols = Split(f[2], ",");
		SqlId  key(cols[0]);
		if (cols.GetCount() > 1){
			dlg.list.AddKey(key);
			for(int i=1; i < cols.GetCount(); i++)
				dlg.list.AddColumn(SqlId(cols[i]), InitCaps(cols[i]));
			dlg.list.SetTable(f[3]);
			if (f.GetCount() < 5)
				dlg.list.Query();
			else
				dlg.list.Query(SqlBool(f[4], SqlS::PRIORITY::TRUEVAL));
			if(dlg.Execute() != IDOK)
				return false;
			args.Add(String()<<dlg.list.Get(key));
		}else
			args.Add("error: expecting key,col1,col2...");
	} else
	if(f[0] == "daterange"){
		WithDateRangeDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];
		if(dlg.Execute() != IDOK)
			return false;
		args.Add(FormatDate(dlg.from.GetDate(), "YYYY-MM-DD"));
		args.Add(FormatDate(dlg.to.GetDate(), "YYYY-MM-DD"));
	} else
	if(f[0] == "date"){
		WithDateDlg<TopWindow> dlg;
		CtrlLayoutOKCancel(dlg, title);
		dlg.label = f[1];
		if(dlg.Execute() != IDOK)
			return false;
		args.Add(FormatDate(dlg.from.GetDate(), "YYYY-MM-DD"));
	} else
	if(f[0] == "text"){
		String val;
		if(!EditText(val, title, f[1]))
			return false;
		args.Add(val);
	} else
	if(f[0] == "number"){
		double val=0;
		if(!EditNumber(val, title, f[1]))
			return false;
		args.Add(FormatDouble(val));

	} else
	if(f[0] == "file"){
		String val;
		FileSelector fsel;
		if(!fsel.ExecuteSaveAs(f[1]) || fsel.GetCount()==0)
			return false;
		val = ~fsel;
		val.Replace("\\", "/");
		args.Add(val);
	}
	return true;
}


void MakeMultiSelect(SuggestCtrl &ctrl){
	ctrl.Delimiter(',');
	ctrl.WhenAction << [&]{
		for(int i=0; i< ctrl.GetLength()-1; i++)
			if(ctrl.GetChar(i) == ',' && ctrl.GetChar(i+1) != ' ')
				ctrl.Insert(++i, " ");
	};
}

VectorMap<String, String>& EnvMap()
{
	static VectorMap<String, String> x;
	return x;
}

bool SetEnvVar(const char *id, const char *val) 
{
	EnvMap().GetAdd(id) = String(val);
#ifdef PLATFORM_POSIX
	return setenv(id, val, 1) == 0;
#else

	//return SetEnvironmentVariable("PYTHON", "asdasd") != 0;
	return putenv(String(id) + "=" + String(val)) == 0;
#endif
}

bool IsValidLCCv1(String str){
	/*
		7851-AT1499
		7852-AT177
	*/
	//PromptOK(DeQtf(str));

	if(str.GetLength() < 8)
		return false;

	if(!isdigit(str[0]) || !isdigit(str[1]) || !isdigit(str[2]) || !isdigit(str[3]))
		return false;

	if(str[4] != '-')
		return false;

	if(!IsUpper(str[5]) || !IsUpper(str[6]))
		return false;

	return true;
}

bool ValidBarcode(String str){
	if(IsValidLCCv1(str))
		return true;

	for (int i=0; i< str.GetCount(); i++){
		if(!isdigit(str[i]))
			return false;
	}
	return true;
}


String GetEnvVar(const char *id){
	return 	EnvMap().Get(id, "");
}

void SetEnvVars(){
	static String rootdir = GetFileFolder(GetExeFilePath());
	#if PLATFORM_POSIX
		static String python = TrimBoth(Sys("which python3"));
		static String wkhtml = GetExeDirFile("bin/wkhtmltopdf"); //System("env wkhtmltopdf");
	#else
		String python = GetExeDirFile("bin\\python.exe");
		String wkhtml = GetExeDirFile("bin\\wkhtmltopdf.exe");
	#endif
	SetEnvVar("ROOT", rootdir);
	SetEnvVar("PYTHON", python);
	SetEnvVar("WKHTML", wkhtml);
	SetEnvVar("DATADIR", DataPath(""));
	SetEnvVar("DB", DataPath("libcat.db"));
}

String ExpandPath(String path){
	String str = path;
	str.Replace("$ROOT", GetEnv("ROOT"));
	str.Replace("$PYTHON", GetEnv("PYTHON"));
	str.Replace("$WKHTML", GetEnv("WKHTML"));
	str.Replace("$DATADIR", GetEnv("DATADIR"));
	str.Replace("$DB", GetEnv("DB"));
	return str;
}


bool SaveCSV(const char *path, Vector<Vector<Value>> &rows, String header, String sep){
	FileOut csv(path);
	if(csv.IsOpen()){
		static unsigned char bom[] = {0xEF, 0xBB, 0xBF};
		csv.Put(bom, 3);
		int fields = rows[0].GetCount();
		if(!header.IsEmpty())
			csv.PutLine(header);
		if(rows.GetCount())
			for(int i=0; i<rows.GetCount(); i++){
				Vector<Value>& cols = rows[i];
				for(int j=0; j<cols.GetCount(); j++){
					if (IsNumber(cols[j]))
						csv.Put(ToCharset(CHARSET_UTF8, AsString(cols[j])));
					else
						csv.Put(ToCharset(CHARSET_UTF8, CsvString(cols[j])));
					if(j != cols.GetCount()-1)
						csv.Put(sep);
					else
						csv.Put('\n');
				}
			}
		return true;
	}
	return false;
}


String OSName(){
	#if PLATFORM_POSIX
		String os = String() << Sys("lsb_release -ds")<<" "<< Sys("uname -a");
		os.Replace("\n", "");
		return os;
	#else
		OSVERSIONINFO osvi;
	    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
	    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	    GetVersionEx(&osvi);
		return Format("Windows %d.%d %s", static_cast<int>(osvi.dwMajorVersion),
				static_cast<int>(osvi.dwMinorVersion), osvi.szCSDVersion);
	#endif
}

int MyPid(){
	#if PLATFORM_POSIX
		return getpid();
	#else
		return GetCurrentProcessId();
	#endif
}

void SysDetached(String cmd){
	LocalProcess proc;
	#if PLATFORM_POSIX
		proc.DoubleFork();
	#endif
	proc.Start(cmd);
	proc.Detach();
}

int64 GetMicroSeconds() {
    uint64_t us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::
                  now().time_since_epoch()).count();
    return us; 
}


/* returns
{
        "hostname": "ubvm",
        "machineid": "a49194b52033f25ca980a16f2cd36795",
        "os": "Ubuntu 20.04.3 LTS",
        "arch": "x86-64"
}
*/
ValueMap OSInfo() {
	static ValueMap infos;
	if (infos.IsEmpty()) {
	#if PLATFORM_POSIX
		infos.GetAdd("platform") = "linux";
		VectorMap<String, String> fields;
		fields
			("Architecture", "arch")
			("Operating System", "os")
			("Static hostname", "hostname")
			("Machine ID", "machineid")
		;

		Vector<String> kvlines = Split(Sys("hostnamectl"), "\n");

		for (int i=0; i < kvlines.GetCount(); i++) {
			String key, val, field;
			if(SplitTo(kvlines[i].Begin(), ":", key, val)){
				String new_ok = TrimBoth(key);
				field = fields.Get(new_ok, "");

				if(field.IsEmpty())
					continue;

				infos.GetAdd(field) = TrimBoth(val);
				field.Clear();
			}
		}
	#else
			infos.GetAdd("platform") = "windows";
			infos.GetAdd("hostname") = GetComputerName();

			String machineid = GetWinRegString("MachineGuid", "SOFTWARE\\Microsoft\\Cryptography", HKEY_LOCAL_MACHINE, KEY_WOW64_64KEY);
			machineid.Replace("-", "");
			infos.GetAdd("machineid") = machineid;

			OSVERSIONINFOEXA  ov;
			ov.dwOSVersionInfoSize = sizeof(ov);
			GetVersionEx((LPOSVERSIONINFOA) &ov);
			infos.GetAdd("os") = Sprintf("Windows %d.%d.%d %d",
					 ov.dwMajorVersion, ov.dwMinorVersion, ov.dwBuildNumber, ov.wProductType);

			SYSTEM_INFO sinfo;
			GetNativeSystemInfo(&sinfo);
			infos.GetAdd("arch") = sinfo.wProcessorArchitecture == 9 ? "x86-64" : "i686";
	#endif
	}
	return infos;
}

void EnsureSingleInstance(Function<void ()> main){
	#if PLATFORM_POSIX
		String lockFile = ConfigPath("libcat.lock");
		int fd = open(lockFile, O_RDWR | O_CREAT, 0666);
		if (fd < 0){
			PromptOK("File Locking failed. Launching anyway");
			main();
		} else{
			if(flock(fd, LOCK_EX | LOCK_NB) == 0){
				main();
				flock(fd, LOCK_UN);
			} else
				Exclamation("Another LibCat is already running");
			close(fd);
		}
	#else
	    HANDLE hMutex = OpenMutex(MUTEX_ALL_ACCESS, 0, "LibCat");

	    if (!hMutex){
			hMutex = CreateMutex(0, 0, "LibCat");
			main();
			CloseHandle(hMutex);
	    }
	    else{
			Exclamation("Another LibCat is already running");
			Exit(0);
	    }
	#endif
}