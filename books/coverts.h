
struct CatIDToStr : public Convert {
	virtual Value  Format(const Value& q) const {
		int id = q;
		return id == 0 ? String("NA") : CatName(id);
	}
};

struct LangIDToStr : public Convert {
	virtual Value  Format(const Value& q) const {
		int id = q;
		return id == 0 ? String("NA") : LangName(id);
	}
};

struct PrintMap : public Convert {
	virtual Value  Format(const Value& q) const {
		if(q.GetTypeName()!="int64")
			PromptOK(q.GetTypeName());
		return AsString(q);
	}
};


struct DateAsDMY : public Convert {
	virtual Value  Format(const Value& q) const {
		String stime = AsString(q);
		int sep = stime.Find(' ');
		if (sep == 10){
			String sdt = stime.Mid(0, sep);
			Date dt = ScanDate("ymd", sdt);
			return Sprintf("%d-%d-%d  ", dt.day, dt.month, dt.year) << stime.Mid(sep+1);
		}
		return stime;
	}
};

struct DeletedBookStatus : public Convert {
	virtual Value  Format(const Value& q) const {
		int status = q;
		switch(status){
			case BOOK_DAMAGED:	return t_("Damaged");
			case BOOK_LOST:		return t_("Lost");
			case BOOK_WITHELD:	return t_("Witheld");
		}
		return "NA";
	}
};
