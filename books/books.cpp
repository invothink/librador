#include <Libcat/Librador.h>
#include <Draw/Draw.h>


Index<int> SelLoan, SelReturn;

#include "coverts.h"
#include "displays.h"

int UpdateBookCount(){
	Sql sql;
	sql * Select(Count(ID)).From(BOOK);
	sql.Fetch();
	BookCount = sql[0];
	return BookCount;
}


BooksTab::BooksTab()
{
	CtrlLayout(*this);
	CtrlLayout(books);
	CtrlLayout(loans);
	
	reBookBarcode.SetPattern(CfgStr("bookBarcode"));
	
	
	books.btnNew.SetImage(LibradorImg::icoAdd()) <<=THISBACK(NewBook);
	books.btnEdit.SetImage(LibradorImg::icoEdt()) <<=THISBACK(EditBook);
	books.btnLoan.SetImage(LibradorImg::right()) <<= THISBACK(IssueBook);
	books.btnClear.SetImage(LibradorImg::clear()) <<=THISBACK(ClearBooksFilter);
	books.btnMore.SetImage(LibradorImg::vmore()) <<=THISBACK(MoreBook);
	
	
	books.filter <<= THISBACK(FilterBooksList);
	books.filter.WhenEnter = THISBACK1(CheckBarcode, &books.filter);
	books.filter.NullText(t_("Search Books"));

	
	books.list.SetLineCy(GetStdFontCy() + DPI(10));
	books.list.SetTable(BOOK);
	books.list.AddKey(ID);
	books.list.AddColumn(STATUS, "  ").Add(ID).SetDisplay(Single<InOutDisplay>());
	books.list.AddColumn(CALL_NO, t_(CfgStr("aliasCall"))).Tip("Call No");
	books.list.AddColumn(TITLE, t_("Title"));
	books.list.AddColumn(AUTHOR, t_("Author"));
	books.list.AddColumn(CAT_ID, t_("Category")).SetConvert(Single<CatIDToStr>());
	books.list.AddColumn(SHELF, t_("Shelf"));
	books.list.AddColumn(ACCN_NO, t_("Accession #")).Tip("Accession No");
	books.list.ColumnWidths("28 60 397 224 145 89 90");
	books.list.WhenLeftClick = THISBACK(ClickedBook);
	books.list.WhenLeftDouble= THISBACK(EditBook);
	books.list.WhenEnterKey = THISBACK(SelectBook);
	books.list.WhenPostQuery << [=] {books.nothingToShow.Show(!books.list.GetCount());};
	books.list.SetOrderBy(TITLE, AUTHOR);
	books.list.Limit(500);
	ClearBooksFilter();
		
	loans.btnReturn.SetImage(LibradorImg::rreturn()) <<= THISBACK(ReturnIssue);
	loans.btnExtend.SetImage(LibradorImg::date()) <<= THISBACK(ExtendIssue);
	loans.btnEdit.SetImage(LibradorImg::icoEdt()) <<=THISBACK(EditIssue);
	loans.btnClear.SetImage(LibradorImg::clear())<<=THISBACK(ClearLoanFilter);
	loans.btnMore.SetImage(LibradorImg::vmore()) <<=THISBACK(MoreIssue);
	
	loans.filter <<= THISBACK(FilterLoanList);
	loans.filter.WhenEnter = THISBACK1(CheckBarcode, &loans.filter);;
	loans.filter.NullText(t_("Search Loans"));
		
	loans.list.SetLineCy(GetStdFontCy() + DPI(10));
	loans.list.SetTable(ISSUES);
	loans.list.AddKey(ID);
	loans.list.AddIndex(BOOK_ID);
	loans.list.AddColumn(STATUS, "  ").AddIndex(DUE).SetDisplay(Single<DueDisplay>());
	loans.list.AddColumn(CALL_NO, t_(CfgStr("aliasCall"))).Tip("Call No");
	loans.list.AddColumn(TITLE, t_("Title"));
	loans.list.AddColumn(NAME, t_("Member"));
	loans.list.AddColumn(PHONE, t_("Mobile"));
	loans.list.AddColumn(ISSUED, t_("Issued"));
	loans.list.AddColumn(DUE, t_("Due"));
	loans.list.ColumnWidths("26 60 397 224 145 89 90");
	loans.list.SetOrderBy(ISSUED);
	loans.list.WhenLeftDouble = THISBACK(EditIssue);
	ClearLoanFilter();
	
	
	
	vsplit.Vert(books.Background(White()),
				loans.Background(White()));
	vsplit.SetMin(0,4000);
	vsplit.SetMin(1,3000);
}

void BooksTab::MoreBook()
{
	static ActionDlg actions("More book actions");
	if(!actions.GetCount())
		actions
			.Add("Categories", LibradorImg::block(), "View categories and books in them", [=]{
				ItemListDlg dlg("Categories", CATEGORY);
				dlg
					.AddColumn(NAME, t_("Name"), 249).AddColumn(CODE, t_("Code"), 45)
					.OnAdd([=]{}).OnEdit([=]{}).OnDelete([=]{})
					.OnView([&](int id){
						BookListDlg(~dlg.list.Get(NAME), CAT_ID, id).Execute();
					})
					.OnChange([=]{
						CatName.Populate();
					})
					.Execute()
				;
			})
			.Add("Publishers", LibradorImg::bank(), "View publishers and books by them", [=]{
				ItemListDlg dlg("Publishers", PUBLISHER);
				dlg
					.AddColumn(NAME, t_("Name"), 249)
					.OnAdd([=]{}).OnEdit([=]{}).OnDelete([=]{})
					.OnView([&](int id){
						BookListDlg(~dlg.list.Get(NAME),PUB_ID, id).Execute();
					})
					.OnChange([=]{
						PubName.Populate();
					})
					.Execute()
				;
			})
			.Add("Languages", LibradorImg::a_color(), "View languages and books in them", [=]{
				ItemListDlg dlg("Languages", LANGUAGE);
				dlg
					.AddColumn(NAME, t_("Name"), 249)
					.OnAdd([=]{}).OnEdit([=]{}).OnDelete([=]{})
					.OnView([&](int id){
						BookListDlg(~dlg.list.Get(NAME),LANG_ID, id).Execute();
					})
					.OnChange([=]{
						LangName.Populate();
					})
					.Execute()
					;
			})
			.Add("Book History", LibradorImg::history(), "View list of deleted booked.", [=] {
				BookArchiveDlg().Execute();
			})
		;
	actions.Execute();
}

void BooksTab::MoreIssue()
{
	static ActionDlg actions("More loan actions");
	if(!actions.GetCount())
		actions
			.Add("Issual History", LibradorImg::history(), "View list of returned booked.", [=] {
					LoanArchiveDlg().Execute();
				})
		;
	actions.Execute();
}


BookDlg::BookDlg()
{
	CtrlLayoutOK(*this, t_("Book"));
	Sizeable().CloseBoxRejects();
	
	lblAccnNo.SetText(t_(CfgStr("aliasAccn")));
	lblCallNo.SetText(t_(CfgStr("aliasCall")));
	ctrls
		(CAT_ID, category)
		(ACCN_NO, accno)
		(CALL_NO, callno)
		(TITLE, title)
		(AUTHOR, author)
		(PUB_ID, publisher)
		(LANG_ID, lang)
		(ISBN, isbn)
		(PRICE, price)
		(SHELF, shelf)
		(TYPE_ID, btype)
		(EDITION, edition)
		(BARCODE, barcode)
		(DDCCODE, ddccode)
		
	;
	
	ok.WhenAction = THISBACK(CloseDlg);	
	title.WhenEnter = THISBACK(CloseDlg);
	author.WhenEnter = THISBACK(CloseDlg);
	price.WhenEnter = THISBACK(CloseDlg);
	shelf.WhenEnter = THISBACK(CloseDlg);
	edition.WhenEnter = THISBACK(CloseDlg);
	ddccode.WhenEnter = THISBACK(CloseDlg);	
	
	String item;
	for(int i=0; i < LangName.GetCount(); i++){
		item = LangName(i);
		if(!item.IsEmpty()) lang.Add(i,item);
	}
	if(lang.GetCount())
		lang.SetIndex(0);
	
	for(int i=0; i < PubName.GetCount(); i++){
		item = PubName(i);
		if(!item.IsEmpty())
			publisher.Add(i, item);
	}
	publisher.AddButton(DropGrid::BTN_PLUS, THISBACK(NewPublisher)).SetImage(LibradorImg::icoAdd());
	//publisher.AddButton().SetImage(LibradorImg::icoAdd())<<= ;
	
	
	for(int i=0; i < CatName.GetCount(); i++){
		item = CatName(i);
		if(!item.IsEmpty())
			category.Add(i, item);
	}
	category.AddButton().SetImage(LibradorImg::icoAdd())<<= THISBACK(NewCategory);
	category <<=THISBACK(ChangeCategory);

	
	MakeMultiSelect(author);
	for(int i=0; i < AutoAuthors.GetCount(); i++)
		author.AddList(AutoAuthors[i]);

	MakeMultiSelect(title);
	for(int i=0; i < AutoTitles.GetCount(); i++)
		title.AddList(AutoTitles[i]);
		
	btype.Add(0, t_("Book"));
	btype.Add(1, t_("E-Book"));
	btype.Add(2, t_("Reference"));
	btype.Add(3, t_("Magazine"));
	btype.Add(4, t_("Newspaper"));
	btype.Add(5, t_("CD/DVD"));
	btype.Add(6, t_("Magnetic Tape"));
	btype.Add(7, t_("Flyer"));
	btype.Add(8, t_("Photo/Film"));
	btype.SetIndex(0);
	
	btnDelete <<= THISBACK(DeleteBook);
}

void BookDlg::CloseDlg()
{
	AcceptBreak(IDOK);
}

void BookDlg::ChildGotFocus()
{
	if(author.HasFocus() && title.IsModified())
		CheckForAuthor();
}

void BookDlg::NewCategory()
{
	WithNewCategoryDlg<TopWindow> dlg;
	CtrlLayoutOK(dlg, t_("New Category"));
	dlg.lblError.Hide();
	dlg.CloseBoxRejects();
	if(dlg.Execute() == IDOK){
		Sql sql;
		sql * Insert(CATEGORY)(NAME, ~dlg.name)(CODE, ~dlg.code);
		int id = sql.GetInsertedId();
		CatName.Set(id, ~dlg.name);
		category.Add(id, ~dlg.name);
		category.SetIndex(category.GetCount()-1);
		UpdateCallNo(~dlg.code);
	}
}


void BookDlg::ChangeCategory()
{
	if(category.GetIndex()<0)
		return;
	int id = category.GetKey(category.GetIndex());
	if(!id)
		return;
		
	Sql sql;
	String code = sql % Select(CODE).From(CATEGORY).Where(ID==id);
	UpdateCallNo(code);
}

void BookDlg::UpdateCallNo(String code)
{
	if(CfgInt("formatCallNo") == CALLNO_CCIDX){
		Sql sql;
		sql.Execute("select max(cast(substr(CALL_NO,?) as int)) from BOOK where CALL_NO"
					" like ?;",String(code).GetCount()+1, String(code)<<"%");
		sql.Fetch();
		callno = String(code)<<Format("%.3d", ScanInt(String("0")<<sql[0].ToString())+1);
	} else
		callno = String() << code << ~accno;
}

void BookDlg::CheckForAuthor()
{
	if(author.GetText().IsEmpty()){
		Sql sql;
		sql * (Select(AUTHOR, LANG_ID, PUB_ID, DDCCODE).From(BOOK).Where(TITLE==~title)
		| Select(AUTHOR, LANG_ID, PUBLISHER, DDCCODE).From(AUTOBOOK).Where(TITLE==~title));
		if(sql.Fetch()){
			author.SetText(String(sql[AUTHOR]));
			lang.SetIndex(int(sql[LANG_ID])-1);
			ddccode.SetText(String(sql[DDCCODE]));
			/*if(IsNumber(sql[PUB_ID]))
				PromptOK(String()<<sql[PUB_ID]);*/
		}
	}
}


void BookDlg::NewPublisher()
{
	String name;
	if(EditTextNotNull(name, "New Publisher", "Name")){
		Sql sql;
		sql * Insert(PUBLISHER)(NAME, name);
		int id = sql.GetInsertedId();
		PubName.Set(id, name);
		publisher.Add(id, name);
		publisher.SetIndex(publisher.GetCount()-1);
	}
	
}

bool BookDlg::ExecuteAdd()
{
	Title("New Book");
	ok.SetLabel("Add");
	btnDelete.Hide();
	multi.Show();
	Sql sql;
	bool ret = false, ok=false;
	if(category.GetCount())
		category.SetIndex(max(0,min(CfgInt("defaultCategory"), category.GetCount()-1)));
	do{
		sql.Execute("select max(cast(ACCN_NO as INTEGER)) from BOOK;");
		sql.Fetch();
		accno = sql[0].IsNull() ? 1 : int(sql[0]) + 1;
		category.WantFocus().InitFocus();
		ChangeCategory();
		ok = false;
		if(ExecuteOK()){
			Sql sql;
			sql * ctrls.Insert(BOOK);
			bookId = sql.GetInsertedId();
			ret = true;
			ok = true;
		}
		accno.Clear();
		callno.Clear();
		title.Clear();
		author.Clear();
		isbn.Clear();
		price.Clear();
		edition.Clear();
		barcode.Clear();
		ddccode.Clear();
		publisher.SetData(Value());
	}while(multi.Get() && ok);
	return ret;
}

bool BookDlg::ExecuteEdit(Value id)
{
	Title(t_("Edit Book #")+FormatInt(id));
	ok.SetLabel("Edit");
	btnDelete.Show();
	multi.Hide();
	if(!ctrls.Load(BOOK, ID == id))
		return false;
	
	bookId = id;
	if(ExecuteOK()){
		SQL * ctrls.Update(BOOK).Where(ID == id);
		return true;
	}
	return false;
}


DelBookDlg::DelBookDlg(String title)
{
	CtrlLayoutOKCancel(*this, String() << "Delete " << title << " ?");
	CloseBoxRejects();
	
	reason.Add(BOOK_DAMAGED, t_("Damaged"));
	reason.Add(BOOK_LOST, t_("Lost"));
	reason.Add(BOOK_WITHELD, t_("Witheld"));
	remark.ProcessTab(false);
}

void BookDlg::DeleteBook()
{
	DelBookDlg dlg(title.GetText().ToString());
	while(dlg.ExecuteOK()==IDOK){
		if(dlg.remark.Get().IsEmpty()){
			PromptOK(t_("Please enter a brief remark for futurer refrence"));
			continue;
		}
		SelLoan.RemoveKey(bookId);
		SQL * ::Update(BOOK)
			(DTIME, SqlCurrentTime())
			(DELNOTE, ~dlg.remark)
			(STATUS, int(dlg.reason))
			.Where(ID == bookId);
		Break(IDOK);
		break;
	}
}

void BooksTab::NewBook()
{
	BookDlg dlg;
	if(dlg.ExecuteAdd()){
		books.list.Query();
		books.list.FindSetCursor(dlg.bookId);
		AutoAuthors.FindAdd(~dlg.author);
		AutoTitles.FindAdd(~dlg.title);
	}
}

void BooksTab::EditBook()
{
	int id = books.list.GetKey();
	if(IsNull(id))
		return;
	
	BookDlg dlg;
	if(dlg.ExecuteEdit(id)){
		books.list.ReQuery();
	}
}
void BooksTab::SelectBook()
{
	int id = books.list.GetKey();
	if(IsNull(id))
		return;
	if(books.list.Get(STATUS)==BOOK_AVALIABLE /*&&
		books.list.GetClickRow()!=books.list.GetCursor() || books.list.GetClickColumn()<1*/){
		if(SelLoan.Find(id)>-1)
			SelLoan.Remove(SelLoan.Find(id));
		else
			SelLoan.Add(id);
		books.list.RefreshRow(books.list.GetCursor());
		
		if(SelLoan.GetCount())
			books.btnLoan.SetLabel(Format(t_("Issue Book (%d)"), SelLoan.GetCount())).Enable();
		else
			books.btnLoan.SetLabel(t_("Issue Book")).Disable();
	}
}
void BooksTab::ClickedBook()
{
	if(books.list.GetClickColumn() == 0)
		SelectBook();
}




void BooksTab::IssueBook()
{
	
	int id = books.list.GetKey();
	if(IsNull(id))
		return;

	IssueDlg dlg;
	if(dlg.ExecuteAdd(SelLoan)){
		ClearBooksFilter();
		loans.list.Query();
		loans.list.SetCursor(0);
	}
}


void BooksTab::EditIssue()
{
	int id = loans.list.GetKey();
	if(IsNull(id))
		return;
	
	IssueDlg dlg;
	if(dlg.ExecuteEdit(id)){
		loans.list.ReQuery();
	}
}

void BooksTab::ExtendIssue()
{
	int id = loans.list.GetKey();
	if(IsNull(id))
		return;
	Date dt = loans.list.Get(DUE);
	if (EditDateDlg(dt, t_("Extend Book"), t_("New due date"))){
		SQL.Begin();
		SQL * ::Update(LEDGER)(DUE, dt).Where(ID == id);
		SQL * ::Update(BOOK)(STATUS, BOOK_EXTENTED).Where(ID == loans.list.Get(BOOK_ID));
		SQL.Commit();
		loans.list.ReQuery();
	}
}

void BooksTab::ReturnIssue()
{
	int id = loans.list.GetKey();
	if(IsNull(id))
		return;
	
	WithIssueReturnDlg<TopWindow> dlg;
	CtrlLayoutOK(dlg, String("Return ") << loans.list.Get(TITLE));
	dlg.retDate.SetData(GetSysDate());
	dlg.name = Format("Return %s ?", loans.list.Get(TITLE));
	dlg.CloseBoxRejects();
	if(dlg.Execute() == IDOK){
		SQL.Begin();
		SQL * ::Update(LEDGER)(RETURNED, ~dlg.retDate).Where(ID == id);
		SQL * ::Update(BOOK)(STATUS, BOOK_AVALIABLE).Where(ID == loans.list.Get(BOOK_ID));
		SQL.Commit();
		QueryIssues();
		QueryBooks();
	}
}


void BooksTab::FilterBooksList()
{
	if(books.filter.GetLength() == 0) {
		books.list.Query(ID == SqlSetFrom(SelLoan));
	}
	if(books.filter.GetLength() < 3) return;
	
	String filter = ~books.filter;

	
	books.list.Query(IsNull(DTIME)
		&& (Like(TITLE, Wild(String("*")<<filter<<"*"))
		|| Like(AUTHOR, Wild(String("*")<<filter<<"*"))
		|| Like(CALL_NO , Wild(String() <<filter<<"*"))
		|| Like(ACCN_NO , Wild(String() <<filter<<"*"))
	));
}

void BooksTab::CheckBarcode(EditString *filterEdit)
{
	String filter = TrimBoth(~*filterEdit);
	if(CfgInt("barcode") & BARCODE_BOOK && reBookBarcode.Match(filter)){
		Sql sql;
		sql * Select(ID).From(BOOK).Where(BARCODE == filter);
		if (sql.Fetch() && int(sql[0]) > 0){
			int id = sql[0];
			books.list.Query(IsNull(DTIME) && ID == id);
			filterEdit->SelectAll();
			switch(int(books.list.Get(STATUS))){
				case BOOK_AVALIABLE:
					//SelectBook(); return;
					break;
				case BOOK_BORROWED:
					loans.list.Query(IsNull(DTIME) && IsNull(RETURNED) && BOOK_ID == id);
					ReturnIssue();
					return;
			}
		}
	}
	SelectBook();
}


void BooksTab::ClearBooksFilter()
{
	SelLoan.Clear();
	books.filter.Clear();
	books.btnLoan.SetLabel(t_("Issue Book")).Disable();
	//books.list.Query(IsNull(DTIME) && ACCN_NO >= 90000000);
	QueryBooks();
}

void BooksTab::QueryBooks()
{
	books.list.Query(SqlBool("cast(ACCN_NO as INTEGER) > 90000000", SqlS::COMP));
}

void BooksTab::FilterLoanList()
{
	if(loans.filter.GetLength() == 0) {ClearLoanFilter(); return; }
	if(loans.filter.GetLength() < 3) return;
	loans.list.Query(IsNull(DTIME) && IsNull(RETURNED)
				&& (Like(TITLE, Wild(String("*")<<~loans.filter<<"*"))
				|| Like(AUTHOR, Wild(String("*")<<~loans.filter<<"*"))
				|| Like(PHONE, Wild(AsString(~loans.filter)<<"*"))
				|| Like(CALL_NO , Wild(String()<<~loans.filter<<"*"))
				|| Like(ACCN_NO , Wild(String()<<~loans.filter<<"*"))
				|| Like(NAME, Wild(String("*")<<~loans.filter<<"*"))
			));
}

void BooksTab::ClearLoanFilter()
{
	loans.filter.Clear();
	QueryIssues();
}

void BooksTab::QueryIssues()
{
	loans.list.Query(IsNull(DTIME) && IsNull(RETURNED));
}



LoanArchiveDlg::LoanArchiveDlg()
{
	CtrlLayout(*this, t_("Issual History"));
	
	Zoomable().Sizeable();
	
	
	filter <<= THISBACK(FilterList);
	filter.NullText(t_("Search History..."));
	btnClear.SetImage(LibradorImg::clear()) <<= THISBACK(ClearFilter);
	btnEdit.SetImage(LibradorImg::icoEdt()) <<= THISBACK(EditIssue);
	
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(ISSUES);
	list.AddKey(ID);
	list.AddIndex(BOOK_ID);
	list.AddIndex(DUE);
	list.AddColumn(ACCN_NO, t_(CfgStr("aliasAccn")));
	list.AddColumn(TITLE, t_("Title"));
	list.AddColumn(NAME, t_("Member"));
	list.AddColumn(PHONE, t_("Mobile"));
	list.AddColumn(ISSUED, t_("Issued"));
	list.AddColumn(DUE, t_("Due"));
	list.AddColumn(RETURNED, t_("Returned"));
	list.ColumnWidths("91 457 264 166 104 104 104");
	list.SetOrderBy(Descending(ISSUED));
	list.WhenLeftDouble = THISBACK(EditIssue);
	list.WhenPostQuery << [=] {	lblLimited.Show(list.GetCount() == 2500); };
	list.Limit(2500);
	ClearFilter();
}

void LoanArchiveDlg::EditIssue()
{
	int id = list.GetKey();
	if(IsNull(id))
		return;
	
	IssueDlg dlg;
	if(dlg.ExecuteEdit(id, true)){
		list.ReQuery();
	}

}

void LoanArchiveDlg::FilterList()
{
	if(filter.GetLength() == 0) {ClearFilter(); return; }
	if(filter.GetLength() < 3) return;
	String ftxt = ~filter;
	list.Query(IsNull(DTIME) && NotNull(RETURNED)
				&&(Like(TITLE, Wild(String("*")<<ftxt<<"*"))
				|| Like(PHONE, Wild(String()<<ftxt<<"*"))
				|| Like(CALL_NO, Wild(String()<<ftxt<<"*"))
				|| Like(ACCN_NO, Wild(String()<<ftxt<<"*"))
				|| Like(NAME, Wild(String("*")<<ftxt<<"*"))));
}

void LoanArchiveDlg::ClearFilter()
{
	filter.Clear();
	list.Query(IsNull(DTIME) && NotNull(RETURNED));
}





BookArchiveDlg::BookArchiveDlg()
{
	CtrlLayout(*this, t_("Book History"));
	
	Zoomable().Sizeable();
	
	
	filter <<= THISBACK(FilterList);
	filter.NullText(t_("Search History..."));
	btnClear.SetImage(LibradorImg::clear()) <<= THISBACK(ClearFilter);
	btnRestore.SetImage(LibradorImg::restore()) <<= THISBACK(RestoreBook);
	btnNote.SetImage(LibradorImg::exclaim()) <<= THISBACK(ShowNote);
	
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(BOOK);
	list.AddKey(ID);
	list.AddColumn(CALL_NO, t_(CfgStr("aliasCall"))).Tip("Call No");
	list.AddColumn(ACCN_NO, t_("Accession #")).Tip("Accession No");
	list.AddColumn(TITLE, t_("Title"));
	list.AddColumn(AUTHOR, t_("Author"));
	list.AddColumn(CAT_ID, t_("Category")).SetConvert(Single<CatIDToStr>());
	list.AddColumn(DTIME, t_("Deleted")).SetConvert(Single<DateAsDMY>());;
	list.AddColumn(STATUS, t_("Status")).SetConvert(Single<DeletedBookStatus>());
	list.ColumnWidths("80 80 320 160 90 100 90");
	list.SetOrderBy(Descending(DTIME));
	list.WhenLeftDouble << THISBACK(ShowNote);
	list.WhenPostQuery << [=] {	lblLimited.Show(list.GetCount() == 2500); };
	list.Limit(2500);
	ClearFilter();
}

void BookArchiveDlg::RestoreBook()
{
	int id = list.GetKey();
	if(IsNull(id))
		return;

	SQL * ::Update(BOOK)
		(DTIME, Null)
		(STATUS, 0)
		.Where(ID == id);
	Break(IDOK);
	list.ReQuery();
}

void BookArchiveDlg::ShowNote()
{
	int id = list.GetKey();
	if(IsNull(id))
		return;
	Sql sql;
	sql * Select(DELNOTE).From(BOOK).Where(ID == id);
	if(sql.Fetch())
		Prompt(t_("Deletion note"), LibradorImg::exclaim(), DeQtf(AsString(sql[DELNOTE])).Begin(), "OK");
	
}

void BookArchiveDlg::FilterList()
{
	if(filter.GetLength() == 0) {ClearFilter(); return; }
	if(filter.GetLength() < 3) return;
	String ftxt = ~filter;
	list.Query(NotNull(DTIME)
				&&(Like(TITLE, Wild(String("*")<<ftxt<<"*"))
				|| Like(CALL_NO, Wild(String()<<ftxt<<"*"))
				|| Like(ACCN_NO, Wild(String()<<ftxt<<"*"))
				|| Like(AUTHOR, Wild(String("*")<<ftxt<<"*"))));
}

void BookArchiveDlg::ClearFilter()
{
	filter.Clear();
	list.Query(NotNull(DTIME));
}



struct InOutDisplay2 : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		int out = q;
		w.DrawRect(r, paper);
		if(!out)
			w.DrawImage(r.left+2, r.top+2, LibradorImg::green());
		else
			w.DrawImage(r.left+2, r.top+2, LibradorImg::grey());
	}
};

BookListDlg::BookListDlg(String name, SqlId &key, Value id)
{
	CtrlLayout(*this);
	Sizeable().CloseBoxRejects();
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(BOOKCAT);
	list.AddKey(ID);
	list.AddColumn(STATUS, "  ").SetDisplay(Single<InOutDisplay2>());
	list.AddColumn(ACCN_NO, t_("Accession #")).Tip("Accession No");	
	list.AddColumn(CALL_NO, t_("Category #")).Tip("Call No");
	list.AddColumn(TITLE, t_("Title"));
	list.AddColumn(AUTHOR, t_("Author"));
	list.AddColumn(ID, "ID");
	list.SetOrderBy(ACCN_NO);
	list.ColumnWidths("27 71 73 277 158 35");
	list.Query(key == id && IsNull(DTIME));
	list.WhenLeftDouble << [&]{	
		int id = list.GetKey();
		if(IsNull(id))
			return;
		BookDlg dlg;
		if(dlg.ExecuteEdit(id)){
			list.ReQuery();
		}
	};
	Title(Format("Showing %d Books from %s #%d (%s)",list.GetCount(),
			 key.IsEqual(CAT_ID) ?"category" : "publisher", int(id), name));
}
