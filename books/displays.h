
struct PrintDisplay : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		w.DrawRect(r, paper);
		w.DrawText(r.left, r.top + 2, q.ToString(), StdFont(), ink);
	}
};

struct InOutDisplay : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		int out = q[STATUS];
		w.DrawRect(r, paper);
		
		switch(0xFFFF & out){
			case BOOK_AVALIABLE:
				if(SelLoan.Find(q[ID])>=0)
					w.DrawImage(r.left+DPI(2), r.top + DPI(3), LibradorImg::achecked());
				else
					w.DrawImage(r.left+DPI(2), r.top + DPI(3), LibradorImg::acheck());
				break;
			case BOOK_BORROWED:
			case BOOK_EXTENTED:
				w.DrawImage(r.left+DPI(2), r.top + DPI(3), LibradorImg::icheck());
				break;
		}
	}
	virtual Size GetStdSize(const Value& q) const
	{
		return Size(DPI(20),DPI(16));
	}	
};

struct CheckedDispay : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		int checked = q;
		if(checked)
			w.DrawImage(r.left+2, r.top+4, LibradorImg::achecked());
		else
			w.DrawImage(r.left+2, r.top+4, LibradorImg::acheck());
	}
};

struct DueDisplay : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		const ValueMap &m = (ValueMap)q;
		w.DrawRect(r, paper);
		if(GetSysDate() > Date(m[DUE]))
			w.DrawImage(r.left+2, r.top+4, LibradorImg::orange());
		else
			w.DrawImage(r.left+2, r.top+4, LibradorImg::green());
	}
};
