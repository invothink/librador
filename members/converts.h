struct MGroupIDToStr : public Convert {
	virtual Value  Format(const Value& q) const {
		int id = q;
		return id == 0 ? String("NA") : MGroupName(id);
	}
};
