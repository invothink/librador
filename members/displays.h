struct AddressDisplay  : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		String txt = q;
		txt.Replace("\n", ", ");
		w.DrawRect(r, paper);
		w.DrawText(r.left, r.top + 2, txt, StdFont(), ink);
	}
};

struct MemDueDisplay : public Display {
	virtual void Paint(Draw& w, const Rect& r, const Value& q,
	                   Color ink, Color paper, dword style) const {
		const ValueMap &m = (ValueMap)q;
		w.DrawRect(r, paper);
		if(m[RETURNED].IsNull())
			if(GetSysDate() > Date(m[DUE]))
				w.DrawImage(r.left+2, r.top+2, LibradorImg::orange());
			else
				w.DrawImage(r.left+2, r.top+2, LibradorImg::green());
		else
			w.DrawImage(r.left+2, r.top+2, LibradorImg::grey());
	}
};