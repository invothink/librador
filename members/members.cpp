#include <Libcat/Librador.h>
#include <Draw/Draw.h>

#include "displays.h"
#include "converts.h"

MembersTab::MembersTab()
{
	CtrlLayout(*this);
	
	reMemberBarcode.SetPattern(CfgStr("memberBarcode"));
	
	btnNew.SetImage(LibradorImg::icoAdd()) <<= THISBACK(NewMember);
	btnEdit.SetImage(LibradorImg::icoEdt()) <<= THISBACK(EditMember);
	btnMore.SetImage(LibradorImg::vmore()) <<=THISBACK(MoreMember);
	
	filter <<= THISBACK(FilterList);
	filter.WhenEnter = THISBACK(CheckBarcode);
	btnClear.SetImage(LibradorImg::clear()) <<= THISBACK(ClearFilter);
	
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(MEMBER);
	list.AddKey(ID);
	list.AddColumn(MEMBER_NO, t_("Mem. #"));
	list.AddColumn(NAME, t_("Name"));
	list.AddColumn(PHONE, t_("Phone"));
	list.AddColumn(MGROUP_ID, SchoolVariant?t_("Class"):t_("Type")).SetConvert(Single<MGroupIDToStr>());
	list.AddColumn(ADDRESS, t_("Address")).SetDisplay(Single<AddressDisplay>());

	list.WhenLeftDouble = THISBACK(EditMember);
	list.SetOrderBy(NAME, ID);
	list.ColumnWidths("46 209 82 76 308");
	list.Query(IsNull(DTIME));
	list.NoPopUpEx();
	
	/*
	UpdateFilterMGroup();
	dropGroup.WhenAction = [=]{
		//RLOG(dropGroup.Get(dropGroup.GetIndex()));
	};
	*/
	dropGroup.Hide();
}

void MembersTab::UpdateFilterMGroup()
{
	dropGroup.Clear();
	for(int i=0; i<MGroupName.GetCount(); i++){
		String name = MGroupName(i);
		int id = MGroupName.GetId(name);
		//RLOG(String(name) << id);
		dropGroup.Add(id, name);
	}
}

int MembersTab::Query(SqlBool where)
{
	return 0;
}

void MembersTab::MoreMember()
{
	static ActionDlg actions("More memeber actions");
	if(!actions.GetCount())
		actions
			.Add(SchoolVariant?"Classes":"Membership Type",LibradorImg::history(), "Manage membership types", [=] {
				ItemListDlg dlg(SchoolVariant?"Classes":"Membership Type", MGROUP);
				dlg
					.AddColumn(NAME, t_("Name"), 249)
					.OnAdd([=]{}).OnEdit([=]{}).OnDelete([=]{})
					.OnView([&](int id){
						MemberListDlg(~dlg.list.Get(NAME),id).Execute();
					})
					.OnChange([&]{
						//if(!dlg.list.IsEditing())
							MGroupName.Populate();
					})
					.Execute()
				;
				})
		;
	actions.Execute();	
}


void MembersTab::NewMember()
{
	MemberDlg dlg;
	if(dlg.ExecuteAdd()){
		list.Query();
		list.FindSetCursor(dlg.memberId);
	}
}

void MembersTab::EditMember()
{
	int id = list.GetKey();
	if(IsNull(id))
		return;
	
	MemberDlg dlg;
	if(dlg.ExecuteEdit(id))
		list.ReQuery();
}

void MembersTab::CheckBarcode()
{
	if((CfgInt("barcode") & BARCODE_MEMBER) && reMemberBarcode.Match(~filter)){
		list.Query(IsNull(DTIME) && BARCODE == ~filter);
		int id = list.GetKey();
		filter.SelectAll();
		
		/*
		if(!IsNull(id) && list.Get(BARCODE) == ~filter){
			MemberHistoryDlg(list.Get(NAME), id).Execute();
		}
		*/
	}
}

void MembersTab::FilterList()
{
	if(filter.GetLength() == 0) {ClearFilter(); return; }
	if(filter.GetLength() < 3) return;
		
	list.Query(IsNull(DTIME)
				&&  (Like(NAME, Wild(String("*")<<~filter<<"*"))
				||	Like(MEMBER_NO, Wild(String()<<~filter<<"*"))
				||	Like(PHONE, Wild(String("*")<<~filter))
				||	Like(ADDRESS, Wild(String("*")<<~filter<<"*")))
	);
}

void MembersTab::ClearFilter()
{
	filter.Clear();
	list.Query(IsNull(DTIME));
}

MemberDlg::MemberDlg()
{
	CtrlLayoutOK(*this, t_("Member"));
	Sizeable().CloseBoxRejects();
	
	membno.Enable(CfgInt("editableMembNo"));
	
	Sql sql;
	sql * Select(ID, NAME).From(MGROUP);
	
	if(sql.Fetch()){
		do
			mgroup.Add(sql[0], sql[1]);
		while(sql.Fetch());
	}else{
		lblMGroup.Hide();
		mgroup.Hide();
	}
	for(int i=0; i<BloodGroups.values.GetCount(); i++)
		blood.Add(i, BloodGroups(i));
	blood.SetIndex(0);

	gender.Clear();
	for(int i=0; i<Genders.values.GetCount(); i++)
		gender.Add(i, Genders(i));
	gender.SetIndex(GENDER_MALE);
		
	address.ProcessTab(false);
	
	refrer.Hide();
	refrerName.Disable();
	btnRefrer.SetImage(LibradorImg::search()).Tip(t_("Select a member"));
	btnRefrer <<= THISBACK(SelectRefrer);
	
	ctrls
		(NAME, name)
		(MEMBER_NO, membno)
		(MGROUP_ID, mgroup)
		(PHONE, phone)
		(DOB, dob)
		(EMAIL, email)
		(GENDER, gender)
		(ADDRESS, address)
		(DOR, dor)
		(DEPOSIT, deposit)
		(WARD, ward)
		(BLOOD, blood)
		(DOJ, doj)
		(REFRER, refrer)
		(JOB, job)
		(BARCODE, barcode)
		
	;
	
	ok.WhenAction = THISBACK(CloseDlg);
	btnDelete <<= THISBACK(DeleteMember);
	btnHistory << [=] { MemberHistoryDlg(~name, memberId).Execute(); };
	
	name.WhenEnter = THISBACK(CloseDlg);
	membno.WhenEnter = THISBACK(CloseDlg);
	phone.WhenEnter = THISBACK(CloseDlg);
	dob.WhenEnter = THISBACK(CloseDlg);
	email.WhenEnter = THISBACK(CloseDlg);
	ward.WhenEnter = THISBACK(CloseDlg);
	doj.WhenEnter = THISBACK(CloseDlg);
	refrer.WhenEnter = THISBACK(CloseDlg);
}

void MemberDlg::CloseDlg()
{
	AcceptBreak(IDOK);
}

bool MemberDlg::ExecuteAdd()
{
	Title("New Member");
	ok.SetLabel("Add");
	btnDelete.Hide();
	btnHistory.Hide();
	multi.Set(0).Show();
	bool refresh = false, ok = false;
	do{
		Sql sql;
		sql.Execute("select max(cast(member_no as integer)) from member;");
		sql.Fetch();
		membno = sql[0].IsNull()?1:int(sql[0])+1;
		
		Date joiningDt = GetSysDate();
		doj.SetDate(joiningDt.year, joiningDt.month, joiningDt.day);

		Date renewDt = GetSysDate() + 365;
		dor.SetDate(renewDt.year, renewDt.month, renewDt.day);

		if(mgroup.IsVisible())
			mgroup.SetIndex(max(0,min(CfgInt("defaultMGroup"), mgroup.GetCount()-1)));
		if(ExecuteOK()){
			Sql sql;
			sql * ctrls.Insert(MEMBER);
			memberId = sql.GetInsertedId();
			refresh = true;
			ok = true;
		}
		name.Clear();
		dob.Clear();
		phone.Clear();
		gender.SetIndex(GENDER_MALE);
		email.Clear();
		address.Clear();
		dor.Clear();
		deposit.Clear();
		ward.Clear();
		blood.SetIndex(0);
		job.Clear();
		barcode.Clear();
		refrer.Clear();
		doj.Clear();
	}while(multi.Get() && ok);
	return refresh;
}


bool MemberDlg::ExecuteEdit(Value id)
{
	Title(t_("Edit Member #") + FormatInt(id));
	ok.SetLabel("Edit");
	btnDelete.Show();
	btnHistory.Show();
	multi.Hide();
	if(!ctrls.Load(MEMBER, ID == id))
		return false;
	
	if(~refrer)
		SetRefrer(~refrer);
	
	memberId = id;
	if(ExecuteOK()){
		SQL * ctrls.Update(MEMBER).Where(ID == id);
		return true;
	}
	return false;
}

void MemberDlg::SetRefrer(Value id)
{
	Sql sql;
	sql * Select(NAME, MEMBER_NO).From(MEMBER).Where(ID == id);
	if(sql.Fetch())
		refrerName = Format("%s (%s)", sql[NAME], sql[MEMBER_NO]);
}

void MemberDlg::SelectRefrer()
{
	if(~refrer)
		SetRefrer(~refrer);
		
	if(membDlg.ExecuteOK()){
		refrer = membDlg.list.GetKey();
		refrerName = Format("%s (%s)",~membDlg.list.Get(NAME),~membDlg.list.Get(MEMBER_NO));
	}
}


void MemberDlg::DeleteMember()
{
	if(PromptYesNo(Sprintf("Do you want to delete '%s' ?", AsString(~name).Begin()))) {
		SQL * ::Update(MEMBER)(DTIME, SqlCurrentTime()).Where(ID == memberId);
		Break(IDOK);
	}
}


MemberHistoryDlg::MemberHistoryDlg(String name, Value id)
{
	CtrlLayout(*this, Format(t_("History - Member #%d (%s)"), id, name) );
	Sizeable().CloseBoxRejects();
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(ISSUES);
	list.AddKey(ID);
	list.AddColumn(STATUS, "  ").AddIndex(DUE).AddIndex(RETURNED).SetDisplay(Single<MemDueDisplay>());
	list.AddColumn(ACCN_NO, t_("Book"));
	list.AddColumn(TITLE, t_("Title"));
	list.AddColumn(AUTHOR, t_("Author"));
	list.AddColumn(ISSUED, t_("Issued"));
	list.AddColumn(DUE, t_("Due"));
	list.AddColumn(RETURNED, t_("Returned"));
	list.SetOrderBy(Descending(ISSUED));
	list.ColumnWidths("31 48 242 148 74 72 71");
	list.Query(MEMBER_ID == id);
	list.NoPopUpEx();
}


SelectMemberDlg::SelectMemberDlg()
{
	CtrlLayoutOK(*this, t_("Select member"));
	Sizeable().CloseBoxRejects();
	
	filter.NullText(t_("Search Member"));
	filter <<= THISBACK(FilterList);
	btnClear <<= THISBACK(ClearFilter);
	btnClear.SetImage(LibradorImg::clear());
	btnNew << [=] {
		MemberDlg dlg;
		if(dlg.ExecuteAdd()){
			list.Query();
			list.FindSetCursor(dlg.memberId);
			btnHistory.Enable(list.GetCount());
			librador->membersTab.list.Query();
			librador->membersTab.list.FindSetCursor(dlg.memberId);
		}
	};
	
	lblDetail.RemoveFrame(0);
	btnHistory.WhenAction =  [=] {
		int id = list.GetKey();
		if(!id)	return;
		MemberHistoryDlg(list.Get(NAME), id).Execute();
	};
	
	ok.WhenAction = THISBACK(CloseDlg);
	
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(MEMBER);
	list.AddKey(ID);
	list.AddColumn(MEMBER_NO, t_("Mem. #"));
	list.AddColumn(NAME, t_("Name"));
	list.AddColumn(PHONE, t_("Phone"));
	list.AddColumn(ADDRESS, t_("Address")).SetDisplay(Single<AddressDisplay>());
	//list.WhenLeftDouble = THISBACK(EditMember);
	list.WhenAction = THISBACK(Highlight);
	list.SetOrderBy(NAME, ID);
	list.ColumnWidths("45 175 94 201");
	list.NoPopUpEx();
	list.Query(IsNull(DTIME));
	btnHistory.Enable(list.GetCount());
}

void SelectMemberDlg::CloseDlg()
{
	Break(IDOK);
}

void SelectMemberDlg::Highlight()
{
	int id = list.GetKey();
	Sql sql;
	sql * Select(Count(ID)).From(LEDGER).Where(MEMBER_ID==id && DUE<GetSysDate() && IsNull(RETURNED));
	if(sql.Fetch()){
		if(int(sql[0])>0){
			lblDetail.SetImage(LibradorImg::orange());
			lblDetail.SetText(Format(t_("Member has %d dues."), int(sql[0])));
			return;
		}
	}
	lblDetail.SetText(t_("Member has no dues."));
	lblDetail.SetImage(LibradorImg::green());
	
	sql * Select(DOR).From(MEMBER).Where(ID == id && IsNull(DTIME) && !IsNull(DOR));
	if (sql.Fetch()) {
	    Date dor = sql[0];
		lblRenew.SetText(String("Membership renews on: ") << FormatDate(dor, "DD-MM-YYYY"));
		lblRenew.SetImage(GetSysDate() < dor?LibradorImg::green():LibradorImg::orange());
		return;
	}
	lblRenew.SetText("Membership renewal date not set");
	lblRenew.SetImage(LibradorImg::orange());
	
}

void SelectMemberDlg::FilterList()
{
	if(filter.GetLength() == 0) {ClearFilter(); return; }
	if(filter.GetLength() < 3) return;
	
		
	list.Query(IsNull(DTIME)
				&&  (Like(NAME, Wild(String()<<~filter<<"*"))
				||	Like(MEMBER_NO, Wild(String()<<~filter<<"*"))
				||	Like(PHONE, Wild(String("*")<<~filter))
				||	Like(ADDRESS, Wild(String("*")<<~filter<<"*"))
				|| BARCODE == ~filter)
	);
}


void SelectMemberDlg::ClearFilter()
{
	filter.Clear();
	list.Query(IsNull(DTIME));
}


MemberListDlg::MemberListDlg(String name, Value id)
{
	CtrlLayout(*this);
	Sizeable().CloseBoxRejects();
	list.SetLineCy(GetStdFontCy() + DPI(10));
	list.SetTable(MEMBER);
	list.AddKey(ID);
	list.AddColumn(MEMBER_NO, t_("Mem. #"));
	list.AddColumn(NAME, t_("Name"));
	list.AddColumn(PHONE, t_("Phone"));
	list.AddColumn(ADDRESS, t_("Address")).SetDisplay(Single<AddressDisplay>());
	list.SetOrderBy(NAME, ID);
	list.ColumnWidths("45 175 94 201");
	list.Query(MGROUP_ID == id && IsNull(DTIME));
	list.WhenLeftDouble << [&]{	
		int id = list.GetKey();
		if(IsNull(id))
			return;
		MemberDlg dlg;
		if(dlg.ExecuteEdit(id)){
			list.ReQuery();
		}
	};
	Title(Format("Showing %d Members from #%d (%s)",list.GetCount(), int(id), name));
}
