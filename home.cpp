#include "Librador.h"

String GEN_STATS = R"(
	select
	  (select count(*) from book where DTIME is NULL and STATUS in (0,1,2) and strftime('%d-%m-%Y', CTIME) = strftime('%d-%m-%Y', 'now')) as today_books,
	  (select count(*) from book where DTIME is not NULL) as damaged_books,
	  (select count(*) from book where DTIME is NULL and STATUS in (0,1,2) and strftime('%m-%Y', CTIME) = strftime('%m-%Y', 'now')) as month_books,
	  (select count(*) from book where DTIME is NULL and STATUS in (0,1,2) and strftime('%Y', CTIME) = strftime('%Y', 'now')) as year_books,
      (select count(*) from ledger where DTIME IS NULL and strftime('%d-%m-%Y', CTIME) = strftime('%d-%m-%Y', 'now')) as today_loans,
	  (select count(*) from ledger where DTIME IS NULL and strftime('%m-%Y', CTIME) = strftime('%m-%Y', 'now')) as month_loans,
	  (select count(*) from ledger where DTIME is NULL and strftime('%Y', CTIME) = strftime('%Y', 'now')) as year_loans,
	  (select count(*) from ledger where DTIME is NULL and RETURNED is NULL and DUE < date('now')) as due_loans,
	  (select count(*) from member where DTIME is NULL and STATUS = 0) as total_members,
	  (select count(*) from member where DTIME is NULL and STATUS = 0 and GENDER = 2) as male_members,
	  (select count(*) from member where DTIME is NULL and STATUS = 0 and GENDER in (1, 3)) as female_members
   ;
)";

String MEM_STATS = R"(
	select mgroup.name, count(*) as total from member left join mgroup on mgroup_id = mgroup.id where DTIME is NULL group by mgroup_id;
)";

String TPL =  R"(
   [!Noto Sans Malayalam UI! 
	[=c $LIBNAME & ]
	[2
			{{3:3:3:3f0g0-1@(200.200.200)l140/30R2r220/30R2 
				[*@2= Books Added]:: 				::-1 [*@2= Books Issued]::@2l50/0R2r30/0 
				:: [> Today [* :]]	:: $TODAY_BOOKS			:: [> Today [* :]]	:: $TODAY_LOANS
				:: [> This Month [* :]]	:: $MONTH_BOOKS		:: [> This Month [* :]]	:: $MONTH_LOANS
				:: [> This Year [* :]]	:: $YEAR_BOOKS			:: [> This Year [* :]]	:: $YEAR_LOANS
				:: [> Damaged [* :]]	:: $DAMAGED_BOOKS		:: [> Due [* :]]	:: $DUE_LOANS
				:: ::-1@(200.200.200)l140/30R2r220/30R2t70/ [*@2=  Members]::  ::@2l50/0R2r30/0t30/0 
				::-3 {{1:1:1:1:1g0f0  :: [= Total [* :]    $TOTAL_MEMBERS]:: [= Female [* :]    $FEMALE_MEMBERS]:: [= Male [* :]    $MALE_MEMBERS]:: )";
String TPL_END = "}}}}] ]";


HomeTab::HomeTab()
{
	CtrlLayout(*this);
	info = R"(
		[=c!Noto Sans Malayalam UI! & Desaveni Library & ]
		[2
				{{3:3:3:3f0g1-1@(200.200.200)l140/30R2r220/30R2 
					[*@2= Books Added]:: 				::-1 [*@2= Books Loaned]::@2l50/0R2r30/0 
					:: [> This Month [* :]]	:: 0		:: [> This Month [* :]]	:: 0
					:: [> This Year [* :]]	:: 0			:: [> This Year [* :]]	:: 0
					:: [> All Time [* :]]	:: 0			:: [> All Time [* :]]	:: 0
					:: [> Damaged [* :]]	:: 0		:: [> Due [* :]]	:: 0
					:: ::-1@(200.200.200)l140/30R2r220/30R2t70/ [*@2=  Members]::  ::@2l50/0R2r30/0t30/0 
					::-4 {{1:1:1:1:1g0f0 :: [= Total [* :]    200]:: [= Female [* :]    200]:: [= Male [* :]    200]:: 
					::-4 [=  Normal User [* :]    1000       Balavedhi [* :]    1203     Special User [* :]    23122 ]
				}}
		]
	)";	
				
	dues.SetLineCy(GetStdFontCy() + DPI(10));
	dues.SetTable(ISSUES);
	dues.AddKey(ID);
	dues.AddIndex(BOOK_ID);
	dues.AddColumn(CALL_NO, t_(CfgStr("aliasCall"))).Tip("Call No");
	dues.AddColumn(TITLE, t_("Title"));
	dues.AddColumn(NAME, t_("Member"));
	dues.AddColumn(PHONE, t_("Mobile"));
	dues.AddColumn(ISSUED, t_("Issued"));
	dues.AddColumn(DUE, t_("Due"));
	dues.ColumnWidths("60 397 224 145 89 90");
	dues.SetOrderBy(ISSUED);
	dues.Query(IsNull(RETURNED) && DUE < SqlCurrentDate());
	
	static EditField::Style EditHilight =  EditField::StyleDefault();
	EditHilight.invalid = Blend(White(), Green(), 60);

	actions.itemHeight = 37;
	actions.itemSpacing = 20;
	actions
		.Add(t_("New Book"), LibradorImg::icoAdd(), t_("Add new book"), [=]{
			librador->tabs.Set(1);
			librador->booksTab.books.btnNew.WhenAction();
		})
		.Add(t_("New Member"), LibradorImg::icoAdd(), t_("Add new member"), [=]{
			librador->tabs.Set(2);
			librador->membersTab.btnNew.WhenAction();
		})
		.Add(t_("New Issual"), LibradorImg::right(), t_("Find or issue a book"), [=]{
			librador->tabs.Set(1);
			librador->booksTab.books.filter.SetFocus();
			librador->booksTab.books.filter.SetStyle(EditHilight);
			SetTimeCallback(300, [=]{librador->booksTab.books.filter.Error(); });
			SetTimeCallback(5000, [=]{librador->booksTab.books.filter.Error(false);	});
		})
		.Add(t_("Return Book"), LibradorImg::rreturn(), t_("Return or extend a book"), [=]{
			librador->tabs.Set(1);
			librador->booksTab.loans.filter.SetFocus();
			librador->booksTab.loans.filter.SetStyle(EditHilight);
			SetTimeCallback(300, [=]{librador->booksTab.loans.filter.Error(); });
			SetTimeCallback(5000, [=]{librador->booksTab.loans.filter.Error(false);	});
		})
	;
}

void HomeTab::UpdateStats()
{
	String tpl(TPL);
	Sql sql;
	Vector<Value> vals;
	
	tpl.Replace("$LIBNAME", CfgStr("name"));
	
	sql.Execute(GEN_STATS);
	sql.Fetch(vals);
	for(int i = 0; i < sql.GetColumns(); i++){
		tpl.Replace(String("$")<<sql.GetColumnInfo(i).name, AsString(vals[i]));
	}
	//::-4 [=  Normal User [* :]    1000       Balavedhi [* :]    1203     Special User [* :]    23122 ]
	int rc=0;
	String groups;
	sql.Execute("select mgroup.name, count(*) as total from member left join mgroup on mgroup_id = mgroup.id group by mgroup_id;");
	while(sql.Fetch()){
		rc++;
		if(!groups.IsEmpty())
			groups<<"         ";
		groups << sql[0] << "[* :]  " << sql[1];
	}
	tpl << "::-4 [= " << groups << " ]" << TPL_END;
	info = tpl;

	dues.ReQuery();
	
	
}

