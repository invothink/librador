#ifndef _nbuild_version_h_
#define _nbuild_version_h_
#define APP_VDATE "21.04"
#define APP_VMAJOR 21
#define APP_VMINOR 4
#define APP_VFEATURE 04
#define APP_VBUILD 770
#define APP_VER "21.04.04.0770"
#define APP_NAME "LibCat"
#define APP_YEAR 2021
#endif
