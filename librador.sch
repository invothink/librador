TABLE_(MEMBER)
	INT_    (ID) PRIMARY_KEY
	DATE_   (CTIME)
	DATE_   (DTIME) INDEX
	INT_    (MGROUP_ID)
	STRING_ (MEMBER_NO, 30) INDEX
	STRING_ (NAME, 120)
	STRING_ (PHONE, 30) INDEX
	STRING_ (EMAIL, 30)
	DATE_   (DOB)
	INT_	(GENDER)  SQLDEFAULT(0)
	STRING_ (ADDRESS, 300)
	DATE_   (DOR)
	INT_    (DEPOSIT) SQLDEFAULT(0)
	INT_	(BLOOD)  SQLDEFAULT(0)
	STRING_ (BARCODE, 30) INDEX
	INT_    (REFRER)
	DATE_   (DOJ)
	STRING_ (WARD, 30)
	DATE_   (EXPIRY)
	STRING_ (JOB, 300)
	INT_    (STATUS) SQLDEFAULT(0)
END_TABLE

TABLE_(MGROUP)
	INT		(ID) PRIMARY_KEY
	STRING  (NAME, 300)
	STRING_ (CODE, 3)
END_TABLE


TABLE_(BOOK)
	INT	    (ID) PRIMARY_KEY
	STRING_ (ACCN_NO, 30) INDEX
	STRING_ (CALL_NO, 30) INDEX
	STRING_ (TITLE, 300) INDEX
	STRING_ (AUTHOR, 300) INDEX
	STRING_ (ISBN, 20) INDEX
	STRING_ (EDITION, 30)
	STRING_ (SHELF, 30)
	STRING  (BARCODE, 30) INDEX
	STRING_ (DDCCODE, 30) 
	STRING_ (DELNOTE, 300)
	INT_    (PRICE)
	INT_    (CAT_ID)
	INT_	(PUB_ID)
	INT_    (LANG_ID)
	INT_    (TYPE_ID) SQLDEFAULT(0)
	INT		(STATUS) SQLDEFAULT(0)
END_TABLE

TABLE_(AUTOBOOK)
	INT	    (ID) PRIMARY_KEY
	STRING  (TITLE, 300) INDEX
	STRING  (AUTHOR, 300)
	STRING  (PUBLISHER, 300)
	STRING  (DDCCODE, 30)
	INT     (LANG_ID)
	INT		(STATUS) SQLDEFAULT(0)
END_TABLE

TABLE_(PUBLISHER)
	INT		(ID) PRIMARY_KEY
	STRING  (NAME, 300)
END_TABLE

TABLE_(CATEGORY)
	INT		(ID) PRIMARY_KEY
	STRING  (NAME, 300)
	STRING  (CODE, 3)
	INT		(STATUS) SQLDEFAULT(0)
END_TABLE

TABLE_(LANGUAGE)
	INT		(ID) PRIMARY_KEY
	STRING  (NAME, 300)
END_TABLE

TABLE_(LEDGER)
	INT     (ID) PRIMARY_KEY
	INT_	(MEMBER_ID)
	INT_	(BOOK_ID)
	INT_	(RENEWALS)
	DATE_	(ISSUED)	
	DATE_	(DUE)
	DATE_	(RETURNED)
END_TABLE
