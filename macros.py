#!/usr/bin/env python3

import os
import sys
from pathlib import Path
from subprocess import check_output
from shlex import split
from datetime import datetime


def run(args):
	return check_output(split(args)).decode().splitlines()


def hsplit(cmd):
	return {h: m for h, m in (l.split(' ', 1) for l in run(cmd))}


def cmd_changelog():
	commits = hsplit('git log --oneline --format="%H %s"')
	tags = run('git tag -l  --format="%(refname:strip=2) %(creatordate:format:(%d %b %Y))"')
	hash_tags = {run(f"git rev-list -n 1 {t.split(' ')[0]}")[0]: t for t in tags}

	changes = {}
	messages = []
	next_ver = f"next version ({datetime.now().strftime('%d %b %Y')})"
	ct = next_ver
	lm = ""
	
	for h, m in commits.items():
		t = hash_tags.get(h)
		if t:
			changes[ct] = messages
			ct = t
			messages = []
		if m.startswith('Merge branch'):
			continue
		if m != lm:
			messages.append(m)
		lm = m

	# commented out to strip first tag or lack of it
	# changes[ct] = messages
	with Path('CHANGELOG.md').open('w') as out:
		title = f"# {Path('.').absolute().name} changelog"
		print(title, file=out)
		print(len(title) * '-', file=out)
		for ver, messages in changes.items():
			if len(messages) < 1:
				continue
			print("\n## Changes in", ver, file=out)
			for m in sorted(messages):
				print(" -", m, file=out)
				
	print('\n```\nLibcat ' + next_ver)
	i = 0
	for m in reversed(changes.get(next_ver, [])):
		i += 1
		print(f"{i:-2}. {m}")
	print('```')


def cmd_sync(target):
	cmd_changelog()
	ctime = datetime.fromtimestamp(Path(target).stat().st_mtime)
	fprefix = "lc-next-" + ctime.strftime('%y%m%d%H') + "-"
	osname = 'win64' if target.endswith('.exe') else 'lin64'
	archive = "/tmp/" + fprefix + osname + ".7z"
	run(f"7za a {archive} CHANGELOG.md {target}")
	run(f"scp {archive} lc1:files/libcat")
	files = run(f"ssh lc1 ls files/libcat/{fprefix}*")
	print('\n'.join(['', 'Files: ', *['https://libcat.in/files/libcat/' + Path(f).name for f in files]]))
	os.unlink(archive)

def cmd_upload():
	if os.environ.get('__interm__') !='1':
		os.environ['__interm__'] = '1'
		run(f"st -e ./macros.py upload")
		exit(0)
	
	changed = run('git status --porcelain --untracked-files=no')
	if len(changed) != 0:
		pfixes = 'fix: feat: build: refact: misc: doc:'.split()
		print("Following changed files exists:\n\n\t"+'\n\t'.join(changed))
		msg = input(f"\nEnter a commit message with prefix ({', '.join(pfixes)}) or ctrl-c to cancel: ")
		if msg == '':
			exit(0)
		print("committing with msg:", msg)
		os.system(f"git commit -am '{msg}'")
		os.system("git push")
		
		input("\nenter to exit")


if __name__ == '__main__':
	cmds = {k[4:]: v for k, v in globals().items() if k.startswith('cmd_')}
	if len(sys.argv) < 2:
		print("commands:\n\t"+'\n\t'.join(cmds.keys()))
		exit(0)
	cmd = sys.argv[1]
	
	cmds.get(cmd, lambda *args: print(f'not found', args))(*sys.argv[2:])
